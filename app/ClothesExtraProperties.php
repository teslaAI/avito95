<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClothesExtraProperties extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'ad_id',
        'type',
    ];
}
