<?php

namespace App\Services;

use App\Option;
use Carbon\Carbon;
use Couchbase\Exception;
use DiDom\Document;
use Session;

class AvitoParser
{
    const LAST_DATE_KEY = 'avito-last-date';

    private $boardUrl = 'https://m.avito.ru/chechenskaya_respublika';

    private $document;

    private $cookies = [];

    public function __construct(Document $document)
    {
        $this->document = $document;
    }

    public function getAds($count = null)
    {
        $data = [];

        $html = $this->loadListPageWithHeaders($this->boardUrl);

//        dd($this->document->loadHtml($html)->text());

        foreach ($this->document->loadHtml($html)->find('._32nlx:not(._32nlx ._3i5aH, ._22pZC, ._32nlx ._2Vu1z)') as $item) {

            if ($item->find('._2owEx ')) {
                $v = strtotime(substr($item->find('._2owEx ')[0]->text(), -5). ':00');
                $now = strtotime(Carbon::now()->subMinutes(2)->toTimeString());
                $substracted = strtotime(Carbon::now()->subMinutes(10)->toTimeString());

//                if ($now > $v && $v > $substracted) {

                // dd($now .' - '. $v . "\r\n" . Carbon::now()->subMinutes(2)->toTimeString() . ' - ' . substr($item->find('._2owEx ')[0]->text(), -5). ':00');

                if (!empty($item->find('.info-shop'))) {
                    continue;
                }

                if ($count !== null && !$count--) {
                    break;
                }

                $adDateString = $this->checkDate($item);
                if (!$adDateString) {
                    continue;
                }

//            if (!$item->attr('data-item-premium')) {
                $url = $item->find('a')[0]->attr('href');
                $processedAd = $this->processAd($url);
                if ($processedAd && is_array($processedAd)) {
                    $city = $item->find('[data-marker="item/location]')[0]->text();
                    $href = $item->find('[data-marker="item/link"]')[0]->attr('href');
                    $hrefPieces = explode('/', $href);
                    $category = isset($hrefPieces[2]) ? $hrefPieces[2] : null;

                    try {
                        $data[] = array_merge($processedAd, [
                            'time' => $adDateString,
                            'city' => $city === 'Орджоникидзевская' ? 'Сунжа' : $city,
                            'category' => $category,
                            'is_parsed' => true,
                        ]);
                    } catch (Exception $e) {

                    }

                }
                sleep(5);
//            }
//                } else {
//                    continue;
//                }
            }
        }

        return $data;
    }

    public function processAd($url)
    {
        sleep(5);
        $document = app()->make(Document::class);

        if ($url[0] === '/') {
            $fullUrl = "https://m.avito.ru$url";
        } else {
            $fullUrl = "https://m.avito.ru/$url";
        }
//        dd($this->loadAdPageWithHeaders($fullUrl, $url));
        $adDoc = $document->loadHtml($this->loadAdPageWithHeaders($fullUrl, $url));

//        echo $adDoc->text();
//        exit();

        $phoneButton = $adDoc->find('[data-marker="item-contact-bar/call"]');

        if (!$phoneButton) return 1;

        $id = str_replace('Объявление №', '', trim($adDoc->find('[data-marker="item-stats/timestamp"]')[0]->text()));

        $personName = $adDoc->find('[data-marker="seller-info/name"]');

        $price = str_replace(['руб.', chr(0xC2).chr(0xA0), "\n", ' '], '', $adDoc->find('[data-marker="item-description/price"]')[0]->text());

        $phone = trim(substr($this->getPhone($fullUrl), 4));

        if ($phone == null) {
            return 1;
        }

        $data = [
            'id' => $id,
            'title' => trim($adDoc->find('[data-marker="item-description/title"]')[0]->text()),
            'text' => trim($adDoc->find('[itemprop=description]')[0]->attr('content')),
//            'text' => 'lorem text',
            'author_name' => !empty($personName) ? trim($personName[0]->text()) : 'Аноним',
            'price' => $price,
            'city' => trim($adDoc->find('[data-marker="delivery/location"]')[0]->text()),
            'images' => $this->processImages($adDoc),
//            'images' => [],
            'category' => $this->processCategory($url),
//            'phone' => $this->processPhone($adDoc, $fullUrl),
            'phone' => str_replace(['-',' '], '', $phone),
            'views' => rand(10, 100)
        ];

        return $data;
    }

    private function checkDate($item)
    {
        $date = trim(str_replace(['Сегодня,', chr(0xC2).chr(0xA0)], '', $item->find('._2owEx')[0]->text()));
        $exploded = explode(':', $date);

        $adDate = Carbon::now();
        $adDate->hour((int)$exploded[0])->minute((int)$exploded[1])->second(0);

        $option = Option::get(self::LAST_DATE_KEY);

        if ($option && (string)$adDate <= $option) {
            dump(Option::get(self::LAST_DATE_KEY));
            return null;
        }

        return (string)$adDate;
    }

    public function loadListPageWithHeaders($fullUrl)
    {
        $opts = [
            'http' => [
                'proxy' => 'tcp://92.249.122.108:61778',
                'method' => "GET",
                'header' =>
                    ":authority: m.avito.ru\r\n".
                    ":method: GET\r\n".
                    ":path: /chechenskaya_respublika\r\n".
                    ":scheme: https\r\n".
                    "Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8\r\n".
//                    "Accept-Encoding: gzip, deflate, br".
                    "Accept-Language: ru,en;q=0.9\r\n".
                    "Cache-Control: no-cache\r\n".
                    "Cookie: u=2b7chjys.lgji3v.fxbf41788c; _ym_uid=1509619638126425520; __gads=ID=50c498690fee4a47:T=1509619639:S=ALNI_MauPKxZ22jJPs8rh8kGWC1sgTgCJA; _vwo_uuid=C41D6E5FAD61581FDE0156E179CDC35F; _vwo_uuid_v2=9CB83E1C5DA4155351E9E6ED4F1A465D|da4a1b743f35d174489b6e06ac317442; _ga=GA1.2.1241490185.1509619637; _ga=GA1.3.1241490185.1509619637; dfp_group=26; _vwo_ds=3%3Aa_0%2Ct_0%241515762641%3A8.76797056%3A%3A%3A18_0%2C12_0; cto_lwid=501beb83-0a2e-4aed-92aa-8ff45e36e74d; _mlocation=628455; crtg_rta=cravadb240%3D1%3B; _vis_opt_exp_32_exclude=1; sessid=0fd34fda08217d0e173c6036b40655aa.1516454621; abp=1; _gid=GA1.2.2069556231.1516454622; _vis_opt_s=11%7C; _vis_opt_test_cookie=1; _ym_isad=1; f=5.0c4f4b6d233fb90636b4dd61b04726f18a38e2c5b3e08b898a38e2c5b3e08b898a38e2c5b3e08b898a38e2c5b3e08b898a38e2c5b3e08b898a38e2c5b3e08b898a38e2c5b3e08b898a38e2c5b3e08b898a38e2c5b3e08b898a38e2c5b3e08b890df103df0c26013a0df103df0c26013a2ebf3cb6fd35a0ac0df103df0c26013a8b1472fe2f9ba6b984dcacfe8ebe897bfa4d7ea84258c63d74c4a16376f87ccd897baa7410138eadfb0fb526bb39450a46b8ae4e81acb9fae2415097439d4047fb0fb526bb39450a46b8ae4e81acb9fa34d62295fceb188dd99271d186dc1cd03de19da9ed218fe2d50b96489ab264ed87829363e2d856a2143114829cf33ca746b8ae4e81acb9fabfb238445a07e29917e4fdff63c88d54e6ea0a3c215c2f5b9d1b7d935fd798e3dd2052e0abe9e5e33fdc85926d4eb0de485b72f6761f37d61cd7784cf7badc3460768b50dd5e12c3376dd221ed8bc1c5d97ade10503d8334465d5650ed2fd5c1685428d00dc691fa9e82118971f2ed6494d66450ac1e729282590a2160b7c4153de19da9ed218fe23de19da9ed218fe25070b82175c7eb3e88a2226f9e3048bd2e3395d4baf5981713de726369c13761; _gid=GA1.3.2069556231.1516454622; v=1516463272; _ym_visorc_24618824=b; sx=H4sIAAAAAAACA1XPTW7CQAyG4bvMmoVDJxOH2yQO8QS3mMoB8yPuXqsSFV1Z3%2BbR60f6qLJsh%2BV4vdxQSMhRkCVjTrtHuqRdmsDNvm0%2BqWaw7MoImQWEid05bdI%2B7Zq2KW3uC26fm1T4tlz3c3dSMBHm7JnRRelF1mksy9dazTQTqROLY3CQyUz%2BkU2DfZB4OB6CxP2dmdU9SsAJHV5kc9NxnNvaTgagCOJq7IAaw9DeyW1p2iDHQ3vBz%2FN6HFDEo0EBzaLzr%2FJ77ryu1N09srICR6Ojm6gR6DvZdv0v2csk65nrEAUqmQnJSDO%2BSFt4HodzOVWCHIkkEF9bHFNFfycLID6fPxxkoZugAQAA\r\n".
//                    "Cookie: sx=H4sIAAAAAAACAwXBOw6AIAwA0Lt0dgDlU7kNaUjVDkSrdjDc3fc%2B8Otx9%2F05s10dnSpLF2RWhPLBCwVqlFY50EaMFFSJupiRWRA1ZzBBg%2BKjT2kOecExfqYusVRUAAAA;\r\n".
                    "Pragma: no-cache\r\n".
                    "Referer: https://m.avito.ru/\r\n".
                    "Upgrade-Insecure-Requests: 1\r\n".
//                    "User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36\r\n".
                    "X-Compress: null\r\n"
            ]
        ];

        $context = stream_context_create($opts);

//        dd($fullUrl);
        $result = file_get_contents($fullUrl, false, $context);

        $try = 1;
        while(!$result){
            $try++;
            $result = file_get_contents($fullUrl, false, $context);

            if($result || $try == 4){
                break;
            }

            sleep(2);
        }

        $this->getCookiesFromResponseHeader($http_response_header);
        return $result;
    }

    private function getCookiesFromResponseHeader($header)
    {
        $cookies1 = explode('; ', str_replace('Set-Cookie: ', '', $header[6]));
        $cookies2 = explode('; ', str_replace('Set-Cookie: ', '', $header[7]));
        $result = [];

        $cookies = array_merge($cookies1, $cookies2);

        foreach ($cookies as $cookie) {
            $exploded = explode('=', $cookie);
            $result[$exploded[0]] = isset($exploded[1]) ? $exploded[1] : '';
        }

//        dd($result);

        $result['_mlocation'] = '628455';
        $result['buyer_location_id'] = '621540';
        $result['cto_lwid'] = 'f6732fe7-882d-48c2-b012-773ae6c2418e';
        $result['sessid'] = 'a7010d0bda43bddb4753099660912190.1516538326';
//        $result['u'] = '2aqvhfsm.1mxa307.fnoierjof8';

        $this->cookies = $result;
    }

    private function getCookiesString()
    {
        $pairs = [];

        foreach ($this->cookies as $key => $cookie) {
            $pairs[] = "$key=$cookie";
        }

        return implode($pairs, '; ');
    }

    private function loadAdPageWithHeaders($fullUrl, $url)
    {
//        dd(22,$this->getCookiesString());

        $opts = [
            'http' => [
                'proxy' => 'tcp://92.249.122.108:61778',
                'method' => "GET",
                'header' =>
                    ":authority: m.avito.ru\r\n".
                    ":method: GET\r\n".
                    ":path: $url\r\n".
                    ":scheme: https\r\n".
                    "Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8\r\n".
//                    "Accept-Encoding: gzip, deflate, br\r\n".
                    "Accept-Language: ru,en;q=0.9\r\n".
                    "Cache-Control: no-cache\r\n".
                    "Cookie: {$this->getCookiesString()}\r\n".
                    "Pragma: no-cache\r\n".
                    "Referer: https://m.avito.ru/chechenskaya_respublika\r\n".
                    "Upgrade-Insecure-Requests: 1\r\n".
                    "User-Agent: Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Mobile Safari/537.36\r\n".
                    "X-Compress: null\r\n"
            ]
        ];

//        dd($opts['http']['header']);

        $context = stream_context_create($opts);

//        dd($fullUrl);

        $result = file_get_contents($fullUrl, false, $context);

        $try = 1;
        while(!$result){
            $try++;
            $result = file_get_contents($fullUrl, false, $context);

            if($result || $try == 4){
                break;
            }

            sleep(2);
        }

        return $result;
    }

    public function processImages($document)
    {
        $result = [];

        $end = explode('initialData__ = ', $document->text())[1];
        $needed = explode(' || {}', $end)[0];

//        if (empty($document->find('#itemPhotogalleryData')[0])) {
//            return [];
//        }

//        $json = $document->find('#itemPhotogalleryData')[0]->text();

        if (isset(json_decode($needed)->item->item->images)) {
            $images = json_decode($needed)->item->item->images;

//        dd($images);

            foreach ($images as $image) {
                $result[] = $image->{'640x480'};
            }
        }

        return $result;
    }

    public function processCategory($url)
    {
        return explode('/', $url)[2];
    }

    public function processPhone($document, $url)
    {
        $exploded = explode('avito.storage.searchHash', $document->find('body')[0]->text());
        dd($exploded);
        $exploded = explode('\'', $exploded[1]);
        $hash = $exploded[1];

        $href = $document->find('.action-show-number')[0]->attr('href');
        $href = "https://m.avito.ru/$href?async&searchHash=$hash";

        // Create a stream
        $opts = [
            'http' => [
                'method' => "GET",
                'header' => "Accept-language: en\r\n" .
                    "X-Requested-With: XMLHttpRequest\r\n" .
                    "Referer: $url"
            ]
        ];

        $context = stream_context_create($opts);

        // Open the file using the HTTP headers set above
        $file = file_get_contents($href, false, $context);

        preg_match('/^{"phone":"(.*)"}.*/', $file, $matches);

        dump($matches);
        return str_replace([' ', '-'], '', $matches[1]);
    }

    public function getPhone($fullUrl)
    {
        $nodePath = env('PATH_TO_NODE');
        exec(env('NODE_COMMAND'), $nodePath);

        if ($nodePath && $nodePath[0]) {

            exec('"' . $nodePath[0] . '" ' . public_path('/js/nightmare_avito.js') . ' --url=' . $fullUrl, $jsOutput);

            return isset($jsOutput[0]) ? $jsOutput[0] : null;
        }
        else {
            echo 'node not found.';
        }
    }
}