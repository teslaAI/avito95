<?php

namespace App\Services;

use App\Option;
use DiDom\Document;

class BerkatParser
{
    const LAST_ID_KEY = 'berkat-last-id';

    private $document;

    public function __construct(Document $document)
    {
        $this->document = $document;
    }

    public function getAds($count = null)
    {
        $ads = [];

        foreach ($this->document->find('.board_list_item') as $key => $adElement) {

            if ($count && $key == $count) {
                break;
            }

            $fields = $this->getAdFields($adElement);

            if ($fields) {
                $ads[] = $fields;
                echo "Parsed ad: {$fields['title']}\r\n";
            }
        }

        return $ads;
    }

    private function getAdFields($adElement)
    {
        $id = $this->getAdId($adElement);

        if ($id <= Option::get(self::LAST_ID_KEY)) {
            return null;
        }

        $phone = $this->getAdPhone($adElement);

        if (!$phone) {
            return null;
        }

        return [
            'id' => $id,
            'title' => $this->getAdTitle($adElement),
            'text' => $this->getAdText($adElement),
            'price' => $this->getAdPrice($adElement),
            'author_name' => $this->getAdAuthorName($adElement),
            'images' => $this->getAdImages($adElement),
            'city' => $this->getAdCity($adElement),
            'category' => $this->getAdCategory($adElement),
            'phone' => $phone,
        ];
    }

    public function getAdId($adElement)
    {
        return $adElement->find('.board_list_id span:not(.board_list_id)')[0]->text();
    }

    private function getAdTitle($adElement)
    {
        return $adElement->find('.board_list_item_title a')[0]->text();
    }

    private function getAdText($adElement)
    {
        return $adElement->find('.board_list_item_text')[0]->text();
    }

    private function getAdPhone($adElement)
    {
        $contactsArray = $adElement->find('.board_list_contacts');

        if (!count($contactsArray)) {
            return null;
        }

        $phone = $contactsArray[0]->find('span:not(.board_list_contacts)')[0]->text();
        return str_replace(['(', ')', '-'], '', $phone);
    }

    private function getAdPrice($adElement)
    {
        $priceArray = $adElement->find('.board_list_price span');

        if (!count($priceArray)) {
            return 0;
        }

        return (int)str_replace(' ', '', $priceArray[0]->text());
    }

    private function getAdAuthorName($adElement)
    {
        $contactsArray = $adElement->find('.board_list_contacts');

        if (count($contactsArray) <= 1) {
            return '';
        }

        $name = $fields['name'] = $contactsArray[1]->text();

        return str_replace(["\r", "\n", "\t"], '', $name);
    }

    private function getAdCity($adElement)
    {
        $cityArray = $adElement->find('.board_list_city');

        return count($cityArray) ? $cityArray[0]->find('span:not(.board_list_city)')[0]->text() : null;
    }

    private function getAdImages($adElement)
    {
        $images = [];

        $adDocument = (new Document())->loadHtmlFile($this->getAdUrl($adElement));

        foreach ($adDocument->find('.fotorama a') as $imageElement) {
            $images[] = 'http://berkat.ru/' . $imageElement->attr('data-full');
        }

        return $images;
    }

    private function getAdUrl($adElement)
    {
        return 'http://berkat.ru' . $adElement->find('.board_list_item_title a')[0]->attr('href');
    }

    private function getAdCategory($adElement)
    {
        return $adElement->find('.board_list_cat a')[0]->text();
    }
}