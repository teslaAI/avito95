<?php

namespace App\Services;

/**
 * Payment client for robokassa
 */
class RobokassaPayment
{
    /**
     * Robokassa login
     * @var string
     */
    protected $mrh_login = null;

    /**
     * Password
     * @var string
     */
    protected $mrh_pass = null;

    /**
     * Order sum
     * @var integer
     */
    protected $out_summ = null;

    /**
     * Number of order
     * @var integer
     */
    protected $inv_id = null;

    /**
     * Description
     * @var string
     */
    protected $shp_item = null;

    /**
     * Description
     * @var integer
     */
    protected $shp_login = null;

    /**
     * RobokassaPayment constructor.
     * @param $out_summ
     * @param $inv_id
     * @param $shp_item
     * @param $shp_login
     * @param $pass
     */
    public function __construct($out_summ, $inv_id, $shp_item, $shp_login, $pass)
    {
        $this->mrh_login = config('api.robokassa_login');
        $this->out_summ = $out_summ;
        $this->inv_id = $inv_id;
        $this->shp_item = $shp_item;
        $this->shp_login = $shp_login;
        $this->mrh_pass = $pass;
    }

    /**
     * Get signature
     *
     * @return tag
     */
    public function signatureValue()
    {
        return md5("$this->mrh_login:$this->out_summ:$this->inv_id:$this->mrh_pass:Shp_item=$this->shp_item:Shp_login=$this->shp_login");
    }

    /**
     * Get after payment signature
     *
     * @return tag
     */
    public function secondValue()
    {
        return md5("$this->out_summ:$this->inv_id:$this->mrh_pass:Shp_item=$this->shp_item:Shp_login=$this->shp_login");
    }
}