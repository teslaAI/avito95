<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BlockedNumber extends Model
{
    protected $fillable = ['number'];

    public function setNumberAttribute($value)
    {
        $this->attributes['number'] = formatPhoneNumber($value);
    }
}
