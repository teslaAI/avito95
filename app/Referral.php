<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Referral extends Model
{
    protected $fillable = ['ad_id', 'user_id', 'user_ip'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
