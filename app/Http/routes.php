<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
*/

if(version_compare(PHP_VERSION, '7.2.0', '>=')) {
    error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING);
}

Auth::routes();

Route::get('/', 'MainpageController@index');
Route::get('/category/{slug?}/{make?}/{model?}', 'MainpageController@index');
// Route::get('/category/{slug}/{make?}/{model?}', 'MainpageController@byCategory');
Route::get('/aadd/setfavorite/{id}', 'AdController@setfavorite');



Route::get('/aadd/removefavorite/{id}', 'AdController@removefavorite');
Route::get('/aadd/refresh/{id}', 'AdController@refresh')->name('web_ad.refresh');
Route::get('/aadd/remove/{id}', 'AdController@destroy')->name('web_ad.destroy');
Route::get('/aadd/remove_image/{id}', 'AdController@removeImageById');
Route::get('/aadd/premium/{id}', 'AdController@makePremium');
Route::get('/aadd/super_premium/{id}', 'AdController@superPremium');
Route::get('/aadd/discount/{id}', 'AdController@discount');
Route::post('/aadd/discount/{id}', 'AdController@makeDiscount')->name('discount');
Route::get('/fresh/aadd/{id}', 'AdController@fresh');
Route::get('/ad/{id}', 'AdController@show');

Route::get('/extra/filters/{slug}/{id?}', 'MainpageController@extra_filters');

// Route::get('/category/{slug}', 'CategoryController@index');

Route::get('publish', 'AdController@create');
Route::post('publish', 'AdController@store');
Route::get('ad/edit/{id}', 'AdController@edit');
Route::post('ad/edit/{id}', 'AdController@update');


Route::get('/publish/extra/getmodels/{make_id}', 'AdController@getModelsByMake');
Route::get('/publish/extra/{slug}/{id?}', 'AdController@getExtraProperties');

Route::post('/publish/images', 'AdImageController@upload');
Route::match(['get', 'post'], '/publish/remove_images', 'AdController@removeImages');

Route::get('/login', 'Auth\LoginController@login');
Route::get('/register', 'Auth\RegisterController@register');
Route::get('/logout', 'Auth\LoginController@logout');
Route::get('/forget', 'Auth\ForgotPasswordController@forget');

Route::match(['get', 'post'], '/profile/settings', 'ProfileController@settings');
Route::get('/profile/ads', 'ProfileController@ads');
Route::get('/profile/searchs', 'ProfileController@searchs');
Route::get('/profile/favorite', 'ProfileController@favorite');
Route::get('/profile/referral', 'ProfileController@referral');
Route::get('/profile/questions', 'ProfileController@questions');

Route::get('/comment/create', 'CommentController@create');
Route::get('/comment/remove/{id}', 'CommentController@remove');

Route::get('/about', 'StaticPagesController@about');
Route::get('/rules', 'StaticPagesController@rules');
Route::get('/contacts', 'StaticPagesController@contacts');

Route::match(['get','post'], '/decode', 'DecodeController@index');

Route::get('parser', 'ParserController@index');
Route::post('parser', 'ParserController@parse');

Route::post('payment', 'AdController@makePayment');
Route::post('payment-success', 'AdController@paymentSuccess');
Route::post('payment-fail', 'AdController@paymentFail');

Route::group(['prefix' => 'api'], function () {
    Route::post('payment-result/{api_token}', 'AdController@paymentResult')->middleware('api_token');
});

Route::group(['namespace' => 'API', 'prefix' => 'api'], function () {
    Route::resource('ad', 'AdController');
    Route::get('category', 'CategoryController@index');
    Route::get('city', 'CityController@index');
});


Route::post('{token}/webhook', function () {
    $update = Telegram::commandsHandler(true);

    return 'ok';
});