<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class StoreAdRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // todo: add auto and realty rules
        return [
            'title' => 'max:70',
            'text' => 'max:1000',
            'name' => 'required',
            'price' => 'max:10',
            'phone' => 'required|not_in_blacklist',
            'category_id' => 'required',
            'city_id' => 'required',
        ];
    }
}
