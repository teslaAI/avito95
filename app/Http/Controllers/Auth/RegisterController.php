<?php

namespace App\Http\Controllers\Auth;

use App\Mail\EmailConfirm;
use Auth;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Mail;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }

    public function register(Request $request)
    {
        if ($request->has('confirm_code') && $request->has('email') && $request->has('password'))
        {
            $recaptcha = config('services.recaptcha');
            $captchaUrl = "{$recaptcha['url']}?secret={$recaptcha['secret']}&response={$request['g-recaptcha-response']}";

            $response = json_decode(file_get_contents($captchaUrl));

            if (!$response->success) {
                return 6;
            }

            $code = $request['confirm_code'];
            $email = $request['email'];
            $password = $request['password'];

            if (User::where('email', $email)->first()->confirm_code == $code)
            {
                $user = User::where('email', $email)->first();
                $user->email_confirmed = true;

                $user->save();

                if (Auth::attempt(['email' => $email, 'password' => $password ])){
                    return 4;
                }
            }else{
                return 5;
            }
        }

        if ($request->has('name') && $request->has('email') && $request->has('password'))
        {
            $name = $request['name'];
            $email = $request['email'];
            $password = $request['password'];

            if (null !== User::where('email', $email)->first())
            {
                return 2;
            }

            $user = new User();

            $user->name = $name;
            $user->email = $email;
            $user->password = $password;
            $user->confirm_code = rand (10000 ,99999);

            Mail::to($email)->send(new EmailConfirm($user->name, $user->confirm_code));

//            if ($result->status)
//            {
//                return 3;
//            }

            $user->save();

            return 1;
        }
    }
}
