<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Request;
use App\User;
use Mail;
use App\Mail\ChangePassword;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function forget()
    {
        if (Request::has('email'))
        {
            $email = Request::input('email');

            $user = User::where('email', $email)->first();

            if (null !== $user)
            {
                $password = rand (100000, 999999);
                $user->password = $password;
                $user->save();

                Mail::to($email)->send(new ChangePassword($user->name, $password));

                return 0;
            }else{
                return 1;
            }
        }
    }
}
