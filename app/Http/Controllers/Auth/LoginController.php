<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Auth;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }

    public function login(Request $request)
    {
        $email = $request['email'];
        $phone = str_replace([' ', '(', ')', '-'], '', $request->input('phone'));
        $password = $request['password'];

        $user = $phone ? User::where('phone', $phone)->first() : User::where('email', $email)->first();

        if(null !== $user)
        {
            if ($phone && Auth::attempt(['phone' => $phone, 'password' => $password ], true)) return 0;

            if ($user->email_confirmed && Auth::attempt(['email' => $email, 'password' => $password ], true)){
                return 0;
            }else{
                return 1;
            }
        }
        else{
            return 2;
        }
    }
}
