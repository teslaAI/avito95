<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request as IlReq;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Comment;
use App\Ad;
use Auth;
use Request;
use Mail;

class CommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Auth::check() && null !== Request::input('text'))
        {
            $text = Request::input('text');
            $ad_id = Request::input('ad_id');

            $comment = new Comment;

            $comment->author_id = Auth::user()->id;
            $comment->ad_id = $ad_id;
            $comment->text = $text;

            if (null !== Request::input('answer_to')){
                $comment->answer_to = Request::input('answer_to');
            }

            if (null !== Request::input('is_private')){
                $comment->is_private = true;
            }

            $comment->save();


            // Mail

            $ad = $comment->ad;

            $data = [
                'username' => $comment->answer_to == null ? $ad->author->name : $comment->answerTo->author->name,
                'ad_title' => $ad->title,
                'ad_id' => $ad->id,
                'comment_text' => $comment->text
            ];

            if ($comment->answer_to == null)
            {
                if ($ad->author->email != null)
                {
                    Mail::send( 'emails.new_question', $data, function ($m) use($ad){
                        $m->from('info@hoam.ru', 'Хоам');
                        $m->to($ad->author->email, $ad->author->name)->subject('Уведомление о новом вопросе');
                    });
                }
            }
            else
            {
                $question_author = $comment->answerTo->author;
                
                if ($question_author->email != null)
                {
                    Mail::send( 'emails.new_answer', $data, function ($m) use($ad, $question_author){
                        $m->from('info@hoam.ru', 'Хоам');
                        $m->to($question_author->email, $question_author->name)->subject('Уведомление о новом ответе');
                    });
                }
            }

            return view('sub.comments', [
                'ad' => Ad::whereId($ad_id)->firstOrFail(),
                // 'comments' => Comment::getByAdId($ad_id)
            ]);
        }else{
            return 'error';
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function remove($id)
    {
        if (Auth::check())
        {
            $comment = Comment::whereId($id)->first();

            if ($comment === null)
            {
                $ad_id = Request::input('ad');
            }
            else
            {
                $ad_id = $comment->ad_id;

                $user_id = Auth::user()->id;

                if ($user_id == $comment->author_id || $user_id == $comment->ad->author->id)
                {
                    $comment->delete();
                }
            }

            return view('sub.comments', [
                'ad' => Ad::whereId($ad_id)->first(),
                'comments' => Comment::getByAdId($ad_id)
            ]);
        }
    }
}
