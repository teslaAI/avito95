<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Category;

class StaticPagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function about()
    {
        return view('static.about', [
            'categories' => Category::getAll()
        ]);
    }

    public function rules()
    {
        return view('static.rules', [
            'categories' => Category::getAll()
        ]);
    }

    public function contacts()
    {
        return view('static.contacts', [
            'categories' => Category::getAll()
        ]);
    }
}
