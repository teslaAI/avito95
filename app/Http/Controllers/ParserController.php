<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;

class ParserController extends Controller
{
    public function index(Request $request)
    {
        Artisan::call('parse', $request->has('count') ? [
            '--count' => $request['count'],
        ] : []);
    }
}
