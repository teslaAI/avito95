<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Ad;
use App\Category;

class AdController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $builder = Ad::with('images')->orderBy('created_at', 'DESC');

        if ($request->has('city_id'))
        {
            $builder->where('city_id', $request['city_id']);
        }

        if ($request->has('category_id'))
        {
            $category = Category::whereId($request['category_id'])->firstOrFail();

            if ($category->parent_id)
            {
                $builder->where('category_id', $category->id);
            }
            else
            {
                $categories_ids = $category->children()->select('id')->get();
                $builder->whereIn('category_id', $categories_ids);
            }
        }

        if ($request->has('price_from'))
        {
            $builder->where('price', '>', $request['price_from']);
        }

        if ($request->has('price_to'))
        {
            $builder->where('price', '<', $request['price_to']);
        }

        if ($request->has('search'))
        {
            $search = $request['search'];

            $builder->where(function ($query) use ($search) {
                $query->where('title', 'LIKE', '%'. $search .'%')
                    ->orWhere('text', 'LIKE', '%'. $search .'%');
            });
        }

        $ads = $builder->paginate(30);

        return response()->json($ads);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $ad = Ad::with('images')->whereId($id)->firstOrFail();

        $ad->incrementViews();

        return response()->json($ad);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
