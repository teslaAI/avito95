<?php

namespace App\Http\Controllers;

use App\Ad;
use App\Category;
use App\Comment;
use App\Favorite;
use App\Referral;
use App\Region;
use App\User;
use Auth;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    public function settings(Request $request)
    {
        if (Auth::guest()) {
            return redirect('/');
        }

        User::edit($request);

        return view('profile.settings', [
            'categories' => Category::getAll(),
            'regions' => Region::with('cities')->get(),
        ]);
    }

    public function ads()
    {
        if (Auth::check()) {
            $ads = Ad::where('author_id', Auth::user()->id)->orderBy('created_at', 'desc')->paginate(30);

            return view('profile.ads', [
                'ads' => $ads
            ]);
        } else {
            return redirect('/');
        }
    }

    public function favorite()
    {
        if (Auth::check()) {
            return view('profile.favorite', [
                'categories' => Category::getAll(),
                'ads' => Favorite::getById(Auth::user()->id),
            ]);
        } else {
            return redirect('/');
        }
    }

    public function searchs()
    {
        if (Auth::check()) {
            return view('profile.searchs', [
                'categories' => Category::getAll(),
            ]);
        } else {
            return redirect('/');
        }
    }

    public function questions()
    {
        if (Auth::check()) {
            $comments = Comment::whereHas('ad', function ($query) {
                $query->where([
                    'author_id' => Auth::user()->id,
                    'answer_to' => null
                ]);
            })->orWhereHas('answerTo', function ($query) {
                $query->where('author_id', Auth::user()->id);
            })->orderBy('created_at', 'desc')->get();

            return view('profile.questions', [
                'categories' => Category::getAll(),
                'comments' => $comments,
            ]);
        } else {
            return redirect('/');
        }
    }

    public function referral()
    {
        $count_users_with_referrals = User::has('referrals')->get()->count();

        $contest_users = User::orderBy('referral_count', 'desc')->get();

        $user_index = $contest_users->search(function($user) {
            return $user->id === Auth::id();
        }) + 1;

        $ads = Ad::where('is_contest', 1)->where('id', '!=', 1)->get();

        return view('profile.referral', compact('user_index', 'count_users_with_referrals', 'ads'));
    }

}
