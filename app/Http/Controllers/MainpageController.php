<?php

namespace App\Http\Controllers;

use App\Ad;
use App\AutoModel;
use App\Category;
use App\Custom\Filters\AdFilters;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;

class MainpageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, AdFilters $filters, $slug = null, $make_slug = null, $model_slug = null)
    {
        if ($request->has('category')) {
            $getParameters = '?';

            foreach ($request->except(['category', 'make', 'model']) as $key => $value) {
                $getParameters .= $value == '' ? '' : "$key=$value&";
            }

            $getParameters = substr($getParameters, 0, -1);

            $redirectPath = '/category/' . $request['category'];

            if ($request->has('make') && $request['make'] != 'any') {
                $redirectPath .= '/' . $request['make'];

                if ($request->has('model') && $request['model'] != 'any') {
                    $redirectPath .= '/' . $request['model'];
                }
            }

            return redirect($redirectPath . $getParameters);
        }

//        $builder = Ad::filter($filters)->orderBy('is_premium', 'desc')->orderBy('created_at', 'desc')
//            ->with(['auto_models', 'favorite']);

        if ($request->segment(2)) {
            $request['search'] = utf8_decode($request['search']);
        }

        if ($request['referral_id']) {
            User::makeReferral(0, $request);
        }

        $builder = Ad::filter($filters)->orderBy('created_at', 'desc')
            ->with(['auto_models', 'favorite']);

        if ($request['clothes_type_of_ad']) {
            $builder->whereHas('clothes_properties', function ($q) use ($request) {
                $q->where('type', $request['clothes_type_of_ad']);
            });
        }


        if ($request['filter'] == 'discount') {
            $builder = $builder->select('ads.*')
                ->join('discounts', 'ads.id', '=', 'discounts.ad_id')->where('discounts.active', 1);

            $discount_categories = Category::getAllByDiscount();
        } else {
            $request['search'] ? $builder : $builder->where('is_premium', 0);
        }

        $premium = Ad::where([['is_premium', 1], ['is_top', 0]]);
        $top_premium = Ad::where('is_top', 1);

        if ($slug) {
            $category = Category::where('slug', $slug)->firstOrFail();

            if ($category->parent_id) {
                $builder->where('category_id', $category->id);
                $premium->where('category_id', $category->id);
                $top_premium->where('category_id', $category->id);
            } else {
                $categories_ids = $category->children()->select('id')->get();

                $builder->whereIn('category_id', $categories_ids);
                $premium->whereIn('category_id', $categories_ids);
                $top_premium->whereIn('category_id', $categories_ids);
            }

            if ($make_slug) {
                if (!$model_slug) {
                    $make_id = AutoModel::where('slug', $make_slug)->select('id')->firstOrFail()->id;

                    $builder->whereHas('auto_models', function ($query) use ($make_id) {
                        $query->where('auto_models.make_id', $make_id);
                    });
                } else {
                    $model_id = AutoModel::where('slug', $model_slug)->select('id')->firstOrFail()->id;

                    $builder->whereHas('auto_models', function ($query) use ($model_id) {
                        $query->where('auto_models.id', $model_id);
                    });
                }
            }
        }

        $grid = \Cookie::get('active_layout') == 'grid' ? \Cookie::get('active_layout') : '';

//        $visitedAdId = \Cookie::get('visited_ad_id');
//
//        if ($visitedAdId) {
//            $newBuilder = clone $builder;
//            $laterThanVisitedAdsCount = $newBuilder->where('created_at', '>', Ad::find($visitedAdId)->created_at)->count();
//            $pageNumber = ceil(($laterThanVisitedAdsCount + 1) / 50);
//            $request['page'] = $pageNumber;
//            Cookie::queue('start_page', $pageNumber, 60 * 24 * 3, null, null, null, false);
//        }

        $ads = $builder->paginate(50)->setPath($request->url())->appends($request->except('page'));

        $top_premium = $top_premium->get();

        $data = [
            'ads' => $ads,
            'premium' => $premium->get(),
            'top_premium' => count($top_premium) >= 4 ? $top_premium->random(4) : $top_premium,
            'grid' => $grid,
            'discount_categories' => isset($discount_categories) ? $discount_categories : null
        ];

        if (isset($category)) {
            $data['category'] = $category;
            $data['title'] = $category->name;
        }

        if ($request->ajax()) {
            $view = view('sub.ads', $data)->render();
            $premium = view('sub.ads', ['ads' => $premium->count() >= 2 ? $premium->get()->random(2) : $premium->get()])->render();
            return response()->json(['html' => $view, 'premium' => $premium]);
        }

        return view('mainpage', $data);
    }

    public function extra_filters($slug, $makeSlug = null)
    {
        switch ($slug) {
            case 'auto':
                return view('sub.autoFilters', [
                    'makes' => AutoModel::allWithAdsCount()
                ]);
                break;
            case 'models':
                return view('sub.models', [
                    'models' => AutoModel::getModelsByMake($makeSlug)
                ]);
                break;
        }
    }
}
