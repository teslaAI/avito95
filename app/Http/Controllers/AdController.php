<?php

namespace App\Http\Controllers;

use App\BlockedNumber;
use App\Discount;
use App\PrepaidDiscount;
use App\Referral;
use App\SuperPremiumAd;
use App\User;
use Illuminate\Http\Request as IlReq;

use App\Http\Controllers\Controller;
use App\Category;
use App\Ad;
use App\Region;
use App\AutoExtraProperties;
use Auth;
use App\Favorite;
use App\AutoModel;
use DB;
use File;
use Request;
use App\AdImage;
use App\ClothesExtraProperties;
// use Validator;

use Gate;
use App\Events\SingleAdLoaded;
use App\Http\Requests\StoreAdRequest;
use App\RealtyExtraProperties;
use Carbon\Carbon;
use App\Services\RobokassaPayment;

class AdController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('publish', [
            'categories' => Category::getAll(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreAdRequest $request)
    {
        $recaptcha = config('services.recaptcha');
        $captchaUrl = "{$recaptcha['url']}?secret={$recaptcha['secret']}&response={$request['g-recaptcha-response']}";

        $response = json_decode(file_get_contents($captchaUrl));

        if (!$response->success) {
            return redirect()->back()->with('robot', true);
        }

        // quick antiflood fix

        $isFlood = Ad::where([
            'title' => $request['title'],
            'text' => $request['text'],
        ])->where('created_at', '>', Carbon::now()->subMinutes(10))->count();

        if ($isFlood)
        {
            return redirect()->back()->with('isFlood', true);
        }

        //////////////////////

        $isFree = $request['is_free'] == 'on';
        $userId = Auth::check() ? Auth::user()->id : null;

        $ad = Ad::create([
            'title' => $request['title'],
            'text' => $request['text'],
            'author_name' => $request['name'],
            'phone' => $request['phone'],
            'whatsapp' => $request['whatsapp'],
            'category_id' => $request['category_id'],
            'city_id' => $request['city_id'],
            'price' => $isFree ? 0 : $request['price'],
            'is_free' => $isFree,
            'author_id' => $userId,
        ]);

        /*
        *   Store the ad's images
        */
        if ($request->has('images'))
        {
            $imagesArr = [];

            foreach ($request['images'] as $image)
            {
                $imagePaths = explode('|', $image);

                $imagesArr[] = [
                    'ad_id' => $ad->id,
                    'small' => $imagePaths[0],
                    'big' => $imagePaths[1]
                ];
            }

            AdImage::insert($imagesArr);
        }

        /*
        *   Store the ad's auto extra properties if received
        */
        if ($request->has('make'))
        {
            AutoExtraProperties::create([
                'ad_id' => $ad->id,
                'model_id' => $request['model'],
                'year' => $request['year'],
                'capacity' => $request['capacity'],
                'engine_type' => $request['engine_type'],
                'transmission' => $request['transmission'],
                'mileage' => $request['mileage'],
                'power' => $request['power'],
            ]);
        }

        /*
        *   Store the ad's realty extra properties if received
        */
        if ($request->has('type_of_ad'))
        {
            RealtyExtraProperties::create([
                'ad_id' => $ad->id,
                'type' => $request['type_of_ad'],
                'lease' => $request['lease'],
                'num_of_rooms' => $request['num_of_rooms'],
                'square' => $request['square'],
                'floor' => $request['floor'],
                'num_of_floors' => $request['num_of_floors'],
            ]);
        }

        /*
        *   Store the ad's realty extra properties if received
        */
        if ($request->has('clothes_type_of_ad'))
        {
            ClothesExtraProperties::create([
                'ad_id' => $ad->id,
                'type' => $request['clothes_type_of_ad']
            ]);
        }

        // Ad::createAd($ad); // Auto properties, realty properties

        return redirect('/ad/' . $ad->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function show(IlReq $request, $id)
    {
//        $ad_id = $request['prev_ad_id'] ? $request['prev_ad_id'] : $id;
//        \Cookie::queue('visited_ad_id', $ad_id, 60 * 24 * 3, null, null, null, false);

        $ad = Ad::with([
            'auto_properties.auto_model.parent',
            'category.parent',
            'comments.answers'
        ])->findOrFail($id);

        $ad->incrementViews();

        if ($request['referral_id']) {
            User::makeReferral($ad->id, $request);
        }

        return view('ad', [
            'ad' => $ad,
            'autoModel' => $ad->auto_properties ? $ad->auto_properties->auto_model : null,
            'similarAds' => $ad->getSimilarAds(),
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // $author_id = Ad::whereId($id)->first()->author->id;

        $ad = Ad::findOrFail($id);

        if (Auth::check() && Auth::user()->can('update', $ad))
        {
            return view('publish', [
                'categories' => Category::getAll(),
                'ad' => $ad,
            ]);
        }
        else
        {
            return redirect('/');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreAdRequest $request, $id)
    {
        $ad = Ad::findOrFail($id);

        if (Gate::allows('update', $ad))
        {
            $isFree = $request['is_free'] == 'on';

            $ad->update([
                'title' => $request['title'],
                'text' => $request['text'],
                'author_name' => $request['name'],
                'phone' => $request['phone'],
                'whatsapp' => $request['whatsapp'],
                'category_id' => $request['category_id'],
                'city_id' => $request['city_id'],
                'price' => $isFree ? 0 : $request['price'],
                'is_free' => $isFree,
            ]);

            /*
            *   Store the ad's images
            */
            if ($request->has('images'))
            {
                $imagesArr = [];

                foreach ($request['images'] as $image)
                {
                    $imagePaths = explode('|', $image);

                    $imagesArr[] = [
                        'ad_id' => $ad->id,
                        'small' => $imagePaths[0],
                        'big' => $imagePaths[1]
                    ];
                }

                AdImage::insert($imagesArr);
            }

            /*
            *   Update the ad's auto extra properties if received
            */
            if ($request->has('make'))
            {
                $autoExtraProperties = AutoExtraProperties::where('ad_id', $ad->id)->first();

                $fields = [
                    'ad_id' => $ad->id,
                    'model_id' => $request['model'],
                    'year' => $request['year'],
                    'capacity' => $request['capacity'],
                    'engine_type' => $request['engine_type'],
                    'transmission' => $request['transmission'],
                    'mileage' => $request['mileage'],
                    'power' => $request['power'],
                ];

                if ($autoExtraProperties) {
                    $autoExtraProperties->update($fields);
                } else {
                    AutoExtraProperties::create($fields);
                }
            }

            /*
            *   Update the ad's realty extra properties if received
            */
            if ($request->has('type_of_ad'))
            {
                $realtyExtraProperties = RealtyExtraProperties::where('ad_id', $ad->id)->first();

                $fields = [
                    'ad_id' => $ad->id,
                    'type' => $request['type_of_ad'],
                    'lease' => $request['lease'],
                    'num_of_rooms' => $request['num_of_rooms'],
                    'square' => $request['square'],
                    'floor' => $request['floor'],
                    'num_of_floors' => $request['num_of_floors'],
                ];

                if ($realtyExtraProperties) {
                    $realtyExtraProperties->update($fields);
                } else {
                    RealtyExtraProperties::create($fields);
                }


            }

            return view('publish', [
                'categories' => Category::getAll(),
                'ad' => $ad,
                'updated' => true,
            ]);
        }else{
            return redirect('/');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(IlReq $request, $id)
    {
        $ad = Ad::findOrFail($id);

        if ($request['block_number']) {
            BlockedNumber::create(['number' => $ad->phone]);
        }

        if (Gate::allows('delete', $ad))
        {
            $ad->delete();
            return response('1', 200);
        }
        else
        {
            // todo: change response code
            return response('0', 200);
        }
    }

    public function getExtraProperties($slug, $id=null)
    {
        switch ($slug) {
            case 'auto':
                if ($id != null)
                {
                    $ad = Ad::with([
                        'auto_properties.auto_model.parent',
                        'category.parent',
                        // 'comments.answers'
                    ])->findOrFail($id);
                }

                // todo: wtf?
                $data = [
                    // 'properties' => AutoExtraProperties::$properties,
                    // 'models' => AutoModel::all(),
                    'models' => AutoModel::where('make_id', null)->get(),
                ];

                if (isset($ad)){
                    $data['ad'] = $ad;
                }

                return view('sub.autoExtraProperties', $data);
                break;

            case 'realty':
                if ($id != null)
                {
                    $ad = Ad::with([
                        // 'auto_properties.auto_model.parent',
                        'category.parent',
                        'realty_properties',
                        // 'comments.answers'
                    ])->findOrFail($id);
                }

                return view('sub.realtyExtraProperties', [
                    'category' => Request::input('category'),
                    'ad' => isset($ad) ? $ad : null,
                ]);
                break;

            case 'clothes':
                if ($id != null)
                {
                    $ad = Ad::with([
                        'category.parent',
                        'clothes_properties',
                    ])->findOrFail($id);
                }

                return view('sub.clothesExtraProperties', [
                    'category' => Request::input('category'),
                    'ad' => isset($ad) ? $ad : null,
                ]);
                break;
        }
    }

    public function removeImages()
    {
        if (Request::has('paths'))
        {
            $paths = explode('|', Request::input('paths'));
            foreach ($paths as $path) {
                File::delete($path);
            }
        }
    }

    public function removeImageById($id)
    {
        if (Auth::check())
        {
            $image = AdImage::whereId($id)->with('ad')->first();

            if (null !== $image && $image->ad->author_id == Auth::user()->id)
            {
                return AdImage::remove($id);
            }
        }
        return 0;
    }

    public function setfavorite($id)
    {
        if (Auth::check())
        {
            Favorite::create([
                'user_id' => Auth::user()->id,
                'ad_id' => $id
            ]);
            return 0;
        }else{
            return 1;
        }
    }

    public function removefavorite($id)
    {
        if (Auth::check())
        {
            Favorite::where('ad_id', $id)->where('user_id', Auth::user()->id)->delete();
        }else{
            return redirect()->back();
        }
    }

    public function fresh($id)
    {
        $ad = Ad::findOrFail($id);

        return view('profile/refresh', ['ad' => $ad]);
    }

    public function refresh($id)
    {
        $ad = Ad::whereId($id)->firstOrFail();

        $this->authorize('refresh', $ad);

        $ad->created_at = date('Y-m-d G:i:s');
        $ad->save();
    }

    public function makePremium($id)
    {
        $ad = Ad::findOrFail($id);

        return view('profile/premium', ['ad' => $ad]);
    }

    public function getModelsByMake($make_id)
    {
        $models = AutoModel::where('make_id', $make_id)->get();

        return view('sub.autoModels', [
            'models' => $models,
        ]);
    }

    /**
     * Make payment in robokassa
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function makePayment(IlReq $request)
    {
        if ($request['message']) {
            SuperPremiumAd::create([
                'message' => $request['message'],
                'ad_id' => substr($request['InvId'], 0, -2),
                'area' => $request['area'],
                'price' => $request['OutSum']
            ]);
        }

        if ($request['prepayment']) {
            PrepaidDiscount::create([
                'ad_id' => substr($request['InvId'], 0, -2),
                'user_id' => auth()->user()->id
            ]);
        }

        $robokassa = new RobokassaPayment($request['OutSum'], $request['InvId'], $request['Shp_item'], auth()->user()->id, config('api.robokassa_password'));
        $signature  = $robokassa->signatureValue();

        return redirect("https://auth.robokassa.ru/Merchant/Index.aspx?"
            ."MerchantLogin=kupi95ru&OutSum=".$request['OutSum']."&InvId=".$request['InvId']
            ."&Shp_item=".$request['Shp_item']."&Shp_login=".auth()->user()->id."&SignatureValue=$signature&IsTest=".env('ROBOKASSA_IS_TEST'));
    }

    /**
     * Payment result
     *
     * @param  \Illuminate\Http\Request  $request
     * @throws
     */
    public function paymentResult(IlReq $request)
    {
        $id = substr($request['InvId'], 0, -2);

        $ad = Ad::whereId($id)->firstOrFail();

        $crc = strtoupper($request['SignatureValue']);

        $robokassa = new RobokassaPayment($request['OutSum'], $request['InvId'], $request['Shp_item'], $request['Shp_login'], config('api.robokassa_password_2'));

        $my_crc = strtoupper($robokassa->secondValue());

        if ($my_crc != $crc)
        {
            echo "bad sign\n";
            exit();
        }

        if ($request['Shp_item'] == 'premium') {
            if(Ad::PREMIUM_SUM <= $request['OutSum']) {
                $ad->premium_date = date('Y-m-d G:i:s');
                $ad->is_premium = 1;
            }
        } elseif ($request['Shp_item'] == 'superPremium') {
            $ad->premium_date = date('Y-m-d G:i:s');
            $ad->is_premium = 1;

            $super_prem = SuperPremiumAd::where('ad_id', $id)->first();
            $super_prem->update(['paid' => 1]);
        } elseif ($request['Shp_item'] == 'prepaidDiscount') {
            $prepaid_discount = PrepaidDiscount::where('ad_id', $id)->where('user_id', $request['Shp_login'])->first();
            $prepaid_discount->update(['paid' => 1]);
        } else {
            if(Ad::REFRESH_SUM <= $request['OutSum']) {
                $ad->created_at = date('Y-m-d G:i:s');
            }
        }

        $ad->save();

        echo "OK".$request['InvId'];
    }

    /**
     * Payment success
     *
     * @return \Illuminate\Http\Response
     */
    public function paymentSuccess()
    {
        return redirect('profile/ads');
    }

    /**
     * Payment fail
     *
     * @return \Illuminate\Http\Response
     */
    public function paymentFail()
    {
        return redirect('profile/ads');
    }

    public function superPremium($id)
    {
        $ad = Ad::findOrFail($id);

        return view('profile/super_premium', ['ad' => $ad]);
    }

    public function discount($id)
    {
        $ad = Ad::findOrFail($id);

        return view('profile/discount', ['ad' => $ad]);
    }

    public function makeDiscount($id, IlReq $request)
    {
        $fields = [
            'ad_id' => $id,
            'prepayment' => $request['prepayment'],
            'discount' => $request['discount']
        ];

        Discount::create($fields);

        return redirect()->back()->with('message', 'Заявка отправлена, ожидайте звонка из службы поддержки');
    }
}
