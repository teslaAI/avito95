<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request as IlReq;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Request;
use Crypt;

class DecodeController extends Controller
{
    public function index()
    {
        if(null !== Request::input('value')){
            return  Crypt::decrypt(Request::input('value'));
        }
    }   
}
