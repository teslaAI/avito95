<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;

use Intervention\Image\Facades\Image;

use File;
use Input;

class AdImageController extends Controller
{
    // old method. need to replace with processImage
    public function upload(Request $request)
    {
        $file = Input::file('file');

        if (null === $file)
        {
            return 'Не удалось загрузить изображение';
        }

        if ($file->isValid())
        {
            $image = Image::make($file);

            $path = 'upload/' . date("Y") .'/'. date('m') . '/' . date('d');
            $extension= File::extension($file->getClientOriginalName());
            $filename = uniqid() .'.'. $extension;

            if (!is_dir($path)){
                mkdir(public_path($path), 0777, true);
            }

            $path .= '/';

            $background = Image::canvas(1200, 900, '#111111');

            $image->backup()->heighten(900)->widen(1200, function($constraint){
                $constraint->upsize();
            });

            $image->insert(asset('img/watermark.png'), 'bottom-right', 20, 20);

            $background->insert($image, 'center');

            $background->save($path."original_".$filename);

            unset($background);

            $image->reset()->fit(760, 570);
            $image->save($path."small_".$filename);

            unset($image);

            return [
                'small' => $path."original_".$filename,
                'big' => $path."small_".$filename
            ];
        }else{
            return response('Invalid file', 400);
        }
    }

    public function processImage($imageSource)
    {
        $image = Image::make($imageSource);
        $now = Carbon::now();

        $path = "upload/$now->year/$now->month/$now->day";
        $absolutePath = public_path($path);
        $filename = uniqid() . '.jpg';

        if (!is_dir($absolutePath)) {
            mkdir($absolutePath, 0777, true);
            chmod($absolutePath, 0777);
        }

        $largeImagePath = "$path/large_$filename";
        $smallImagePath = "$path/small_$filename";

        $this->saveLargeImage($image, $largeImagePath);
        $this->saveSmallImage($image, $smallImagePath);

        return [
            'big' => $largeImagePath,
            'small' => $smallImagePath,
        ];
    }

    private function saveLargeImage($image, $path)
    {
        $background = Image::canvas(1200, 900, '#111111');

        if ($image->width() > $image->height()) {
            $image->fit(1200, 700, function($img) {
                $img->upsize();
            });
        } else {
            $image->fit(1200, 1000, function($img) {
                $img->upsize();
            });
        }

        $image->heighten(900)->widen(1200, function ($constraint) {
            $constraint->upsize();
        });

        $image->insert(asset('img/watermark.png'), 'bottom-right', 20, 20);

        $background->insert($image, 'center');

        $background->save(public_path($path));
    }

    private function saveSmallImage($image, $path)
    {
        $image->fit(760, 570);
        $image->save(public_path($path));
    }
}
