<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Option extends Model
{
    public $fillable = ['key', 'value'];

    public static function get($key)
    {
        $option = Option::where('key', $key)->first();

        return $option ? $option->value : null;

        $current_timestamp = Carbon::now()->addHour()->toDateTimeString();
    }

    public static function set($key, $value)
    {
        $optionBuilder = Option::where('key', $key);
        $option = $optionBuilder->first();

        $fields = [
            'key' => $key,
            'value' => $value,
        ];

        if ($option) {
            return $optionBuilder->update($fields);
        } else {
            return Option::create($fields);
        }
    }
}
