<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RealtyExtraProperties extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'ad_id',
        'type',
        'lease',
        'num_of_rooms',
        'square',
        'floor',
        'num_of_floors',
    ];


	/**
     * Get the properties html markup
     *
     * @return string
     */
    public function getHtml()
    {
        $result = '<dl>';

        switch ($this->lease)
        {
            case 'dayly': $result .= '<dt>Аренда:</dt><dd>Посуточная</dd>';
            case 'long_term': $result .= '<dt>Аренда:</dt><dd>Длительная</dd>';
        }

        if ($this->num_of_rooms)
        {
            $result .= "<dt>Кол-во комнат:</dt><dd>". e($this->num_of_rooms) ."</dd>";
        }

        if ($this->square)
        {
            $result .= "<dt>Площадь:</dt><dd>". e($this->square) ." м²</dd>";
        }

        if ($this->floor)
        {
            $result .= "<dt>Этаж:</dt><dd>". e($this->floor) ."-й</dd>";
        }

        if ($this->num_of_floors)
        {
            $result .= "<dt>Кол-во этажей:</dt><dd>". e($this->num_of_floors) ."</dd>";
        }

        $result .= '</dl>';

        return $result;
    }
}
