<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    public function answerTo()
    {
        return $this->belongsTo('App\Comment', 'answer_to');
    }

    public function question()
    {
        return $this->belongsTo('App\Comment', 'answer_to');
    }

    public function answers()
    {
        return $this->hasMany('App\Comment', 'answer_to');
    }

    public function ad()
    {
        return $this->belongsTo('App\Ad');
    }

    public function author()
    {
        return $this->belongsTo('App\User', 'author_id');
    }

    public static function getByAdId($id)
    {
    	$comments = Comment::where(['ad_id' => $id, 'answer_to' => null])
    							->join('users', 'comments.author_id', '=', 'users.id')
    							->select(['comments.*', 'users.name as author_name'])
                                ->orderBy('comments.created_at')
    							->get();

    	foreach ($comments as $comment)
    	{
    		$comment->answers = Comment::where('answer_to', $comment->id)
			    							->join('users', 'comments.author_id', '=', 'users.id')
			    							->select(['comments.*', 'users.name as author_name'])
    										->get();
    	}

    	return $comments;
    }


    /**
     * Get the comment's formatted time.
     *
     * @return string
     */
    public function getFormattedTime()
    {
        $time = $this->created_at;

        switch(true)
        {
            case $time->isToday(): $formatted = 'Сегодня ' . $time->format('H:i'); break;
            case $time->isYesterday(): $formatted = 'Вчера ' . $time->format('H:i'); break;
            default: $formatted = $time->format('d.m.Y H:i');
        }

        return $formatted;
    }
}
