<?php

if (! function_exists('formatPhoneNumber')) {
    function formatPhoneNumber($number)
    {
        $number = str_replace([' ', '(', ')', '-'], '', $number);

        if ($number[0] != '+') {
            if ($number[0] == '8') {
                $number = substr_replace($number, '+7', 0, 1);
            } else {
                $number = "+7$number";
            }
        }

        return $number;
    }
}