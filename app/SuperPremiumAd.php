<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SuperPremiumAd extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'message',
        'ad_id',
        'area',
        'price',
        'paid',
    ];

    public function ad()
    {
        return $this->belongsTo('App\Ad');
    }
}
