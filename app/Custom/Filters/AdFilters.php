<?php

namespace App\Custom\Filters;

use Illuminate\Database\Eloquent\Builder;
use App\Region;
use App\Category;

class AdFilters extends QueryFilters
{
    /**
     * Filter by search word.
     *
     * @param  string $search
     * @return Builder
     */
    public function search($search = null)
    {
        return $this->builder->where(function($query) use($search){
            $query->where('title', 'LIKE', '%'. $search .'%')
                ->orWhere('text', 'LIKE', '%'. $search .'%')
                ->orWhere('phone', 'LIKE', '%'. $search .'%');
        });
    }

    /**
     * Filter by 'is free' checkbox.
     *
     * @param  boolean $checkbox
     * @return Builder
     */
    public function is_free($checkbox = false)
    {
        return $this->builder->where('is_free', (boolean)$checkbox);
    }

    /**
     * Filter by minimal price.
     *
     * @param  boolean $price
     * @return Builder
     */
    public function pricefrom($price = null)
    {
        if (null === $this->request->is_free && null !== $price)
        {
            return $this->builder->where('price', '>=', $price);
        }

        // return $this->builder;
    }

    /**
     * Filter by maximal price.
     *
     * @param  boolean $price
     * @return Builder
     */
    public function priceto($price = null)
    {
        if (null === $this->request->is_free && null !== $price)
        {
            return $this->builder->where('price', '<=', $price);
        }

        // return $this->builder;
    }

    /**
     * Filter by city.
     *
     * @param  string $city_id
     * @return Builder
     */
    public function city($city_id = null)
    {
        if (!$city_id)
        {
            return;
        }

        $exploded = explode('region', $city_id);

        if (count($exploded) > 1) // if get-parameter has 'region' substring
        {
            $cities = Region::whereId($exploded[1])->first()->cities()->select('id')->get();

            return $this->builder->whereIn('city_id', $cities);
        }
        
        return $this->builder->where('city_id', $city_id);
    }

    /**
     * Filter by 'withphoto' checkbox.
     *
     * @param  boolean $checkbox
     * @return Builder
     */
    public function withphoto($checkbox = false)
    {
        if ($checkbox)
        {
            $this->builder->whereHas('images', function(){}); // todo: is it possible to remove closure?
        }
    }
    

    /*
    |--------------------------------------------------------------------------
    | Auto filters
    |--------------------------------------------------------------------------
    */

    /**
     * Filter by minimal year of auto.
     *
     * @param  string $year
     * @return Builder
     */
    public function auto_year_from($year)
    {
        return $this->builder->whereHas('auto_models', function($query) use($year) {
            $query->where('year', '>=', $year);
        });
    }

    /**
     * Filter by maximal year of auto.
     *
     * @param  string $year
     * @return Builder
     */
    public function auto_year_to($year)
    {
        return $this->builder->whereHas('auto_models', function($query) use($year) {
            $query->where('year', '<=', $year);
        });
    }

    /**
     * Filter by minimal capacity of auto.
     *
     * @param  string $capacity
     * @return Builder
     */
    public function auto_capacity_from($capacity)
    {
        return $this->builder->whereHas('auto_models', function($query) use($capacity) {
            $query->where('capacity', '>=', $capacity);
        });
    }

    /**
     * Filter by maximal capacity of auto.
     *
     * @param  string $capacity
     * @return Builder
     */
    public function auto_capacity_to($capacity)
    {
        return $this->builder->whereHas('auto_models', function($query) use($capacity) {
            $query->where('capacity', '<=', $capacity);
        });
    }

    /**
     * Filter by engine type of auto.
     *
     * @param  string $type
     * @return Builder
     */
    public function auto_engine_type($type)
    {
        return $this->builder->whereHas('auto_models', function($query) use ($type) {
            $query->where('engine_type', $type);
        });
    }

    /**
     * Filter by engine transmission of auto.
     *
     * @param  string $transmission
     * @return Builder
     */
    public function auto_transmission($transmission)
    {
        return $this->builder->whereHas('auto_models', function($query) use ($transmission) {
            $query->where('transmission', $transmission);
        });
    }

}
