<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use File;

class AdImage extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    // protected $fillable = [
    //     'ad_id',
    //     'small',
    //     'big',
    // ];


    public static function boot()
    {
        parent::boot();

        AdImage::deleting(function($image){
            File::delete(public_path($image->big));
            File::delete(public_path($image->small));
        });
    }

	public function ad()
	{
		return $this->belongsTo('App\Ad');
	}

    public static function remove($id)
    {
    	$image = AdImage::whereId($id)->first();

    	$image->delete();

    	return 1;
    }

}
