<?php

namespace App\Telegram\Commands;

use Telegram\Bot\Commands\Command;

class DisableAppCommand extends Command
{
    /**
     * @var string Command Name
     */
    protected $name = "disableapp";

    /**
     * @var string Command Description
     */
    protected $description = "Disable application";

    /**
     * @inheritdoc
     */
    public function handle()
    {
        \Artisan::call('down', ['--allow' => '149.154.160.0/20', '--allow' => '91.108.4.0/22']);

        $this->replyWithMessage(['text' => 'Режим технических работ включен']);
    }
}