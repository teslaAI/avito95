<?php

namespace App\Telegram\Commands;

use Telegram\Bot\Commands\Command;

class EnableAppCommand extends Command
{
    /**
     * @var string Command Name
     */
    protected $name = "enableapp";

    /**
     * @var string Command Description
     */
    protected $description = "Enable application";

    /**
     * @inheritdoc
     */
    public function handle()
    {
        \Artisan::call('up');

        $this->replyWithMessage(['text' => 'Режим технических работ выключен']);
    }
}