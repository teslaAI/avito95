<?php

namespace App\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;
use App\User;
use App\Comment;

class CommentPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function before($user, $ability)
    {
        if ($user->is_admin) {
            return true;
        }
    }


    /**
     * Determine if the given comment can be showed to the user.
     *
     * @param \App\User $user
     * @param \App\Comment $comment
     * @return bool
     */
    public function show(User $user, Comment $comment)
    {
        $isCommentAuthor = $user->id === $comment->author_id;

        return $isCommentAuthor || $user->id === $comment->ad->author_id;
    }


    /**
     * Determine if the given answer to comment can be showed to the user.
     *
     * @param \App\User $user
     * @param \App\Comment $answer
     * @return bool
     */
    public function showAnswer(User $user, Comment $answer)
    {
        $isAnswerAuthor = $user->id === $answer->author_id;

        return $isAnswerAuthor || $user->id === $answer->answerTo->author_id;
    }


    /**
     * Determine if the given comment can be deleted by the user.
     *
     * @param \App\User $user
     * @param \App\Comment $comment
     * @return bool
     */
    public function delete(User $user, Comment $comment)
    {
        $isCommentAuthor = $user->id === $comment->author_id;

        return $isCommentAuthor || $user->id === $comment->ad->author_id;
    }


    /**
     * Determine if the given comment can be replied by the user.
     *
     * @param \App\User $user
     * @param \App\Comment $comment
     * @return bool
     */
    public function reply(User $user, Comment $comment)
    {
        return $user->id === $comment->ad->author_id;
    }
}
