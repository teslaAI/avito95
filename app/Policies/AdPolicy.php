<?php

namespace App\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;
use App\User;
use App\Ad;

class AdPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function before($user, $ability)
    {
        if ($user->is_admin) {
            return true;
        }
    }


    /**
     * Determine if the given ad can be updated by the user.
     *
     * @param \App\User $user
     * @param \App\Ad $ad
     * @return bool
     */
    public function update(User $user, Ad $ad)
    {
        return $user->id === $ad->author_id;
    }


    /**
     * Determine if the given ad can be deleted by the user.
     *
     * @param \App\User $user
     * @param \App\Ad $ad
     * @return bool
     */
    public function delete(User $user, Ad $ad)
    {
        return $user->id === $ad->author_id;
    }

    /**
     * Determine if the given ad can be managed by the user.
     *
     * @param \App\User $user
     * @param \App\Ad $ad
     * @return bool
     */
    public function manage(User $user, Ad $ad)
    {
        return $user->id === $ad->author_id;
    }


    /**
     * Determine if the given ad can be refreshed by the user.
     *
     * @param \App\User $user
     * @param \App\Ad $ad
     * @return bool
     */
    public function refresh(User $user, Ad $ad)
    {
        return $user->id === $ad->author_id && $ad->getDaysToRefresh() <= 0;
    }
}
