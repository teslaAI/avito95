<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AutoExtraProperties extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'ad_id',
        'model_id',
        'year',
        'capacity',
        'engine_type',
        'transmission',
        'mileage',
        'power',
    ];

    public function ad()
    {
        return $this->belongsTo('App\Ad');
    }

    public function auto_model()
    {
        return $this->belongsTo('App\AutoModel', 'model_id');
    }

    /**
     * Get the properties html markup
     *
     * @return string
     */
    public function getHtml()
    {
        $result = '<dl>';

        if ($this->year)
        {
            $result .= "<dt>Год:</dt><dd>". e($this->year) ."</dd>";
        }

        if ($this->capacity)
        {
            $result .= "<dt>Объем:</dt><dd>". e($this->capacity) ." л.</dd>";
        }

        if ($this->power)
        {
            $result .= "<dt>Мощность:</dt><dd>". e($this->power) ." л.с.</dd>";
        }

        if ($this->transmission)
        {
            $result .= "<dt>Трансмиссия:</dt><dd>". e($this->transmission) ."</dd>";
        }

        if ($this->mileage)
        {
            $result .= "<dt>Пробег:</dt><dd>". e($this->mileage) ." км.</dd>";
        }

        $result .= '</dl>';

        return $result;
    }
}
