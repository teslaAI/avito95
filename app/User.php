<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

use Auth;
use Illuminate\Notifications\Notifiable;
use Request;
use Input;
use File;
use Image;
use App\Avatar;
use Validator;

class User extends Model implements AuthenticatableContract,
                                    AuthorizableContract,
                                    CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword, Notifiable;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'email', 'password'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    public function city()
    {
        return $this->belongsTo('App\City');
    }

    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = bcrypt($value);
    }

    public function avatar()
    {
        return $this->hasOne('App\Avatar');
    }

    public function setEmailAttribute($value)
    {
        if ($value) {
            $this->attributes['email'] = $value;
        } else {
            $this->attributes['email'] = null;
        }
    }

    public static function edit($request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:50',
            'phone' => 'required|max:50',
            'email' => 'required|email',
            'city' => 'required|integer',
            'image' => 'image',
        ]);

        if (!$validator->fails())
        {
            Auth::user()->name = $request->input('name');
            Auth::user()->phone = str_replace([' ', '(', ')', '-'], '', $request->input('phone'));
            Auth::user()->whatsapp = str_replace([' ', '(', ')', '-'], '', $request->input('whatsapp'));
            Auth::user()->email = $request->input('email');
            Auth::user()->city_id = $request->input('city');

            if (null != Request::input('password'))
            {
                Auth::user()->password = $request->input('password');
            }

            if (null != Input::file('image'))
            {
                $file = Input::file('image');

                if ($file->isValid())
                {
                    $image = Image::make($file);

                    $notFullPath = 'avatars/' . date("Y") .'/'. date('m') . '/' . date('d') . '/';
                    $path = 'upload/' . $notFullPath;
                    $extension = File::extension($file->getClientOriginalName());
                    $filename = uniqid() .'.'. $extension;

                    if (!is_dir($path)){
                        mkdir(public_path($path), 0777, true);
                    }

                    $path .= '/';

                    $image->fit(140, 140);
                    $image->save($path.$filename);

                    $avatar = Avatar::where('user_id', Auth::user()->id)->first();

                    if (is_null($avatar))
                    {
                        $avatar = new Avatar;
                        $avatar->user_id = Auth::user()->id;
                    }else{
                        File::delete('upload/' . $avatar->path);
                    }
                    $avatar->path = $notFullPath.$filename;
                    $avatar->save();
                }else{
                    echo('Invalid file');
                }
            }

            Auth::user()->save();
        }
    }


    /**************************************
    Toaya code
    */

    /**
     * Get the user's avatar URL.
     *
     * @return string
     */
    public function getAvatarURL()
    {
        if ($this->avatar)
        {
            $path = $this->avatar->path;

            $result = "/upload/$path";
        }
        else
        {
            $result = "/img/noavatar.jpg";
        }

        return Request::root() . $result;
    }

    public function referrals()
    {
        return $this->hasMany('App\Referral');
    }

    public static function makeReferral($ad_id, $request)
    {
        $referral = Referral::where('ad_id', $ad_id)
            ->where('user_id', $request['referral_id'])
            ->where('user_ip', $request->ip())->first();

        if (!$referral) {
            Referral::create([
                'ad_id' => $ad_id,
                'user_id' => $request['referral_id'],
                'user_ip' => $request->ip()
            ]);

            $user = self::whereId($request['referral_id'])->first();
            $user->incrementReferralCount();
        }
    }

    public function incrementReferralCount()
    {
        $this->timestamps = false;
        $this->increment('referral_count');
        $this->timestamps = true;
    }

}
