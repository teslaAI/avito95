<?php

namespace App\Listeners;

use App\Events\SingleAdLoaded;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use Auth;
use App\Comment;

class SetViews
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  SingleAdLoaded  $event
     * @return void
     */
    public function handle(SingleAdLoaded $event)
    {
        $ad = $event->ad;

        $ad->incrementViews();

        if ($user = Auth::user())
        {
            if ($user->id == $ad->author_id)
            {
                $ad->comments()->where('answer_to', null)->update(['is_viewed' => true]);
            }
            else
            {
                $questions_ids = $ad->comments()->where('author_id', $user->id)->get()->map(function($question) {
                    return $question->id;
                });

                Comment::whereIn('answer_to', $questions_ids)->update(['is_viewed' => true]);
            }
        }
    }
}
