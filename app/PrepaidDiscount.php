<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PrepaidDiscount extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'ad_id',
        'user_id',
        'paid'
    ];

    public function ad()
    {
        return $this->belongsTo('App\Ad');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
