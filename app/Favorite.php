<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Favorite extends Model
{
	protected $fillable = ['user_id', 'ad_id'];

    public static function getById($id)
    {
    	$favorites = Favorite::where('user_id', $id)->get();

    	$ad_ids_array = [];

    	foreach ($favorites as $favorite) {
    		$ad_ids_array[] = $favorite->ad_id;
    	}


		$ads = Ad::whereIn('id', $ad_ids_array )->paginate(20);

    	return $ads;
    }
}
