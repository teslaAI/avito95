<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Cookie;
use Laravelrus\LocalizedCarbon\Traits\LocalizedEloquentTrait;
use Request;
use Auth;

class Ad extends Model
{
    use Custom\Traits\Filterable;
    use LocalizedEloquentTrait;

    protected $dates = ['created_at', 'updated_at', 'premium_date'];

    const PREMIUM_SUM = 200;
    const REFRESH_SUM = 10;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'text',
        'author_name',
        'phone',
        'whatsapp',
        'category_id',
        'city_id',
        'price',
        'is_free',
        'is_parsed',
        'is_contest',
        'author_id',
        'views',
        'premium_date',
        'is_premium'
    ];

    public function category()
    {
        return $this->belongsTo('App\Category');
    }

    public function city()
    {
        return $this->belongsTo('App\City');
    }

    public function images()
    {
        return $this->hasMany('App\AdImage');
    }

    public function favorite()
    {
        return $this->hasMany('App\Favorite');
    }

    public function author()
    {
        return $this->belongsTo('App\User', 'author_id');
    }

    // нужно будет убрать
    public function auto_models()
    {
        return $this->belongsToMany('App\AutoModel', 'auto_extra_properties', 'ad_id', 'model_id');
    }

    public function auto_properties()
    {
        return $this->hasOne('App\AutoExtraProperties');
    }

    public function comments()
    {
        return $this->hasMany('App\Comment')->whereNull('answer_to');
    }

    public function realty_properties()
    {
        return $this->hasOne('App\RealtyExtraProperties');
    }

    public function clothes_properties()
    {
        return $this->hasOne('App\ClothesExtraProperties');
    }

    public static function boot()
    {
        parent::boot();

        Ad::deleting(function($ad){
            foreach ($ad->images as $image)
            {
                $image->delete();
            }
        });
    }


    /**
     * Get the ad's price.
     *
     * @return string
     */
    public function getPrice()
    {
        if ($this->is_free) {
            return 'Бесплатно';
        }

        if (!$this->price) {
            return 'Цена не указана';
        }

        if ($this->discount && $this->discount->active) {
            return number_format($this->price - ($this->discount->discount / 100) * $this->price, 0, '.', ' ') . ' руб.';
        }

        return number_format($this->price, 0, '.', ' ') . ' руб.';
    }


    /**
     * Get the ad's formatted time.
     *
     * @return string
     */
    public function getFormattedTime()
    {
        return $this->created_at->diffForHumans();

        $time = $this->created_at;

        switch(true)
        {
            case $time->isToday(): $formatted = 'Сегодня ' . $time->format('H:i'); break;
            case $time->isYesterday(): $formatted = 'Вчера ' . $time->format('H:i'); break;
            default: $formatted = $time->format('d.m.Y');
        }

        return $formatted;
    }


    /**
     * Get the ad's text.
     *
     * @param boolean $is_preview
     * @param integer $length
     * @return string
     */
    public function getText($is_preview = false, $length = 100)
    {
        if ($this->text)
        {
            $result = $is_preview ? str_limit($this->text, $length) : nl2br(e($this->text));
        }
        else
        {
            $result = 'Нет описания';
        }

        return $result;
    }


    /**
     * Get the ad's expire time.
     *
     * @return string
     */
    public function getExpireTime()
    {
        $months = [
            'января',
            'февраля',
            'марта',
            'апреля',
            'мая',
            'июня',
            'июля',
            'августа',
            'сентября',
            'октября',
            'ноября',
            'декабря'
        ];

        $leftAtObj = $this->created_at->addMonths(1);

        return $leftAtObj->day .' '. $months[$leftAtObj->month - 1] .' '. $leftAtObj->year;
    }


    /**
     * Get the ad's expire time.
     *
     * @return string
     */
    public function getExpirePremium()
    {
        $leftAtObj = $this->premium_date->addDays(5);
        $now = Carbon::now()->day;

        $left = $leftAtObj->day - ($now);

        return $left;
    }


    /**
     * Get the ad's passed time progress.
     *
     * @return integer
     */
    public function getPassedTimeProgress()
    {
        $progress = $this->created_at->diffInDays() * 3.33;

        return $progress > 100 ? 100 : $progress;
    }


    /**
     * Get the ad's days left to refresh.
     *
     * @param boolean $humanString
     * @return integer
     */
    public function getDaysToRefresh($humanString = false)
    {
        $canRefreshAfter = 3;
        $diff = $this->created_at->diffInDays();
        $result = max($canRefreshAfter - $diff, 0);

        if ($humanString) {
            $append = $result === 1 ? 'день' : 'дня';
            return "$result $append";
        }

        return $result;
    }


    /**
     * Get the ad's similar ads.
     *
     * @return object
     */
    public function getSimilarAds()
    {
        $count = 5;
        $collection = Ad::where('id', '!=', $this->id)->where('category_id', $this->category_id)->orderByRaw('RAND()')->take($count)->get();

        return $collection;
    }


    /**
     * Check is the ad authenticated user's favorite.
     *
     * @return boolean
     */
    public function isTheUsersFavorite()
    {
        if ($user = Auth::user())
        {
            // todo: change to relation
            $count = Favorite::where('ad_id', $this->id)->where('user_id', $user->id)->count();

            if ($count > 0)
            {
                return true;
            }
        }

        return false;
    }


    /**
     * Get the ad's thumbnail URL.
     *
     * @param  boolean  $big
     * @return string
     */
    public function getThumbnailUrl()
    {
        if ($this->images()->count())
        {
            $thumbnail = $this->images()->first();

            return $thumbnail->small;
        }

        return 'img/placeholder-small.jpg';
    }

    /**
     * Get the ad's share image URL.
     *
     * @param $network
     * @return string
     */
    public function getShareImageUrl($network = '')
    {
        if ($this->images()->count())
        {
            $image = $this->images()->first();

            $url = $image->big;
        }
        else
        {
            switch ($network)
            {
                case 'vk': $url = 'img/share/vk.png'; break;
                case 'facebook': $url = 'img/share/facebook.png'; break;
                default: $url = 'img/hoam-og.jpg';
            }
        }

        return asset($url);
    }

    public function setTitleAttribute($value)
    {
        if (!Request::has('category_id')) {
            $this->attributes['title'] = $value;
            return;
        }

        $autoCategories = [
            'transport',
            'cars',
            'motorcycles',
        ];

        $realtyCategories = [
            'apartment',
            'houses',
            'land',
            'commercial_realty',
        ];

        $clothesCategories = [
            'clothes'
        ];

        $request = Request::all();

        $categorySlug = Category::whereId($request['category_id'])->first()->slug;

        if (in_array($categorySlug, $autoCategories))
        {
            $autoModel = AutoModel::whereId(Request::input('model'))->first();

            $value = sprintf('%s %s, %d',
                $autoModel->parent->title,
                $autoModel->title,
                $request['year']
            );
        }

        if (in_array($categorySlug, $realtyCategories))
        {
            switch ($categorySlug) {
                case 'apartment':
                    $value = sprintf("%s %d-к квартиру, %s м², %d/%d эт.",
                        $request['type_of_ad'],
                        $request['num_of_rooms'],
                        $request['square'],
                        $request['floor'],
                        $request['num_of_floors']
                    );
                    break;
                case 'houses':
                    $value = sprintf("%s дом, %s м²",
                        $request['type_of_ad'],
                        $request['square']
                    );
                    break;
                case 'land':
                    $value = sprintf("%s участок, %s сот.",
                        $request['type_of_ad'],
                        $request['square']/100
                    );
                    break;
                case 'commercial_realty':
                    $value = sprintf("%s помещение, %s м²",
                        $request['type_of_ad'],
                        $request['square']
                    );
                    break;
            }
        }

//        if (in_array($categorySlug, $clothesCategories))
//        {
//            $value = sprintf('%s',
//                $request['clothes_type_of_ad']
//            );
//        }

        $this->attributes['title'] = $value;
    }

    /**
     * Accessor for the ad's URL
     *
     * @return \Illuminate\Contracts\Routing\UrlGenerator|string
     */
    public function getUrlAttribute()
    {
        return url("ad/$this->id");
    }

    public function incrementViews()
    {
        $this->timestamps = false;
        $this->increment('views');
        $this->timestamps = true;
    }

    public function getShareTitleAttribute()
    {
        $result = $this->title;

        if ($this->price) {
            $result .= " — {$this->price}р.";
        }

        return $result;
   }

    public function getIsParsedAttribute()
    {
   		return $this->attributes['is_parsed'] || strpos($this->phone, '(') === false;
    }

    public function discount()
    {
        return $this->hasOne('App\Discount');
    }

    public function prepaidDiscount()
    {
        return $this->hasMany('App\PrepaidDiscount');
    }

    public function referrals()
    {
        return $this->hasMany('App\Referrals');
    }
}
