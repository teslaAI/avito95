<?php

namespace App\Providers;

use App\BlockedNumber;
use Illuminate\Support\ServiceProvider;
use Validator;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Validator::extend('not_in_blacklist', function ($attribute, $value, $parameters, $validator) {
            if (in_array(formatPhoneNumber($value), BlockedNumber::all()->pluck('number')->toArray())) {
                return false;
            }
            return true;
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
