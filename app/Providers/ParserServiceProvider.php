<?php

namespace App\Providers;

use App\Services\AvitoParser;
use App\Services\BerkatParser;
use DiDom\Document;
use Illuminate\Support\ServiceProvider;

class ParserServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->singleton(BerkatParser::class, function ($app) {
            $document = $app->make(Document::class);
            $document->loadHtmlFile('http://berkat.ru/?page=2');
            
            return new BerkatParser($document);
        });

        $this->app->singleton(AvitoParser::class, function ($app) {
            $document = $app->make(Document::class);

            return new AvitoParser($document);
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
