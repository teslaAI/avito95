<?php

namespace App\Providers;

use App\Region;
use Illuminate\Support\ServiceProvider;
use Auth;
use App\Ad;
use App\Comment;
use App\Category;

class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer(['layout', 'errors.layout', 'sub.filters'], function ($view) {

            $new_questions = 0;
            $premium = Ad::where('is_premium', 1)->get();

            if (Auth::check())
            {
                $new_questions = Comment::where('is_viewed', false)->whereHas('ad', function ($query){
                    $query->where([
                        'author_id' => Auth::user()->id,
                        'answer_to' => null,
                    ]);
                })->orWhereHas('answerTo', function ($query){
                    $query->where([
                        'author_id' => Auth::user()->id,
                    ]);
                })->where('is_viewed', false)->count();
            }

            $view->with([
                'new_questions' => $new_questions,
                'categories' => Category::getAll(),
                'premium' => $premium->count() >= 2 ? $premium->random(2) : $premium
            ]);
        });

        view()->composer(['sub.filters', 'publish'], function ($view) {
            $view->with([
                'regions' => Region::with('cities')->get(),
            ]);
        });

        view()->composer(['mainpage', 'layout'], function ($view) {
            $view->with([
                'discount_count' => Category::adsCountDiscount()
            ]);
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
