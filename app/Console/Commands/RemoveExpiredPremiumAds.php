<?php

namespace App\Console\Commands;

use App\Ad;
use Carbon\Carbon;
use Illuminate\Console\Command;

class RemoveExpiredPremiumAds extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'premium:clear';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Remove expired premium ads';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $edgeDay = Carbon::now()->subDays(5);

        Ad::where('premium_date', '<', $edgeDay)->where('is_premium', 1)->get()->each(function ($ad) {
            $this->info("Remove premium: $ad->title");
            $ad->update(['is_premium' => 0]);
        });
    }
}
