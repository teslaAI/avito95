<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Telegram\Bot\Laravel\Facades\Telegram;
use App\Ad;

class CheckParser extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'parser:check';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check availability of parser';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $ad = Ad::latest()->first()->created_at;
        $date = strtotime(Carbon::parse($ad)->addMinutes(50));
        $now = strtotime(Carbon::now());

        if ($now > $date) {
            Telegram::sendMessage([
                'chat_id' => '154892450',
                'text' => 'Парсер подвис!'
            ]);

            $option = \App\Option::first();
            $optionLastDate = strtotime($option->value);

            if ($optionLastDate > $now) {
                $option->delete();
            }
        }
    }
}
