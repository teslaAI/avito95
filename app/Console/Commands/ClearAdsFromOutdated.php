<?php

namespace App\Console\Commands;

use App\Ad;
use Carbon\Carbon;
use Illuminate\Console\Command;

class ClearAdsFromOutdated extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ads:clear';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Delete outdated ads.';

    /**
     * Create a new command instance.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $edgeDay = Carbon::now()->subDays(40);

        Ad::where('created_at', '<', $edgeDay)->where('is_premium', 0)->where('id', '!=', 1)->get()->each(function ($ad) {
            $this->info("Deleting ad: $ad->title");
            $ad->delete();
        });
    }
}
