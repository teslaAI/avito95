<?php

namespace App\Console\Commands;

use App\Ad;
use App\AdImage;
use App\Category;
use App\City;
use App\Http\Controllers\AdImageController;
use App\Option;
use App\Services\AvitoParser;
use App\Services\BerkatParser;
use Carbon\Carbon;
use Illuminate\Console\Command;

class Parse extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'parse {--count=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Parse and insert ads from remote website.';

    private $timestamp;

    private $adImageController;

    /**
     * Create a new command instance.
     *
     * @param $adImageController
     */
    public function __construct(AdImageController $adImageController)
    {
        parent::__construct();
        $this->timestamp = (string)Carbon::now()->subMinutes(random_int(1, 5));
        $this->adImageController = $adImageController;
    }

    /**
     * Execute the console command.
     *
     * @param $parser
     */
    public function handle(AvitoParser $parser)
    {
        $countOption = $this->option('count');
        $ads = $parser->getAds($countOption ? $countOption : null);
//        $ads = $parser->getAds(5);
        $adImages = [];

//        dd(777, $ads);

        if (!count($ads)) {
            $this->info('New ads not found');
            return;
        }

        $ads = $this->replaceCitiesNamesWithIds($ads);
        $ads = $this->replaceCategoriesNamesWithIds($ads);

        foreach ($ads as $ad) {

            $adModel = Ad::create($ad);
            $adModel->created_at = $adModel->updated_at = $this->timestamp;
            $adModel->save();

            foreach ($ad['images'] as $image) {
                $imageFields = $this->saveImage($image, $adModel->id);

                if ($imageFields) {
                    $adImages[] = $imageFields;
                }
            }

            $this->info("Created ad: $adModel->title");
        }

        AdImage::insert($adImages);

        $time = array_column($ads, 'time');

        rsort($time);

        $max_time = $time[0] < Carbon::now()->addMinutes(10)->toDateTimeString() ? $time[0] : $time[1];

        Option::set(AvitoParser::LAST_DATE_KEY, $max_time);

        $this->info("Done!");
    }

    private function saveImage($image, $adId)
    {
        try {
            $imageSizes = $this->adImageController->processImage($image);
        } catch (\Exception $e) {
            $this->error("Failed loading image: $image\r\n" . $e->getMessage());
            \Log::error("Message: {$e->getMessage()}");
            return null;
        }

        return [
            'ad_id' => $adId,
            'big' => $imageSizes['big'],
            'small' => $imageSizes['small'],
            'created_at' => $this->timestamp,
            'updated_at' => $this->timestamp,
        ];
    }

    private function replaceCitiesNamesWithIds($ads)
    {
        $namesOfCities = array_unique(array_filter(array_column($ads, 'city')));
        $cities = City::whereIn('name', $namesOfCities)->get();

        foreach ($ads as $key => $ad) {
            if ($ad['city']) {
                $city = $cities->where('name', $ad['city'])->first();
                $ads[$key]['city_id'] = $city ? $city->id : null;
            }

            unset($ads[$key]['city']);
        }

        return $ads;
    }

    private function replaceCategoriesNamesWithIds($ads)
    {
        $compareList = $this->getCompareList();

        foreach ($ads as $key => $ad) {
            if (isset($compareList[$ad['category']])) {
                $ads[$key]['category'] = $compareList[$ad['category']];
            }
        }

        $namesOfCategories = array_unique(array_filter(array_column($ads, 'category')));
        $categories = Category::whereIn('name', $namesOfCategories)->get();

        foreach ($ads as $key => $ad) {
            if ($ad['category']) {
                $category = $categories->where('name', $ad['category'])->first();
                $ads[$key]['category_id'] = $category ? $category->id : null;
            }
            unset($ads[$key]['category']);
        }

        return $ads;
    }

    private function getCompareList()
    {
        return [
            'avtomobili' => 'Автомобили',
            'vakansii' => 'Вакансии',
            'zemelnye_uchastki' => 'Земельные участки',
            'predlozheniya_uslug' => 'Предложение услуг',
            'bytovaya_tehnika' => 'Бытовая техника',
            'chasy_i_ukrasheniya' => 'Одежда, обувь, аксессуары',
            'zapchasti_i_aksessuary' => 'Запчасти и аксессуары', // avto
            'detskaya_odezhda_i_obuv' => 'Одежда, обувь, аксессуары',
            'drugie_zhivotnye' => 'Животные и растения',
            'koshki' => 'Животные и растения',
            'doma_dachi_kottedzhi' => 'Дома и коттеджи',
            'odezhda_obuv_aksessuary' => 'Одежда, обувь, аксессуары',
            'mebel_i_interer' => 'Мебель и интерьер',
            'gruzoviki_i_spetstehnika' => 'Грузовики и спецтехника',
            'ohota_i_rybalka' => 'Спорт и отдых',
            'telefony' => 'Телефоны',
            'nastolnye_kompyutery' => 'Компьютеры',
            'remont_i_stroitelstvo' => 'Предложение услуг',
            'kvartiry' => 'Квартиры',
            'gotoviy_biznes' => 'Оборудование для бизнеса',
            'oborudovanie_dlya_biznesa' => 'Оборудование для бизнеса',
            'remont_i_stroitelstvo' => 'Стройматериалы'
        ];
    }

//    private function getCompareList()
//    {
//        return [
//            'Легковые автомобили' => 'Автомобили',
//            'Грузовики. Автобусы. Спецтехника' => 'Грузовики и спецтехника',
//            'Автозапчасти и принадлежности' => 'Запчасти и аксессуары',
//            'Автосервис и услуги' => 'Предложение услуг',
//            'Транспортные услуги' => 'Предложение услуг',
//            'Мото-транспорт' => 'Мотоциклы',
//            'Шины' => 'Запчасти и аксессуары',
//            'Диски' => 'Запчасти и аксессуары',
//            'Колёса' => 'Запчасти и аксессуары',
//            'Тюнинг' => 'Запчасти и аксессуары',
////            'Поиск попутчиков' => '',
//            'Квартира' => 'Квартиры',
//            'Дом' => 'Дома и коттеджи',
//            'Земельный участок' => 'Земельные участки',
//            'Коммерческая недвижимость' => 'Коммерческая недвижимость',
//            'Телефоны' => 'Телефоны',
//            'СИМ карты, номера' => 'Телефоны',
//            'Аксессуары к телефонам' => 'Телефоны',
//            'Ремонт телефонов и запчасти' => 'Предложение услуг',
//            'Телекоммуникационные услуги' => 'Предложение услуг',
////            'Сети, кабельная продукция' => '',
//            'Телефония, IP-телефония' => 'Телефоны',
//            'Строительные работы' => 'Предложение услуг',
//            'Услуги электрика' => 'Предложение услуг',
//            'Услуги сантехника' => 'Предложение услуг',
//            'Сантехника, газ, отопление' => 'Сантехника',
//            'Инструменты, рабочий инвентарь' => 'Инструменты',
//            'Строительное оборудование' => 'Оборудование для бизнеса',
//            'Строительные, отделочные материалы' => 'Стройматериалы',
//            'Ремонт квартир и офисов' => 'Предложение услуг',
//            'Установка и изготовление на заказ' => 'Предложение услуг',
//            'Железо' => 'Стройматериалы',
//            'Стекло' => 'Стройматериалы',
//            'Ландшафт' => 'Предложение услуг',
//            'Архитектура и дизайн' => 'Предложение услуг',
////            'Столярные изделия' => '',
//            'Прочие услуги по ремонту' => 'Предложение услуг',
//            'Вакансии' => 'Вакансии',
//            'Ищу работу' => 'Резюме',
//            'Сборка мебели' => 'Предложение услуг',
//            'Репетиторство' => 'Предложение услуг',
//            'Обучение, курсы' => 'Предложение услуг',
//            'Реклама, оформление' => 'Предложение услуг',
//            'Пошив одежды' => 'Предложение услуг',
//            'Праздники, мероприятия' => 'Предложение услуг',
//            'Охрана, сыскные услуги' => 'Предложение услуг',
////            'Интернет, программы, сети' => '',
////            'Юридические услуги' => '',
//            'Финансы и аудит' => 'Предложение услуг',
//            'Домашний персонал' => 'Предложение услуг',
////            'Прочие' => '',
//            'Настольные ПК' => 'Компьютеры',
//            'Ноутбуки' => 'Ноутбуки',
//            'Принтеры и картриджи' => 'Оргтехника и расходники',
//            'Планшетные компьютеры и КПК' => 'Планшеты',
//            'Комплектующие' => 'Комплектующие ПК',
//            'Серверы и сети' => 'Комплектующие ПК',
//            'Мониторы и ИБП' => 'Комплектующие ПК',
//            'Диски, программы, фильмы' => 'Комплектующие ПК',
////            'Аккаунты' => '',
//            'Компьютерные аксессуары' => 'Комплектующие ПК',
//            'Сканеры' => 'Оргтехника и расходники',
//            'МФУ' => 'Оргтехника и расходники',
//            'Ремонт' => 'Предложение услуг',
//            'Телевизоры' => 'Бытовая техника',
//            'Сабвуферы, колонки, усилители' => 'Фото и видео техника',
//            'Игровые приставки' => 'Фото и видео техника',
//            'Видеокамеры' => 'Фото и видео техника',
//            'Видеонаблюдение' => 'Фото и видео техника',
//            'Спутниковые системы' => 'Бытовая техника',
//            'Микроволновые печи' => 'Бытовая техника',
//            'Холодильники' => 'Бытовая техника',
//            'Пылесосы' => 'Бытовая техника',
//            'Фото техника' => 'Фото и видео техника',
//            'Кондиционеры' => 'Бытовая техника',
//            'Техника для дома' => 'Бытовая техника',
//            'Электроника для детей' => 'Бытовая техника',
//            'Продажа бизнеса' => 'Готовый бизнес',
//            'Оборудование для бизнеса' => 'Оборудование для бизнеса',
//            'Деловое партнерство' => 'Готовый бизнес',
//            'Бизнес - образование' => 'Готовый бизнес',
////            'Сетевой маркетинг' => '',
//            'Идеи для бизнеса' => 'Готовый бизнес',
//            'Для зала' => 'Мебель и интерьер',
//            'Для кухни' => 'Мебель и интерьер',
//            'Для спальной' => 'Мебель и интерьер',
//            'Для ванной' => 'Мебель и интерьер',
//            'Интерьер' => 'Мебель и интерьер',
//            'Разная мебель' => 'Мебель и интерьер',
////            'Велосипеды' => '',
////            'Антиквариат, коллекции' => '',
//            'Туризм, экскурсии, туры' => 'Предложение услуг',
//            'Обучение и занятия' => 'Предложение услуг',
//            'Праздники' => 'Предложение услуг',
//            'Книги и печатная продукция' => 'Книги и журналы',
//            'Билеты' => 'Книги и журналы',
////            'Музыкальные инструменты' => '',
//            'Спортивные секции и клубы' => 'Предложение услуг',
//            'Спортивная одежда' => 'Одежда, обувь, аксессуары',
//            'Тренажеры, снаряды' => 'Спорт и отдых',
//            'Спортивное питание' => 'Спорт и отдых',
//            'Экстремальный спорт' => 'Спорт и отдых',
//            'Снаряжение, спецодежда' => 'Одежда, обувь, аксессуары',
//            'Гладкоствольное оружие' => 'Спорт и отдых',
//            'Нарезное оружие' => 'Спорт и отдых',
//            'Рыболовные принадлежности' => 'Спорт и отдых',
//            'Оптика, бинокли' => 'Спорт и отдых',
//            'Домашние животные, растения' => 'Животные и растения',
//        ];
//    }
}
