<?php

namespace App\Console\Commands;

use App\Ad;
use Carbon\Carbon;
use Illuminate\Console\Command;

class RefreshAllAdsCreatedTime extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ads:refresh-time';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Refreshes created_at time of all ads.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        foreach (Ad::where('author_id', null)->get() as $ad) {
            $ad->created_at = (string)Carbon::now()->subMinutes(rand(0, 60 * 24 * 5));
            $ad->save();
        }
    }
}
