<?php

namespace App\Console\Commands;

use App\AdImage;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;

class ClearUploads extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'uploads:clear';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Clear uploads directory from images unrelated to DB.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $uploadsPath = public_path('upload');
        $count = 0;

        foreach (glob($uploadsPath . '/*/*/*/*') as $imagePath) {
            $imageLocalPath = explode('public', $imagePath)[1];
            $imageLocalPath = substr($imageLocalPath, 1);
            if (!AdImage::where('big', $imageLocalPath)->orWhere('small', $imageLocalPath)->count()) {
                File::delete(public_path($imageLocalPath));
                $this->info("Deleted image: $imageLocalPath.");
                $count++;
            }
        }

        $this->info("Deleted $count images.");
    }
}
