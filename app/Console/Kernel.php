<?php

namespace App\Console;

use App\Console\Commands\ClearAdsFromOutdated;
use App\Console\Commands\RemoveExpiredPremiumAds;
use App\Console\Commands\ClearUploads;
use App\Console\Commands\RefreshAllAdsCreatedTime;
use App\Console\Commands\Parse;
use App\Console\Commands\Down;
use App\Console\Commands\CheckParser;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        \App\Console\Commands\Inspire::class,
        Parse::class,
        Down::class,
        ClearAdsFromOutdated::class,
        ClearUploads::class,
        RefreshAllAdsCreatedTime::class,
        RemoveExpiredPremiumAds::class,
        CheckParser::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('inspire')->hourly();
        $schedule->command('parse --count=1')->cron('*/2 * * * *');
//        $schedule->command('ads:clear')->daily();
        $schedule->command('premium:clear')->daily();
//        $schedule->command('parser:check')->everyTenMinutes();
    }
}
