<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AutoModel extends Model
{
    public function parent()
    {
    	return $this->belongsTo('App\AutoModel', 'make_id');
    }

    public function models()
    {
    	return $this->hasMany('App\AutoModel', 'make_id');
    }

    public function auto_extra_properties()
    {
    	return $this->hasMany('App\AutoExtraProperties', 'model_id');
    }

    public function category()
    {
        return $this->belongsTo('App\Category');
    }

    public static function allWithAdsCount()
    {
    	$makes = AutoModel::where('make_id', null)->with(['models.auto_extra_properties', 'category'])->get();

    	foreach ($makes as $make)
    	{
    		$count = 0;
    		foreach ($make->models as $model) {
    			$count += $model->auto_extra_properties->count();
    		}
    		$make->ads_count = $count;
    	}

    	return $makes->sortByDesc('ads_count');
    }

    public static function getModelsByMake($slug)
    {
        $make = AutoModel::whereSlug($slug)->orderBy('title')->first();

        return $make->models;
    }
}
