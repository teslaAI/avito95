<?php

namespace App;

use Illuminate\Contracts\Routing\UrlGenerator;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Request;

class Category extends Model
{
	/**
	 * Relationships
	 */

    public function children()
    {
    	return $this->hasMany('App\Category', 'parent_id');
    }

    public function parent()
    {
    	return $this->belongsTo('App\Category', 'parent_id');
    }

    public function ads()
    {
    	return $this->hasMany('App\Ad');
    }

    public function childrenAds()
    {
        return $this->hasManyThrough(Ad::class, Category::class, 'parent_id');
    }
    
    /**
     * Get all categories with children ordered by priority
     *
     * @return Collection
     */
	public static function getAll()
	{
        return Category::with(['children' => function($query){
            $query->orderBy('priority', 'desc');
        }])->where('parent_id', null)->orderBy('priority')->get();
	}


	/**
     * Check is the category active.
     *
     * @return boolean
     */
    public function isActive()
    {
        return $this->slug === Request::segment(2);
    }


	/**
     * Check is the category or its children active.
     *
     * @return boolean
     */
    public function isActiveOrChildren()
    {
        if ($this->isActive()) {
            return true;
        }

        foreach ($this->children as $child) {
            if ($child->isActive()) {
                return true;
            }
        }

        return false;
    }


	/**
     * Get related to the category ad's count.
     *
     * @return integer
     */
	public function adsCount()
	{
		if ($this->parent_id)
		{
			return $this->hasMany('App\Ad')->count();
		}
		else
		{
			return $this->hasManyThrough('App\Ad', 'App\Category', 'parent_id')->count();
		}
	}

    /**
     * Accessor for URL of the category
     *
     * @return UrlGenerator|string
     */
    public function getUrlAttribute()
    {
        // Todo: Automobile makes and models
        return url("category/$this->slug");
    }

    /**
     * Get all categories by discount with children ordered by priority
     *
     * @return Collection
     */
    public static function getAllByDiscount()
    {
        return \DB::table('categories')
            ->join('ads', 'categories.id', '=', 'ads.category_id')
            ->join('discounts', 'ads.id', '=', 'discounts.ad_id')
            ->where('discounts.active', 1)
            ->where('categories.name', '!=', 'Другое')
            ->groupBy('categories.name')
            ->get();
    }

    /**
     * Get related to the category ad's count.
     *
     * @return integer
     */
    public static function adsCountDiscount()
    {
        return \DB::table('ads')
            ->join('discounts', 'ads.id', '=', 'discounts.ad_id')->where('discounts.active', 1)
            ->count();
    }
}
