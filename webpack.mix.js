const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.combine([
    'resources/js/jquery-2.1.4.min.js',
    'resources/js/dropzone.js',
    'resources/js/jquery.formstyler.min.js',
    'resources/js/fotorama.js',
    'resources/js/jquery.maskedinput.min.js',
    'resources/js/script.js',
    'resources/js/main.js',
    'resources/js/pwabuilder-sw-register.js',
    'resources/js/owl.carousel.min.js',
], 'public/js/app.js')
    .sass('resources/sass/app.scss', 'public/css')
    .options({
        processCssUrls: false,
        cssNano: {
            discardComments: {
                removeAll: true,
            },
        },
    });
