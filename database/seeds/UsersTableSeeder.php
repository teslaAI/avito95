<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();
        DB::table('users')->truncate();

        DB::table('users')->insert([[
            'name' => 'Микаил',
            'email' => 'm.aziev@bk.ru',
            'password' => bcrypt('123'),
            'phone' => '+79286950155',
            'city_id' => 1,
            'phone_confirmed' => true,
            'address' => 'ул. Московская, д. 31',
            'is_admin' => true,

            'created_at' => DB::raw('CURRENT_TIMESTAMP'),
            'updated_at' => DB::raw('CURRENT_TIMESTAMP')
        ],[
            'name' => 'Берд',
            'email' => 'berd@ant.ms',
            'password' => bcrypt('123'),
            'phone' => '+79281112233',
            'city_id' => 1,
            'phone_confirmed' => true,
            'address' => 'ул. Куркиева, д. 31',
            'is_admin' => true,

            'created_at' => DB::raw('CURRENT_TIMESTAMP'),
            'updated_at' => DB::raw('CURRENT_TIMESTAMP')
        ]]);
    }
}
