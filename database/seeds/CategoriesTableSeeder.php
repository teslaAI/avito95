<?php

use App\Category;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    private $data = [
        [1, 'Электроника', 'electronics', NULL, 4],
        [2, 'Компьютеры', 'computers', 1, 3],
        [3, 'Ноутбуки', 'laptops', 1, 5],
        [4, 'Телефоны', 'phones', 1, 7],
        [5, 'Планшеты', 'tablets', 1, 6],
        [6, 'Оргтехника и расходники', 'equipment', 1, 2],
        [7, 'Комплектующие ПК', 'pc_accessories', 1, 4],
        [8, 'Фото и видео техника', 'photo_video', 1, 1],
        [9, 'Другое', 'other_electronics', 1, 0],
        [10, 'Транспорт', 'transport', NULL, 1],
        [11, 'Автомобили', 'cars', 10, 0],
        [12, 'Мотоциклы', 'motorcycles', 10, 0],
        [13, 'Грузовики и спецтехника', 'trucks_buses', 10, 0],
        [14, 'Запчасти и аксессуары', 'parts', 10, 0],
        [15, 'Недвижимость', 'realty', NULL, 3],
        [16, 'Квартиры', 'apartment', 15, 0],
        [17, 'Дома и коттеджи', 'houses', 15, 0],
        [18, 'Коммерческая недвижимость', 'commercial_realty', 15, 0],
        [19, 'Земельные участки', 'land', 15, 0],
        [20, 'Другое', 'other_realty', 15, 0],
        [21, 'Другое', 'other_transport', 10, 0],
        [22, 'Работа', 'job', NULL, 6],
        [23, 'Вакансии', 'vacancies', 22, 0],
        [24, 'Резюме', 'summary', 22, 0],
        [25, 'Услуги', 'services', NULL, 2],
        [26, 'Предложение услуг', 'offer_services', 25, 0],
        [27, 'Запросы на услуги', 'service_requests', 25, 0],
        [28, 'Бытовая техника', 'appliances', 1, 3],
        [29, 'Для дома', 'for_home', NULL, 7],
        [30, 'Мебель и интерьер', 'interior', 29, 3],
        [32, 'Продукты питания', 'food', 29, 2],
        [33, 'Животные и растения', 'animals', 29, 1],
        [34, 'Другое', 'other_home', 29, 0],
        [35, 'Ремонт и строительство', 'construction', NULL, 5],
        [36, 'Стройматериалы', 'construction_materials', 35, 0],
        [37, 'Инструменты', 'instruments', 35, 0],
        [38, 'Сантехника', 'plumbing', 35, 0],
        [39, 'Предметы обихода', 'household_items', 29, 4],
        [40, 'Отделочные материалы', 'decoration', 35, 0],
        [41, 'Садовая техника', 'gardening_equipment', 35, 0],
        [42, 'Другое', 'other_equipment', 35, 0],
        [43, 'Личные вещи', 'personal', NULL, 8],
        [44, 'Одежда, обувь, аксессуары', 'clothes', 43, 1],
        [45, 'Красота и здоровье', 'health', 43, 2],
        [46, 'Спорт и отдых', 'sports', 43, 3],
        [47, 'Часы и украшения', 'decorations', 43, 4],
        [48, 'Книги и журналы', 'books', 43, 5],
        [49, 'Другое', 'other_personal', 43, 0],
        [50, 'Для бизнеса', 'for_business', NULL, 7],
        [51, 'Готовый бизнес', 'business', 50, 3],
        [52, 'Оборудование для бизнеса', 'business_equipment', 50, 2],
        [53, 'Другое', 'other_business', 50, 1]
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->truncate();
        $now = Carbon::now();

        foreach ($this->parentCategories() as $parentCategory) {
            $categoryModel = Category::create([
                'name' => $parentCategory[1],
                'slug' => $parentCategory[2],
                'priority' => $parentCategory[4],
            ]);

            foreach ($this->childCategories($parentCategory[0]) as $childCategory) {
                Category::create([
                    'name' => $childCategory[1],
                    'slug' => $childCategory[2],
                    'parent_id' => $categoryModel->id,
                    'priority' => $childCategory[4],
                ]);
            }
        }
    }

    private function parentCategories()
    {
        return array_filter($this->data, function ($item) {
            return $item[3] === null;
        });
    }

    private function childCategories($parentNumber)
    {
        return array_filter($this->data, function ($item) use ($parentNumber) {
            return $item[3] === $parentNumber;
        });
    }
}
