<?php

use Faker\Factory as Faker;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AdsTableSeeder extends Seeder
{
    private $faker;

    private $templates;

    public function __construct()
    {
        $this->faker = Faker::create('ru_RU');
        $this->fillTemplates();
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('ads')->truncate();

        $bundle = [];
        $templatesCount = count($this->templates);

        foreach (range(0, 100) as $index) {
            $timestamp = $this->faker->dateTimeThisMonth();
            $templateKey = $index % $templatesCount;

            $bundle[] = array_merge($this->templates[$templateKey], [
                'created_at' => $timestamp,
                'updated_at' => $timestamp,
            ]);
        }

        DB::table('ads')->insert($bundle);
    }

    public function fillTemplates()
    {
        $this->templates[] = [
            'title' => 'Mitsubishi Lancer',
            'text' => 'Автомобиль приобретался в салоне официального дилера. Абсолютно новый. Заводской AMG пакет. На гарантии до 2017 года. Комбинированный салон, черный потолок, колесные диски R18. 100% Юридически чист, покупался за наличные, чеки, договор, без дтп.',
            'price' => 360000,
            'phone' => '+79286950155',
            'author_id' => 1,
            'author_name' => 'Василий',
            'category_id' => 2,
            'city_id' => 1,
        ];

        $this->templates[] = [
            'title' => 'Mersedes Bens',
            'text' => 'Автомобиль приобретался в салоне официального дилера. Абсолютно новый. Заводской AMG пакет. На гарантии до 2017 года. Комбинированный салон, черный потолок, колесные диски R18. 100% Юридически чист, покупался за наличные, чеки, договор, без дтп.',
            'price' => 432000,
            'phone' => '+79286950155',
            'author_id' => 1,
            'author_name' => 'Васёк',
            'category_id' => 3,
            'city_id' => 1,
        ];
    }
}
