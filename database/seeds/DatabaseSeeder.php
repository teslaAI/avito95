<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        $this->call(UsersTableSeeder::class);
        $this->call(CategoriesTableSeeder::class);
        $this->call(CityTableSeeder::class);
        $this->call(RegionTableSeeder::class);
        $this->call(AdsTableSeeder::class);
        $this->call(AvatarTableSeeder::class);
        $this->call(AdImageTableSeeder::class);
        $this->call(CommentTableSeeder::class);
        $this->call(AutoModelsTableSeeder::class);
        $this->call(AutoExtraPropertiesTableSeeder::class);

        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }

}

//class CategoryTableSeeder extends DatabaseSeeder{
//
//    public function run()
//    {
//        DB::table('categories')->delete();
//        DB::table('categories')->truncate();
//
//        DB::table('categories')->insert([[
//            'name' => 'Транспорт',
//            'slug' => 'transport',
//            'parent_id' => null,
//
//            'created_at' => DB::raw('CURRENT_TIMESTAMP'),
//            'updated_at' => DB::raw('CURRENT_TIMESTAMP')
//        ],[
//            'name' => 'Легковые авто',
//            'slug' => 'cars',
//            'parent_id' => 1,
//
//            'created_at' => DB::raw('CURRENT_TIMESTAMP'),
//            'updated_at' => DB::raw('CURRENT_TIMESTAMP')
//        ],[
//            'name' => 'Мототехника',
//            'slug' => 'motorcycles',
//            'parent_id' => 1,
//
//            'created_at' => DB::raw('CURRENT_TIMESTAMP'),
//            'updated_at' => DB::raw('CURRENT_TIMESTAMP')
//        ],[
//            'name' => 'Электроника',
//            'slug' => 'electronic',
//            'parent_id' => null,
//
//            'created_at' => DB::raw('CURRENT_TIMESTAMP'),
//            'updated_at' => DB::raw('CURRENT_TIMESTAMP')
//        ],[
//            'name' => 'Телефоны',
//            'slug' => 'mobiles',
//            'parent_id' => 4,
//
//            'created_at' => DB::raw('CURRENT_TIMESTAMP'),
//            'updated_at' => DB::raw('CURRENT_TIMESTAMP')
//        ]]);
//    }
//}

class CityTableSeeder extends DatabaseSeeder{

    public function run()
    {
        DB::table('cities')->delete();
        DB::table('cities')->truncate();

        DB::table('cities')->insert([[
            'name' => 'Назрань',
            'region_id' => 1,

            'created_at' => DB::raw('CURRENT_TIMESTAMP'),
            'updated_at' => DB::raw('CURRENT_TIMESTAMP')
        ],[
            'name' => 'Карабулак',
            'region_id' => 1,

            'created_at' => DB::raw('CURRENT_TIMESTAMP'),
            'updated_at' => DB::raw('CURRENT_TIMESTAMP')
        ]]);
    }
}

class RegionTableSeeder extends DatabaseSeeder{

    public function run()
    {
        DB::table('regions')->delete();
        DB::table('regions')->truncate();

        DB::table('regions')->insert([[
            'name' => 'Ингушетия',

            'created_at' => DB::raw('CURRENT_TIMESTAMP'),
            'updated_at' => DB::raw('CURRENT_TIMESTAMP')
        ]]);
    }
}

class AvatarTableSeeder extends DatabaseSeeder{

    public function run()
    {
        DB::table('avatars')->delete();
        DB::table('avatars')->truncate();

        DB::table('avatars')->insert([[
            'path' => '1.jpg',
            'user_id' => 2,
            'created_at' => DB::raw('CURRENT_TIMESTAMP'),
            'updated_at' => DB::raw('CURRENT_TIMESTAMP')
        ]]);
    }
}

class AdImageTableSeeder extends DatabaseSeeder{

    public function run()
    {
        DB::table('ad_images')->delete();
        DB::table('ad_images')->truncate();

        DB::table('ad_images')->insert([[
            'ad_id' => 1,
            'is_main' => true,
            'big' => '1.jpg',
            // 'medium' => '1.jpg',
            'small' => '1.jpg',
            'created_at' => DB::raw('CURRENT_TIMESTAMP'),
            'updated_at' => DB::raw('CURRENT_TIMESTAMP')
        ],[
            'ad_id' => 1,
            'is_main' => false,
            'big' => '1.jpg',
            // 'medium' => '1.jpg',
            'small' => '1.jpg',
            'created_at' => DB::raw('CURRENT_TIMESTAMP'),
            'updated_at' => DB::raw('CURRENT_TIMESTAMP')
        ],[
            'ad_id' => 2,
            'is_main' => true,
            'big' => '2.jpg',
            // 'medium' => '2.jpg',
            'small' => '2.jpg',
            'created_at' => DB::raw('CURRENT_TIMESTAMP'),
            'updated_at' => DB::raw('CURRENT_TIMESTAMP')
        ],[
            'ad_id' => 2,
            'is_main' => false,
            'big' => '2.jpg',
            // 'medium' => '2.jpg',
            'small' => '2.jpg',
            'created_at' => DB::raw('CURRENT_TIMESTAMP'),
            'updated_at' => DB::raw('CURRENT_TIMESTAMP')
        ]]);
    }
}

class CommentTableSeeder extends DatabaseSeeder{

    public function run()
    {
        DB::table('comments')->delete();
        DB::table('comments')->truncate();

        DB::table('comments')->insert([[
            'author_id' => 2,
            'ad_id' => 1,
            'text' => 'Ассаламу алейкум! Интересует обмен на Land Rover Discovery 3, г.2006, 4,4 (бензин)? Автомобиль на дроме не стоит. Если заинтересованны могу отправить фото.',
            'answer_to' => null,
            'created_at' => DB::raw('CURRENT_TIMESTAMP'),
            'updated_at' => DB::raw('CURRENT_TIMESTAMP')
        ],[
            'author_id' => 1,
            'ad_id' => 1,
            'text' => 'Привет Зигмунд Фрейд - австрийский психолог, психиатр и невролог и еврей. Ты наиболее известен как основатель психоанализа, который оказал значительное влияние на психологию, медицину, социологию.',
            'answer_to' => 1,
            'created_at' => DB::raw('CURRENT_TIMESTAMP'),
            'updated_at' => DB::raw('CURRENT_TIMESTAMP')
        ],[
            'author_id' => 1,
            'ad_id' => 2,
            'text' => 'Ассаламу алейкум! Eсли заинтересованны могу отправить фото.',
            'answer_to' => null,
            'created_at' => DB::raw('CURRENT_TIMESTAMP'),
            'updated_at' => DB::raw('CURRENT_TIMESTAMP')
        ]]);
    }
}

class AutoExtraPropertiesTableSeeder extends DatabaseSeeder{

    public function run()
    {
        DB::table('auto_extra_properties')->delete();
        DB::table('auto_extra_properties')->truncate();

        DB::table('auto_extra_properties')->insert([[
            'ad_id' => 1,
            'year' => 2004,
            'capacity' => 2.4,
            'power' => 280,
            'engine_type' => 'бензин',
            'transmission' => 'автомат',
            'mileage' => 150400,
            'model_id' => 2
        ]]);
    }
}

class AutoModelsTableSeeder extends DatabaseSeeder{

    public function run()
    {
        DB::table('auto_models')->delete();
        DB::table('auto_models')->truncate();

        DB::table('auto_models')->insert([[
            'title' => 'BMW',
            'slug' => 'bmw',
            'category_id' => 2,
            'make_id' => null
        ],[
            'title' => 'M5',
            'slug' => 'm5',
            'make_id' => 1,
            'category_id' => null
        ]]);
    }
}
