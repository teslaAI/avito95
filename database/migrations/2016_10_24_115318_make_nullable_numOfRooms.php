<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MakeNullableNumOfRooms extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('realty_extra_properties', function (Blueprint $table) {
            $table->string('num_of_rooms')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('realty_extra_properties', function (Blueprint $table) {
            $table->string('num_of_rooms')->change();
        });
    }
}
