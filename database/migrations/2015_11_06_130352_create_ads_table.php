<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ads', function (Blueprint $table) {
            $table->increments('id');

            $table->string('title');
            $table->text('text');
            $table->integer('price')->unsigned();
            $table->string('phone', 20);
            $table->integer('views')->unsigned()->default(0);
            $table->integer('author_id')->unsigned()->nullable();
            $table->string('author_name');
            $table->integer('category_id')->unsigned();
            $table->boolean('is_premium')->default(null);
            $table->integer('city_id')->unsigned();

            // $table->timestamp('refreshed_at');
            $table->timestamps();

            $table->foreign('author_id')->references('id')->on('users');
            $table->foreign('city_id')->references('id')->on('cities');
            $table->foreign('category_id')->references('id')->on('categories');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ads');
    }
}
