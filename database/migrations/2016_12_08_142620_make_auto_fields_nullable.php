<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MakeAutoFieldsNullable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('auto_extra_properties', function (Blueprint $table) {

            $table->float('capacity')->nullable()->change();
            $table->integer('power')->nullable()->change();
            $table->string('transmission')->nullable()->change();
            $table->integer('mileage')->nullable()->change();
            $table->string('engine_type')->nullable()->change();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('auto_extra_properties', function (Blueprint $table) {
            
            $table->float('capacity')->change();
            $table->integer('power')->change();
            $table->string('transmission')->change();
            $table->integer('mileage')->change();
            $table->string('engine_type')->change();

        });
    }
}
