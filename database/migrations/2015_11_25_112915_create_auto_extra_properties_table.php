<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAutoExtraPropertiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('auto_extra_properties', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('ad_id')->unsigned();

            $table->integer('year');
            $table->float('capacity'); // объем
            $table->integer('power');
            $table->string('transmission');
            $table->integer('mileage'); // пробег
            $table->string('engine_type');

            $table->integer('model_id')->unsigned();

            $table->timestamps();

            $table->foreign('ad_id')->references('id')->on('ads')->onDelete('cascade');
            $table->foreign('model_id')->references('id')->on('auto_models');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('auto_extra_properties');
    }
}
