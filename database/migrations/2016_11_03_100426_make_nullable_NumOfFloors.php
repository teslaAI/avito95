<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MakeNullableNumOfFloors extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('realty_extra_properties', function (Blueprint $table) {
            $table->string('num_of_floors')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('realty_extra_properties', function (Blueprint $table) {
            $table->string('num_of_floors')->change();
        });
    }
}
