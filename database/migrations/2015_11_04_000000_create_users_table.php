<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email')->unique()->nullable();
            $table->string('password', 60);
            $table->string('phone', 20)->unique();
            // $table->integer('avatar_id')->unsigned()->nullable();
            $table->integer('city_id')->unsigned()->nullable();
            $table->string('address');
            $table->boolean('is_admin');

            $table->rememberToken();
            $table->timestamps();

            // $table->foreign('avatar_id')->references('id')->on('avatars');
            $table->foreign('city_id')->references('id')->on('cities');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
