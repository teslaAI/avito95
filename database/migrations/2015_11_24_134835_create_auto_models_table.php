<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAutoModelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('auto_models', function (Blueprint $table) {
            $table->increments('id');

            $table->string('title');
            $table->integer('make_id')->nullable()->unsigned();
            $table->integer('category_id')->unsigned()->nullable();
            $table->string('slug');

            $table->timestamps();

            $table->foreign('make_id')->references('id')->on('auto_models')->onDelete('cascade');
            $table->foreign('category_id')->references('id')->on('categories');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('auto_models');
    }
}
