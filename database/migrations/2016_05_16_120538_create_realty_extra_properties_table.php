<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRealtyExtraPropertiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('realty_extra_properties', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('ad_id')->unsigned();
            $table->string('type');
            $table->string('lease'); // срок аренды
            $table->string('num_of_rooms');
            $table->integer('square');
            $table->integer('floor');
            $table->integer('num_of_floors');

            $table->timestamps();

            $table->foreign('ad_id')->references('id')->on('ads')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('realty_extra_properties');
    }
}
