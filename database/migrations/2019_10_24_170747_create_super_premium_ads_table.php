<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSuperPremiumAdsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('super_premium_ads', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('ad_id');
            $table->text('message');
            $table->string('area');
            $table->integer('price')->unsigned();
            $table->boolean('paid')->default(0);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('super_premium_ads');
    }
}
