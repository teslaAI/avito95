<?php

return [
	'title' => 'Auto models',
	'single' => 'auto model',
	'model' => 'App\AutoModel',
	'columns' => [
		'id',
		'title',
		'slug',
		'make_id' => [
			'title' => 'make',
			'relationship' => 'parent',
			'select' => '(:table).title'
		],
		'category_id' => [
			'title' => 'category',
			'relationship' => 'category',
			'select' => '(:table).name'
		]
	],
	'edit_fields' => [
		'title',
		'slug',
		'parent' => [
			'type' => 'relationship',
			'title' => 'make',
			'name_field' => 'title',
			'options_filter' => function($query){
				$query->where('make_id', null);
			}
		],
		'category' => [
			'type' => 'relationship',
			'title' => 'category',
			'name_field' => 'name',
			'options_filter' => function($query){
				$query->whereHas('parent', function($query){
					$query->where('slug', 'transport');
				});
			}
		]
	],
];