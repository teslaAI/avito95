<?php

return [
    'title' => 'Prepayment',
    'single' => 'prepayment',
    'model' => 'App\PrepaidDiscount',
    'query_filter'=> function($query) {
        $query->where('paid', 1);
    },
    'columns' => [
        'id',
        'title' => array(
            'title' => 'title',
            'relationship' => 'ad',
            'select' => "title",
        ),
        'ad_id',
        'author_name' => array(
            'title' => 'name',
            'relationship' => 'user',
            'select' => "name",
        ),
        'paid',
        'phone' => array(
            'title' => 'phone',
            'relationship' => 'user',
            'select' => "phone",
        ),
        'created_at'
    ],
    'edit_fields' => [
        'id',
        'ad_id',
        'user_id',
        'paid'
    ],
];