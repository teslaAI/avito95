<?php

return [
	'title' => 'Comments',
	'single' => 'comment',
	'model' => 'App\Comment',
	'columns' => [
		'id',
		'author_id',
		'ad_id',
		'answer_to',
		'is_private',
		'is_viewed',
		'text',
	],
	'edit_fields' => [
		'text' => [
			'type' => 'textarea'
		],
	],
];