<?php

return [
	'title' => 'Users',
	'single' => 'user',
	'model' => 'App\User',
	'columns' => [
		'id',
		'name',
		'email',
		'phone',
		'address',
		'is_admin',
		'phone_confirmed'
	],
	'edit_fields' => [
		'name' => [
			'type' => 'text'
		],
		'email' => [
			'type' => 'text'
		],
		'phone' => [
			'type' => 'text'
		],
		'password' => [
			'type' => 'text'
		],
		'address' => [
			'type' => 'text'
		],
		'is_admin' => [
			'type' => 'bool'
		],
		'phone_confirmed' => [
			'type' => 'bool'
		]
	],
];