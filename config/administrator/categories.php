<?php

return [
	'title' => 'Categories',
	'single' => 'category',
	'model' => 'App\Category',
	'columns' => [
		'id',
		'name',
		'slug',
		'parent_id',
		'priority'
	],
	'edit_fields' => [
		'name',
		'slug',
		'priority' => [
			'type' => 'number'
		],
		'parent' => [
			'type' => 'relationship',
			'title' => 'parent_category',
			'name_field' => 'name',
			'options_filter' => function($query) {
				$query->where('parent_id', null);
			}
		]
	],
];