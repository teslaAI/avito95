<?php

return [
    'title' => 'Discount',
    'single' => 'discount',
    'model' => 'App\Discount',
    'columns' => [
        'id',
        'title' => array(
            'title' => 'title',
            'relationship' => 'ad',
            'select' => "title",
        ),
        'ad_id',
        'prepayment',
        'discount',
        'active',
        'author_name' => array(
            'title' => 'author_name',
            'relationship' => 'ad',
            'select' => "author_name",
        ),
        'phone' => array(
            'title' => 'phone',
            'relationship' => 'ad',
            'select' => "phone",
        )
    ],
    'edit_fields' => [
        'id',
        'ad_id',
        'discount',
        'prepayment',
        'active' => [
            'type' => 'bool'
        ],
    ],
];