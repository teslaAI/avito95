<?php

return [
	'title' => 'Regions',
	'single' => 'region',
	'model' => 'App\Region',
	'columns' => [
		'id',
		'name'
	],
	'edit_fields' => [
		'name'
	],
];