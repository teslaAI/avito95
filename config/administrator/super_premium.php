<?php

return [
    'title' => 'Super premium',
    'single' => 'super premium',
    'model' => 'App\SuperPremiumAd',
    'query_filter'=> function($query) {
        $query->where('paid', 1);
    },
    'columns' => [
        'id',
        'title' => array(
            'title' => 'title',
            'relationship' => 'ad',
            'select' => "title",
        ),
        'message',
        'ad_id',
        'area',
        'price',
        'author_name' => array(
            'title' => 'author_name',
            'relationship' => 'ad',
            'select' => "author_name",
        ),
        'phone' => array(
            'title' => 'phone',
            'relationship' => 'ad',
            'select' => "phone",
        )
    ],
    'edit_fields' => [
        'id',
        'message' => [
            'type' => 'textarea'
        ],
        'ad_id',
        'area',
        'price'
    ],
];