<?php

return [
	'title' => 'Ads',
	'single' => 'ad',
	'model' => 'App\Ad',
    'filters' => array(
        'title' => array(
            'title' => 'Заголовок',
        ),
        'phone' => array(
            'title' => 'Телефон',
        ),
        'author_name' => array(
            'title' => 'Имя автора',
        )
    ),
	'columns' => [
		'id',
		'title',
		'text',
		'category_id',
		'price',
		'phone',
		'views',
        'author_name',
		'author_id',
        'is_premium',
        'is_contest',
        'is_top'
	],
	'edit_fields' => [
		'title',
		'text' => [
			'type' => 'textarea'
		],
		'price',
		'phone',
		'category' => [
			'type' => 'relationship',
			'title' => 'category',
			'name_field' => 'name',
			'options_filter' => function($query){
				$query->where('parent_id', '!=', 'null');
			}
		],
		'author_name',
		'city' => [
			'type' => 'relationship',
			'title' => 'city',
			'name_field' => 'name'
		],
        'is_contest' => [
            'type' => 'bool'
        ],
        'is_top' => [
            'type' => 'bool'
        ],
        'is_premium' => [
            'type' => 'bool'
        ],
        'premium_date' => [
            'type' => 'datetime',
            'date_format' => 'yy-mm-dd',
            'time_format' => 'HH:mm',
        ],
	],
];