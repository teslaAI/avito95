<?php

return [
	'title' => 'Cities',
	'single' => 'city',
	'model' => 'App\City',
	'columns' => [
		'id',
		'name',
		'region_id' => [
			'title' => 'region',
			'relationship' => 'region',
			'select' => '(:table).name'
		]
	],
	'edit_fields' => [
		'name',
		'region' => [
			'type' => 'relationship',
			'title' => 'region',
			'name_field' => 'name'
		]
	],
];