<?php

return [
    'title' => 'Referrals',
    'single' => 'referral',
    'model' => 'App\User',
    'query_filter'=> function($query) {
        $query->orderBy('referral_count', 'desc');
    },
    'columns' => [
        'id',
        'name',
        'phone',
        'email',
        'referral_count'
    ],
    'edit_fields' => [
        'name',
        'phone',
        'email'
    ],
];