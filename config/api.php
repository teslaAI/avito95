<?php
return [
    /*
    |--------------------------------------------------------------------------
    | Login And Password For Robokassa
    |--------------------------------------------------------------------------
    */
    'robokassa_login' => env('ROBOKASSA_LOGIN'),
    'robokassa_password' => env('ROBOKASSA_PASSWORD'),
    'robokassa_password_2' => env('ROBOKASSA_PASSWORD_2')
];