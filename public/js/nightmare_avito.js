var Xvfb = require('xvfb');
const Nightmare = require("nightmare");

var xvfb = new Xvfb({
    silent: true
});

xvfb.start();

var proxies = [
    '81.163.57.147:41258',
    '176.113.126.127:39921',
    '77.232.167.200:31513',
    '185.17.133.107:8080',
    '185.17.134.149:60964'
];

const nightmare = Nightmare({
    show: true,
    switches: {
        'ignore-certificate-errors': true,
        // 'proxy-server': proxies[Math.floor(Math.random() * 5)],
        'proxy-server': '92.249.122.108:61778'
    }
});

const argv = require('minimist')(process.argv.slice(2));

nightmare
    .useragent('Mozilla/5.0 (Linux; Android 5.0; SM-G900P Build/LRX21T) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Mobile Safari/537.36')
    .header(':authority', 'm.avito.ru')
    .header(':method', 'GET')
    .header(':path', '/chechenskaya_respublika')
    .header(':scheme', 'https')
    .header('accept', 'application/json, text/plain, */*')
    .header('accept-encoding', 'gzip, deflate, br')
    .header('accept-language', 'en,ru;q=0.9,it;q=0.8,uk;q=0.7,kk;q=0.6,en-US;q=0.5,la;q=0.4')
    .header('cache-control', 'no-cache')
    .header('content-type', 'application/json;charset=utf-8')
    .header('cookie', 'u=2fr5nnza.lgjhul.g878wr5ijz; _ym_uid=157174695067376488; _ym_d=1571746950; _ga=GA1.2.1763359959.1571746950; __gads=ID=8ffdbe844629d5d2:T=1571746990:S=ALNI_MZJ9pbUlO9F0VXRT233TOEbq9_qIw; buyer_selected_search_radius4=0_general; __cfduid=dd34957c486283ffbc1a665f79f5992c11571907851; buyer_location_id=660711; _fbp=fb.1.1576488121689.30961478; _ga=GA1.3.1763359959.1571746950; __utmz=99926606.1581341943.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none); __utma=99926606.1763359959.1571746950.1581341943.1581341942.1; sessid=9a845be24ca93561ba7b6415a571faa1.1581668472; luri=chechenskaya_respublika; sx=H4sIAAAAAAACAxXKMQ6AIAwAwL90digGpPIbLQZDI50EDeHvxpuvw7ovG7XnzbdVloaMmpRRCEKHCgGyjyXWi8rJImyTVcI%2FCLFoSjDBAcE4Mt5Z8vMYH0bRu1JUAAAA; dfp_group=25; f=5.0c4f4b6d233fb90636b4dd61b04726f147e1eada7172e06c47e1eada7172e06c47e1eada7172e06c47e1eada7172e06cb59320d6eb6303c1b59320d6eb6303c1b59320d6eb6303c147e1eada7172e06c8a38e2c5b3e08b898a38e2c5b3e08b890df103df0c26013a0df103df0c26013a2ebf3cb6fd35a0ac0df103df0c26013a8b1472fe2f9ba6b984dcacfe8ebe897b0690af5b7632e5fa1a979fbaa79957605050a556a35c540af0c77052689da50d0df103df0c26013aba0ac8037e2b74f971e7cb57bbcb8e0f03c77801b122405c8b1472fe2f9ba6b91d6703cbe432bc2a71e7cb57bbcb8e0f03c77801b122405c2da10fb74cac1eab2da10fb74cac1eab2ebf3cb6fd35a0ac20f3d16ad0b1c546fa967f4e8cd565314e1848a6933553220823d89c0e0d484f420d66e83a34e2965e61d702b2ac73f7914425b4794c130a1b8695b8de9d77be08237309c2986ef2e58627782d40d5af8732de926882853a9d9b2ff8011cc827c4d07ec9665f0b70915ac1de0d034112fd6409484b56ca8dd1f40e1eb99caef32da10fb74cac1eab2da10fb74cac1eabebbe6c23a7edab1a; _ym_visorc_34241905=b; _ym_isad=1; _ym_visorc_188382=w; _gid=GA1.2.2010706891.1581668478; _ym_visorc_106253=w; dfp_group=48; _mlocation=621540; _mlocation_mode=default; _ym_visorc_24618824=b; _ym_visorc_125618=w; _gid=GA1.3.2010706891.1581668478')
    .header('sec-ch-ua', 'Google Chrome 79')
    .header('sec-fetch-dest', 'document')
    .header('sec-fetch-mode', 'navigate')
    .header('sec-fetch-site', 'same-origin')
    .header('sec-fetch-user', '?1')
    .header('sec-origin-policy', '0')
    .header('upgrade-insecure-requests', '1')
    .header('x-compress', 'null')


    .goto(argv.url)
    .wait('._39k-I')
    .click('._39k-I')
    .wait('._3Ryy-')
    .evaluate(() => document.querySelector("._3Ryy-").href)
    .end()
    .then(function (result) {
            console.log(result);
            xvfb.stop();
        }
    )
    .catch((error) => {
        console.error('Search failed:', error);
        xvfb.stop();
    });