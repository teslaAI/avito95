var Xvfb = require('xvfb');
const Nightmare = require("nightmare");

var xvfb = new Xvfb({
    silent: true
});

xvfb.start();

var proxies = [
    '81.163.57.147:41258',
    '176.113.126.127:39921',
    '77.232.167.200:31513',
    '185.17.133.107:8080',
    '185.17.134.149:60964'
]

const nightmare = Nightmare({
    show: true,
    switches: {
        'ignore-certificate-errors': true,
       // 'proxy-server': proxies[Math.floor(Math.random() * 4)],
     	'proxy-server': '178.47.251.170:8080'
    }
});

const argv = require('minimist')(process.argv.slice(2));

nightmare
    .useragent('Mozilla/5.0 (Linux; Android 5.0; SM-G900P Build/LRX21T) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.132 Mobile Safari/537.36')
    .header(':authority', 'm.avito.ru')
    .header(':method', 'GET')
    .header(':path', '/chechenskaya_respublika')
    .header(':scheme', 'https')
    .header('accept', 'application/json, text/plain, */*')
    .header('accept-encoding', 'gzip, deflate, br')
    .header('accept-language', 'en,ru;q=0.9,it;q=0.8,uk;q=0.7,kk;q=0.6,en-US;q=0.5,la;q=0.4')
    .header('cache-control', 'no-cache')
    .header('content-type', 'application/json;charset=utf-8')
    .header('cookie', 'u=2fr5nnza.lgjhul.g878wr5ijz; _ym_d=1571746950; _ym_uid=157174695067376488; _ga=GA1.2.1763359959.1571746950; __gads=ID=8ffdbe844629d5d2:T=1571746990:S=ALNI_MZJ9pbUlO9F0VXRT233TOEbq9_qIw; buyer_selected_search_radius4=0_general; __cfduid=dd34957c486283ffbc1a665f79f5992c11571907851; _fbp=fb.1.1576488121689.30961478; _ga=GA1.3.1763359959.1571746950; __utmz=99926606.1581341943.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none); __utma=99926606.1763359959.1571746950.1581341943.1581341942.1; buyer_location_id=653391; luri=groznyy; buyer_selected_search_radius2=0_job; sx=H4sIAAAAAAACAw3KQQqAIBAF0Lv8dYupJhy9TVCMMAsNITHx7rl7i9dxlpybz9e9m6XKJElIlUUQOl4ERN%2B0fO2RWNWYWJORpfkmjBULboT1EN6cd07G%2BAH0N%2BOXVAAAAA%3D%3D; dfp_group=87; sessid=8ae1c926da86c0372c25a755f1ee9347.1584193378; f=5.0c4f4b6d233fb90636b4dd61b04726f147e1eada7172e06c47e1eada7172e06c47e1eada7172e06c47e1eada7172e06cb59320d6eb6303c1b59320d6eb6303c1b59320d6eb6303c147e1eada7172e06c8a38e2c5b3e08b898a38e2c5b3e08b890df103df0c26013a0df103df0c26013a2ebf3cb6fd35a0ac0df103df0c26013a8b1472fe2f9ba6b984dcacfe8ebe897b0690af5b7632e5fa1a979fbaa79957605050a556a35c540a2da10fb74cac1eab2da10fb74cac1eab2da10fb74cac1eab2da10fb74cac1eab2da10fb74cac1eab2da10fb74cac1eab2da10fb74cac1eab2da10fb74cac1eab2da10fb74cac1eab2da10fb74cac1eab2da10fb74cac1eab2da10fb74cac1eab2da10fb74cac1eabfa967f4e8cd565314e1848a6933553228d12bc8949c6e98cb90be192ad50e648872f459da72136b353a361c4de0ffdee68b89b894d078180687ef3684770b88fb365f56e92c925c5f6d90886864009ff5dcded8022a3c9fccbf1a5019b899285164b09365f5308e731f2b7daefd09fa06c2d76bb20417a292da10fb74cac1eab2da10fb74cac1eab3069315ebaf9ae7f8012e98924060d02; _ym_isad=1; _gid=GA1.2.496962925.1584193383; dfp_group=12; _mlocation=621540; _mlocation_mode=default; v=1584195206; _ym_visorc_24618824=b')
    .header('sec-ch-ua', 'Google Chrome 80')
    .header('sec-fetch-dest', 'document')
    .header('sec-fetch-mode', 'navigate')
    .header('sec-fetch-site', 'same-origin')
    .header('sec-fetch-user', '?1')
    .header('sec-origin-policy', '0')
    .header('upgrade-insecure-requests', '1')
    .header('x-compress', 'null')


    .goto('https://m.avito.ru/oyshara/mebel_i_interer/stol_s_stulyami_1885935446')
    .wait('._39k-I')
    .click('._39k-I')
    .wait('._3Ryy-')
    .evaluate(() => document.querySelector("._3Ryy-").href)
    .end()
    .then(function (result) {
            console.log(result);
            xvfb.stop();
        }
    )
    .catch((error) => {
        console.error('Search failed:', error);
        xvfb.stop();
    });