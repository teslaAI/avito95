$(window).load(function(){
		var leftblock = $('.slider').outerHeight() + $('.item-comments').outerHeight()
		var rightblock = $('.item-desc').outerHeight()

		if (leftblock < rightblock) {
			$('.item-related').css('clear','both')
		}
});

$.fn.scrollBottom = function() { 
  return $(document).height() - this.scrollTop() - this.height(); 
};

$(document).ready(function(){

	$('input, select').styler({
		selectSearch: true
	});

	$('.sign-in a, #open-popup, #pp-open-popup').on('click', function(){
		$('.popup').addClass('active')
		return false
	})

	$('.popup').on('click', function(e){
		if( $(e.target).hasClass('popup') )
		{
			$(this).removeClass('active')
		}
	})

    $('#discount-open-popup').on('click', function(){
        $('.discount-popup').addClass('active')
        return false
    })

    $('.discount-popup').on('click', function(e){
        if( $(e.target).hasClass('discount-popup') )
        {
            $(this).removeClass('active')
        }
    })

	$('.scroll-top').on('click', function(){
    	$('html, body').animate({
            scrollTop: $("html").offset().top
        });
    })

    $menu = $('.menu');

	$(window).on('scroll', function(){
	    if ($(window).scrollTop() > 500)
		    {
		    	$('.scroll-top').addClass('scroll-top-visible');
		    }else{
		    	$('.scroll-top').removeClass('scroll-top-visible');
		    }
		if ($(window).scrollBottom() < 60)
		    {
		    	$('.scroll-top').addClass('scroll-top-footer');
		    }else{
		    	$('.scroll-top').removeClass('scroll-top-footer');
		    }
	}).scroll()

	$(window).on('scroll touchmove', function () {
		if ($(window).scrollTop() > 60) {
			$menu.addClass('scrolled');
		} else {
			$menu.removeClass('scrolled');
		}
	}).scroll()

	$('#registr').on('click', function(){
		$(this).addClass('active')
		$('#login').removeClass('active')
		$('.registration').css('display','block')
		$('.forms').addClass('forms-660')
		$('.forms').removeClass('forms-0')
	})

	$('#login').on('click', function(){
		$(this).addClass('active')
		$('#registr').removeClass('active')
		$('.forms').removeClass('forms-660').removeClass('forms-0')
		$('.registration').css('display','none')
		$('.login').css('display','block') 
	})

	$('.pass-forget').on('click', function(){
		$('#registr').removeClass('active')
		$('#login').removeClass('active')
		$('.forms').removeClass('forms-660')
		$('.forms').addClass('forms-0')
		$('.login').css('display','none') 
		$('.registration').css('display','none')
	})

	$('.nav .nav-item').on('click', function(){
		$(this).toggleClass('active')
		$(this).siblings('.nav .nav-item').removeClass('active')
	})

	$('.submenu li').on('click', function(){
		$(this).toggleClass('active2')
		$(this).siblings('.submenu li').removeClass('active2')
	})

	$('.h-avatar').on('mouseenter', function(){
		$(this).addClass('active')
	}).on('mouseleave', function(){
		$(this).removeClass('active')
	})

	$('.touch').on('touchstart', function(){
		$('.h-avatar').toggleClass('active')
	})

	$('body').on('click', function(e){
		$('.f-tooltip').remove()
		$('.mob-menu').removeClass('active')
    	$('.col-1').removeClass('left250')
	})

	$('.filter').on('click', '.show-all', function(){
		$('.all-brands').slideToggle(100)
		if ($(this).text() == "Показать все") {
			$(this).text('Популярные').fadeIn(100)
		}else{
			$(this).text('Показать все').fadeIn(100)
		}
		return false;
	})

    var $itemFilter = $('.item-filter');
	$('#expand_filters').on('click', function(){
        if ($(this).hasClass('active')) {
			$(this).removeClass('active');
            $itemFilter.slideUp(200);
		} else {
            $(this).addClass('active');
            $itemFilter.find('input, select').trigger('refresh');
            $itemFilter.slideDown(200);
		}
	});

	$(".number").keydown(function(event) {
        if ( event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || 
            (event.keyCode == 65 && event.ctrlKey === true) || 
            (event.keyCode >= 35 && event.keyCode <= 39)) {
                return
        }
        else {
            if ((event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
                event.preventDefault()
            }   
        }
    })

    $(".back-btn").on('click', function(e){
    	e.stopPropagation()
    })

    $(".mob-menu-btn").on('click', function(e){
        e.stopImmediatePropagation()
        $('.mob-menu').toggleClass('active')
        $('.col-1').toggleClass('left250')
    })

    $('.col-1').on('click', function(f){
		f.stopImmediatePropagation()
    })

    $('#free').on('click', function(){
    	if ($("#free input").prop("checked")) {
    		$('#price').val('').attr('disabled', true)
    	}else{
    		$('#price').attr('disabled', false)
    	}
    })
    if ($("#free input").prop("checked"))
    {
		$('#price').val('').attr('disabled', true)
	}
	else
	{
		$('#price').attr('disabled', false)
	}


	$('#filters_is_free').on('click', function(){
    	if ($("#filters_is_free input").prop("checked")) {
    		$('#price1, #price2').val('').attr('disabled', true)
    	}else{
    		$('#price1, #price2').attr('disabled', false)
    	}
    })
    if ($("#filters_is_free input").prop("checked")) {
		$('#price1, #price2').val('').attr('disabled', true)
	}else{
		$('#price1, #price2').attr('disabled', false)
	}

    if ($(window).width() > 801 && Cookies.get('active_layout') == null) {
        Cookies.set('active_layout', 'list')
    }

    $(".grid-btn").on('click', function(e){
        e.stopImmediatePropagation()
        $('.list').addClass('grid')
        $('.grid-btn').addClass('active')
        $('.list-btn').removeClass('active')

        Cookies.set('active_layout', 'grid')
    })

    $(".list-btn").on('click', function(e){
        e.stopImmediatePropagation()
        $('.list').removeClass('grid')
        $('.list-btn').addClass('active')
        $('.grid-btn').removeClass('active')

        Cookies.set('active_layout', 'list')
    })

    if (Cookies.get('active_layout') == 'list') {
        $('.list').removeClass('grid')
        $('.list-btn').addClass('active')
        $('.grid-btn').removeClass('active')
    }

    var prevScrollpos = window.pageYOffset;
    var element = document.getElementById("add-mobile");
    if (element) {
        function showHideAdButton() {
            var currentScrollPos = window.pageYOffset;
            if (prevScrollpos > currentScrollPos) {
                document.getElementById("add-mobile").style.bottom = "20px";
            } else {
                document.getElementById("add-mobile").style.bottom = "-50px";
            }
            prevScrollpos = currentScrollPos;
        }
	}

    window.addEventListener('scroll', showHideAdButton);

    // Set links to visited
    function setVisited() {
        let url = window.location.href.split('?')[1];

        if (url != 'filter=discount') {
            if (!localStorage.visited) {
                localStorage.setItem("visited", "[]");
            } else {
                var visited = JSON.parse(localStorage.getItem("visited"));
                visited.forEach(function(link, index) {
                    $("a.ad-image-link[href$='" + link + "']").addClass("visited");
                });
            };
        }

        $("a.ad-image-link").on("click", function(e) {
            if (!/visited/.test(e.target.parentNode.className)) {
                var visits = JSON.parse(localStorage.getItem("visited"));
				visits.push(this.href);
				localStorage.setItem("visited", JSON.stringify(visits));
            }
        });

        $(".list-item-title a").on("click", function(e) {
            if (!/visited/.test(e.target.parentNode.className)) {
                var visits = JSON.parse(localStorage.getItem("visited"));
                visits.push(this.href);
                localStorage.setItem("visited", JSON.stringify(visits));
            }
        });
    }

    setVisited()

    $( document ).ajaxStop(function() {
        setVisited();
    });

    // function go_to_ad() {
    //     $(".content .premium .ad-image-link").on("click", function(e) {
    //         e.preventDefault();
    //         var id = this.parentNode.id.split('_')[1];
    //         var prev_id = this.parentNode.previousSibling.previousSibling.id.split('_')[1];
    //         window.location.href = '/ad/'+id+'?prev_ad_id='+prev_id;
    //     });
    //
    //     $(".content .premium .list-item-title a").on("click", function(e) {
    //         e.preventDefault();
    //         var id = this.parentNode.parentNode.parentNode.parentNode.id.split('_')[1];
    //         var prev_id = this.parentNode.parentNode.parentNode.parentNode.previousSibling.previousSibling.id.split('_')[1];
    //         window.location.href = '/ad/'+id+'?prev_ad_id='+prev_id;
    //     });
    // }
    //
    //
    // go_to_ad();

    // $( document ).ajaxStop(function() {
    //     go_to_ad();
    // });


    var lazyLoadInstance = new LazyLoad({
        elements_selector: ".lazy"
        // ... more custom settings?
    });

    $(".button-download").on('click', function(){
        $('.application').addClass('hidden')
        Cookies.set('app_download', 'closed', { expires: 700 })
    })

    $(".button-close").on('click', function(e){
        e.preventDefault()
        $('.application').addClass('hidden')
        Cookies.set('app_download', 'closed', { expires: 1/24 })
    })

    if (!Cookies.get('app_download')) {
        $('.application').removeClass('hidden')
    }

    $('.owl-carousel').owlCarousel({
        margin: 30,
        nav: true,
        dots: false,
        autoWidth: true,
    });

})