var autoCategories = ['cars', 'motorcycles', 'transport'];
var realtyCategories = ['apartment', 'houses', 'commercial_realty', 'land'];
var clothesCategories = ['clothes']

$(document).ready(function(){

	$('.item-phone a').on('click', function(){
		var parentElem = $(this).parent()
		var val = parentElem.data('value')
		var root = parentElem.data('root')
		$.ajax({
			type: 'GET',
			url: root + '/decode',
			data: {
				value: val
			},
			beforeSend: function(){},
			success: function(data){
				parentElem.html('<a href="tel:'+ data +'">'+ data +'</a>')
			},
			error: function(data){
				parentElem.html('Ошибка загрузки номера')
			},
			complete: function(){}
		})
		return false
	})

	$('#comments-block').on('click', '.replay', function(){
		var $form = $('.comment-form-hided')
		var $headline = $('.comment-form-h-hided')
		var comment_id = $(this).closest('.comment').data('id')
		var name = $(this).closest('.comment').find('.comment-author').text()

		$form.find('input[name=answer_to]').remove().end().append('<input type="hidden" name="answer_to" value="'+ comment_id +'">')

		$headline.find('#appended_username').remove().end().append('<span id="appended_username"> пользователю ' + name + '</span>')

		if ($form.css('display') == 'none')
		{
			$form.show()
			$headline.show()
		}
		$('#comment-textarea').focus()
		return false
	})

	$('.comment-form-h-hided, .comment-form-hided').hide()

	$('.comment-form').submit( function(e){
		var data = $(this).serialize()
		var url = $(this).attr('action')

		var $form = $(this)

		$.ajax({
            type: 'GET',
            url: url,
            data: data,
            beforeSend: function(data) {
                $form.find('input[type="submit"]').attr('disabled', 'disabled');
            },
            success: function(data){
            	$('#comments-block').fadeOut(200, function(){
            		$('#comments-block').html(data).fadeIn(200)
            	})
            	$form[0].reset()
            },
            error: function (xhr, ajaxOptions, thrownError) {
            	alert(xhr.status); // ответ сервера
                alert(thrownError); // текст ошибки
            },
            complete: function(data) {
               $form.find('input[type="submit"]').prop('disabled', false);
            }
        })

		e.preventDefault()
	})

	$('#comments-block').on('click', '.comment-remove', function(e){
		var url = $(this).attr('href')

		$.ajax({
			type: 'GET',
			url: url,
			success: function(data){
				$('#comments-block').fadeOut(200, function(){
            		$('#comments-block').html(data).fadeIn(200)
            	})
			}
		})

		e.preventDefault()
	})

	$('#category').on('change', function(){
		var choosedCat = $(this).find('option[value='+ $(this).val() +']').data('slug');
		var url = $(this).data('extraurl');
		var ad_id = $(this).data('id');

		if ( $.inArray(choosedCat, autoCategories) != -1){

			url += '/auto';
			if (ad_id == '')
			{
				ad_id = '';
			}else{
				url += '/' + ad_id;
			}

			url += location.search

			// console.log(data)

			$.ajax({
				type: 'GET',
				url: url,
				// data: data,
				beforeSend: function(){
					$('.publish-loader').slideDown(200);
				},
				success: function(data){
					$('.create-ad-adv').remove()
					$('.publish-loader').after(data).fadeOut(0).fadeIn(200)
					$('.create-ad-adv').find('select').styler()

					$('#title').hide(0).attr('required', false)
					$('label[for=title]').hide(0)
					$('#publish_make').change();
				},
				complete: function(){
					$('.publish-loader').slideUp(200);
				},
			})
		}
		else if ($.inArray(choosedCat, realtyCategories) != -1)
		{
			url += '/realty';

			if (ad_id == '')
			{
				ad_id = '';
			}else{
				url += '/' + ad_id;
			}

			url += '?category=' + choosedCat;

			$.ajax({
				type: 'GET',
				url: url,
				beforeSend: function(){
					$('.publish-loader').slideDown(200);
				},
				success: function(data){
					$('.create-ad-adv').remove()
					$('.publish-loader').after(data).fadeOut(0).fadeIn(200)
					$('.create-ad-adv').find('select').styler()

					$('#title').hide(0).attr('required', false)
					$('label[for=title]').hide(0)
					$('#publish_make').change();
				},
				complete: function(){
					$('.publish-loader').slideUp(200);
				},
			})
		}
        else if ($.inArray(choosedCat, clothesCategories) != -1)
        {
            url += '/clothes';

            if (ad_id == '')
            {
                ad_id = '';
            }else{
                url += '/' + ad_id;
            }

            url += '?category=' + choosedCat;

            $.ajax({
                type: 'GET',
                url: url,
                beforeSend: function(){
                    $('.publish-loader').slideDown(200);
                },
                success: function(data){
                    $('.create-ad-adv').remove()
                    $('.publish-loader').after(data).fadeOut(0).fadeIn(200)
                    $('.create-ad-adv').find('select').styler()

                    // $('#title').hide(0).attr('required', false)
                    // $('label[for=title]').hide(0)
                    $('#publish_make').change();
                },
                complete: function(){
                    $('.publish-loader').slideUp(200);
                },
            })
        }
		else
		{
			$('.create-ad-adv').remove()

			$('#title').show(0).attr('required', 'required')
			$('label[for=title]').show(0)
		}
	}).change()

	$('#filters_category_select').on('change', function(){
		if ( $.inArray($(this).val(), autoCategories) != -1){
			if (typeof $('.search-adv-attr')[0] == 'undefined')
			{
				var url = addParameterToURL( $(this).data('url') + '/auto' + location.search, 'make=' + $(this).data('make') )
				var url = addParameterToURL( url, 'model=' + $(this).data('model'))

				$.ajax({
					type: 'GET',
					url: url,
					beforeSend: function(){
						$('.loader').slideDown(200);
					},
					success: function(data){
						$('.filter-load').append(data)
						$('.filter').find('select').styler()
						$('#filter_make_select').change()
					},
					complete: function(){
						$('.loader').slideUp(200);
					},
				})
			}
		}else{
			$('.search-adv-attr, .brands').remove()
		}

        if ( $.inArray($(this).val(), clothesCategories) != -1){
            if (typeof $('.search-adv-attr')[0] == 'undefined')
            {
                var url = addParameterToURL( $(this).data('publish-url') + '/clothes' + location.search, 'make=' + $(this).data('make') )

                $.ajax({
                    type: 'GET',
                    url: url,
                    beforeSend: function(){
                        $('.loader').slideDown(200);
                    },
                    success: function(data){
                        $('.filter-load').append(data)
                        $('.filter').find('select').styler()
                        $('#filter_make_select').change()
                    },
                    complete: function(){
                        $('.loader').slideUp(200);
                    },
                })
            }
        } else {
            $('.create-ad-adv').remove()
        }
	}).change()

	$('#phone, #whatsapp, .phone-input, .forget-form input[name=phone]').mask('+7 (999) 999-99-99')

	var $createAdForm = $('.form-ad-wrapper');

	var iconImageSvgUrl = $('#my-awesome-dropzone').data('img-url');

	Dropzone.options.myAwesomeDropzone = {
		dictDefaultMessage: '<div class="dz-u-image"><img src="'+ iconImageSvgUrl +'"><p>Добавить фото</p></div>',
		addRemoveLinks: true,
		dictRemoveFile: 'Удалить',
		dictCancelUpload: 'Отмена',
		maxFiles: 9,
		maxFilesize: 10,
		acceptedFiles: 'image/*',
		dictMaxFilesExceeded: 'Можно загрузить только 9 изображений',
		dictInvalidFileType: 'Можно загружать только изображения',
		accept: function(file, done) {done();},
		success: function(data){
			var images = JSON.parse(data.xhr.response)
			var dataAttrs = ' data-name="' + data.name +'" data-lastmod="' + data.lastModified + '" ';
			$createAdForm.append('<input type="hidden" name="images[]" '+ dataAttrs +' value="'+ images.big +'|'+ images.small +'">')
		},
		removedfile: function(file) {
			var _ref;
			var val;
			$createAdForm.find('input[type=hidden]').each(function(){
				if ($(this).data('name') == file.name && $(this).data('lastmod') == file.lastModified)
				{
					val = $(this).val()
					$(this).remove()
				}
			})
			$.ajax({
				url: $('#my-awesome-dropzone').data('rm-url'),
				method: 'get',
				data: {
					paths: val
				}
			})
			console.log('removed')
			return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;
		},
		headers: {
	        'X-CSRF-Token': $('input[name="_token"]').val()
	    }
	}

	function setFavorite() {
        $('.meta .meta-fav').on('click', function (e) {
            $thisElem = $(this)

            if ($thisElem.hasClass('active')) {
                $.ajax({
                    url: $thisElem.data('remove-url'),
                    success: function () {
                        $thisElem.removeClass('active')
                        $('.f-tooltip').remove()
                    }
                })
            } else {
                $.ajax({
                    url: $thisElem.data('set-url'),
                    success: function (data) {
                        if (typeof timeoutId != 'undefined') {
                            clearInterval(timeoutId);
                        }
                        $('.f-tooltip').remove()
                        if (data == 1) {
                            $thisElem.after('<div class="f-tooltip active">Cначала <a href="" class="open-popup">авторизуйтесь</a></div>')
                        } else {
                            $thisElem.addClass('active').after('<div class="f-tooltip active">Добавлено в <a href="' + $thisElem.data('favorite-url') + '">избранное</a></div>')
                        }
                        timeoutId = setTimeout(function () {
                            // $('.f-tooltip').remove()
                        }, 3000)
                    }
                })
            }
            e.stopPropagation ? e.stopPropagation() : e.cancelBubble = true;
            e.preventDefault()
        })
	}

    setFavorite();

    $( document ).ajaxStop(function() {
        setFavorite();
    });

	$('.meta').on('click', '.open-popup', function(){
		$('.popup').addClass('active')
		return false
	})

	$('.fav.button-2').on('click', function(){
		$thisElem = $(this)
		if ($thisElem.data('auth') == 1){
			if ($thisElem.hasClass('active'))
			{
				$url = $(this).data('remove-url')
				$.ajax({
					url: $url,
					success: function(){
						$thisElem.removeClass('active')
					}
				})
			}else{
				$url = $(this).data('set-url')
				$.ajax({
					url: $url,
					success: function(){
						$thisElem.addClass('active')
					}
				})
			}
		}else{
			$('.ad-add2fav-warning').fadeIn(200)
		}
	})

	$('.meta').on('click', '.f-tooltip', function(e) {
		e.stopPropagation?e.stopPropagation():e.cancelBubble = true;
	})

	$('#delete-1').on('click', function(){

		if (confirm("Удалить объявление?"))
		{
			$thisElem = $(this)
			$.ajax({
				url: $thisElem.data('url'),
				success: function(data){
					$adElem = $thisElem.closest('.my-ad')

					if (data == '1')
					{
						$adElem.fadeOut(200)
						setTimeout(function(){
							$adElem.remove()
						}, 200)
					}else{
						console.log('Ошибка удаления объявления')
					}
				}
			})
		}
	})

	$('#delete-2').on('click', function(){

		if (confirm("Удалить объявление?"))
		{
			$thisElem = $(this)
			$.ajax({
				url: $thisElem.data('url'),
				success: function(data){

					if (data == '1')
					{
						location.href = $thisElem.data('root')
					}else{
						console.log('Ошибка удаления объявления')
					}
				}
			})
		}
	})

	$('.filter').on('change', '#filter_make_select', function(){
		var $model = $('#filter_model_select')
		var value = $(this).val()

		if (value != 'any')
		{
			var url = $(this).data('url') + '/' + value;
			url = addParameterToURL(url, 'model=' + $('#filters_category_select').data('model'))
			$.ajax({
				url: url,
				success: function(data){
					$model.html(data).trigger('refresh')
				}
			})
			$model.prop('disabled', false).trigger('refresh')
		}else{
			$model.val(null).prop('disabled', 'disabled').trigger('refresh')
		}
	})

	/*
	*
	*	Filters hiding
	*
	*/

	$('.filter').on('change', '#auto_year_from', function(){
		var value = $(this).val()

		$('#auto_year_to-styler').find('li').each(function(){
			if ($(this).html() <= value){
				$(this).css('display', 'none')
			}else{
				$(this).show('display', 'block')
			}
		})
	})

	$('.filter').on('change', '#auto_year_to', function(){
		var value = $(this).val()

		$('#auto_year_from-styler').find('li').each(function(){
			if ($(this).html() >= value){
				$(this).css('display', 'none')
			}else{
				$(this).show('display', 'block')
			}
		})
	})

	$('.filter').on('change', '#auto_capacity_from', function(){
		var value = $(this).val()

		$('#auto_capacity_to-styler').find('li').each(function(){
			if ($(this).html() <= value){
				$(this).css('display', 'none')
			}else{
				$(this).show('display', 'block')
			}
		})
	})

	$('.filter').on('change', '#auto_capacity_to', function(){
		var value = $(this).val()

		$('#auto_capacity_from-styler').find('li').each(function(){
			if ($(this).html() >= value){
				$(this).css('display', 'none')
			}else{
				$(this).show('display', 'block')
			}
		})
	})

	$('.button-2.refresh:not(.unactive)').on('click', function(){
		$thisElem = $(this);
		$.ajax({
			url: $thisElem.data('url'),
			success: function () {
				$thisElem[0].setAttribute('data-title', $thisElem.data('title').replace(/[0-9]+/, 3));
				$thisElem.closest('.my-ad').find('.loaded').css('width', 0); // used on "my ads" page
				$thisElem.addClass('unactive').addClass('success').on('mouseleave', function(){
					$thisElem.removeClass('success');
				});
			}
		})
	})

	$('.registration').on('submit', function(){
		var data = $(this).serialize()
		var url = $(this).attr('action')
		var $thisElem = $(this)
		$.ajax({
			url: url,
			method: 'get',
			data: data,
			success: function(data){
				$('.error-msg').remove()
				$thisElem.find('input[name=email].error').removeClass('error')
				$thisElem.find('input[name=confirm_code].error').removeClass('error')

				switch(data)
				{
					case '1':
						$thisElem.find('input').attr('type', 'hidden');
						$thisElem.find('button').before('<p>Введите код подтверждения, отправленный на указанный E-mail.</p><input placeholder="Код подтверждения" name="confirm_code" type="text" required>')
						$thisElem.find('input[name=confirm_code]').focus()
						break;
					case '2':
						$thisElem.find('input[name=email]').addClass('error').before('<p class="error-msg">Этот E-mail уже зарегистрирован.</p>')
						break;
					case '3':
						$thisElem.find('button').before('<p>Произошла ошибка при отправке СМС.</p>');
						break;
					case '4':
						location.href = location.href;
						break;
					case '5':
						$thisElem.find('input[name=confirm_code]').addClass('error').before('<p class="error-msg">Неправильный код подтверждения.</p>')
						break;

				}
				console.log(data)
			}
		})
		return false
	})


	$('.pass-forget').on('click', function(){
		return false
	})

	$('.forget-form').on('submit', function(){
		var data = $(this).serialize()
		var url = $(this).attr('action')
		var email = $(this).find('input[name=email]').val()

		$.ajax({
			url: url,
			data: data,
			success: function(data){
				$('.forget-form input[name=email].error').removeClass('error')
				$('.error-msg').remove()

				switch(data)
				{
					case '0':
						$('#login').click()
						$('.login input[name=email]').val(email)
						break;
					case '1':
						$('.forget-form input[name=email]').addClass('error').before('<p class="error-msg">Пользователь с таким E-mail не зарегистрирован</p>')
						break;
				}
				console.log(data)
			}
		})

		return false
	})

	$('.login').on('submit', function(){
		var data = $(this).serialize()
		var url = $(this).attr('action')
		$.ajax({
			url: url,
			data: data,
			success: function(data){
				$('.login input[name=email].error, .login input[name=password].error').removeClass('error')
				$('.error-msg').remove()
				switch(data)
				{
					case '0':
						var loc = location.href.replace('#', '');
						location.href = loc;
						break;
					case '1':
						$('.login input[name=password]').addClass('error').before('<p class="error-msg">Неправильный пароль</p>')
						break;
					case '2':
						$('.login input[name=email]').addClass('error').before('<p class="error-msg">Пользователь с таким E-mail не зарегистрирован</p>')
						break;
				}
				console.log(data)
			}
		})

		return false
	})

	// $('.go-premium').on('click', function(){
	// 	alert('Чтобы закрепить объявление в топе, свяжитесь с администраторами сайта. Номер указан на странице "Контакты".');
	// })

	$('.del-btn').on('click', function(){
		$thisElem = $(this)
		$.ajax({
			url: $thisElem.data('url'),
			success: function(data){
				if (data == '1')
				{
					$thisElem.closest('li').remove()
				}else{
					console.log('Ошибка удаления изображения')
				}
			}
		})
	})

	$('.complain.button-2').on('click', function(){
		alert('Функция будет доступна в ближайшее время');
	})

	$('#title').on('change keyup', function(){
		$('#title-chars-left').html( 70 - $(this).val().length );
	})

	$('#desc').on('change keyup', function(){
		$('#text-chars-left').html( 1000 - $(this).val().length );
	})

	// document.body.addEventListener('swl',function(){
	// 	$('.menu.left250').removeClass('left250');
	// 	$('.mob-menu.active').removeClass('active');
	// });
	//
	// document.body.addEventListener('swr',function(){
	// 	$('.menu').addClass('left250');
	// 	$('.mob-menu').addClass('active');
	// });

	$('.form-ad-wrapper').on('change', '#publish_make', function(){
		var url = $(this).data('models-url') + '/' + $(this).val();
		console.log(url);
		$.ajax({
			url: url,
			success: function(data){
				$('#publish_model').html(data).attr('disabled', false).trigger('refresh');
			},
		});
	})

	$(document).on('change', '#realty_ad_type', function(){
		if ($(this).find('option:selected').data('disable-lease'))
		{
			$('#realty_lease').attr('disabled', true).trigger('refresh');
		}
		else
		{
			$('#realty_lease').attr('disabled', false).trigger('refresh');
		}
	});

    function deleteAd() {
        $('.delete_ad_form').on('submit', function (e) {
            e.preventDefault();
            var $form = $(this);
            $.ajax({
                url: $form.attr('action'),
                data: $form.serialize(),
                success: function (response) {
                    if (response === '1') {
                        $form.closest('.list-item').remove();
                    } else {
                        console.error('Недостаточно прав для удаления объявления.');
                    }
                },
            });
        });
    };

    deleteAd();

    $( document ).ajaxStop(function() {
        deleteAd();
    });

	$('.form-ad-wrapper textarea').on('focus', function () {
        $(this).addClass('focus');
    }).on('blur', function () {
        if (!$(this).val()) {
            $(this).removeClass('focus');
        }
    });

	// Stick premium ads
    window.onscroll = function() {myFunction()};

    var header = document.getElementById("premium");
    var sticky = header.offsetTop;

    function myFunction() {
        if (window.pageYOffset > sticky && $('body').height() > 1650) {
            header.classList.add("sticky");
        } else {
            header.classList.remove("sticky");
        }
    }

})

function addParameterToURL(_url, param){
    // _url = location.href;
    _url += (_url.split('?')[1] ? '&':'?') + param;
    return _url;
}

//	SWIPE
$(window).load(function(){
	var
	 ce=function(e,n){var a=document.createEvent("CustomEvent");a.initCustomEvent(n,true,true,e.target);e.target.dispatchEvent(a);a=null;return false},
	 nm=true,sp={x:0,y:0},ep={x:0,y:0},
	 touch={
	  touchstart:function(e){sp={x:e.touches[0].pageX,y:e.touches[0].pageY}},
	  touchmove:function(e){nm=false;ep={x:e.touches[0].pageX,y:e.touches[0].pageY}},
	  touchend:function(e){if(nm){ce(e,'fc')}else{var x=ep.x-sp.x,xr=Math.abs(x),y=ep.y-sp.y,yr=Math.abs(y);if(Math.max(xr,yr)>100){ce(e,(xr>yr?(x<0?'swl':'swr'):(y<0?'swu':'swd')))}};nm=true},
	  touchcancel:function(e){nm=false}
	 };
	 for(var a in touch){document.addEventListener(a,touch[a],false);}
})
