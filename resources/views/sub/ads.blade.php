@if (!Request::has('filter'))
    @foreach($top_premium as $key => $ad)
        @include('sub.adsTemp', ['ad' => $ad])
    @endforeach
@endif

@foreach ($ads as $key => $ad)
    @include('sub.adsTemp')
    @if (in_array($key, [11, 21, 31, 41]))
        @if (count($premium)) @include('sub.adsTemp', ['ad' => $premium->random()]) @endif
    @endif
    <!-- Insert advertisement -->
    {{--@if ($key !== 0 && in_array($key, [5, 25, 45]))--}}
        {{--<div class="list-item">--}}
            {{--<ins class="adsbygoogle"--}}
             {{--style="display:block"--}}
             {{--data-ad-format="fluid"--}}
             {{--data-ad-layout-key="-fp+65+48-ja+jo"--}}
             {{--data-ad-client="ca-pub-9145432942650848"--}}
             {{--data-ad-slot="3232312088"></ins>--}}
        {{--</div>--}}
        {{--<script>--}}
             {{--(adsbygoogle = window.adsbygoogle || []).push({});--}}
        {{--</script>--}}
    {{--@endif--}}
@endforeach
