<option value="" selected disabled>Выберите модель</option>
@foreach ($models as $model)
	<option value="{{ $model->id }}" data-make="{{ null !== $model->parent ? $model->parent->id : '' }}">{{ $model->title }}</option>
@endforeach
