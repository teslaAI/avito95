<option value="any">Любая модель</option>
@foreach ($models as $model)
	<option value="{{ $model->slug }}" {{ $model->slug == Request::input('model') ? 'selected' : '' }}>{{ $model->title }}</option>
@endforeach
