<div class="list-item clearfix {{ Request::has('filter') ? 'discount' : '' }} {{ $ad->is_top ? 'top-premium' : '' }} {{ $ad->is_premium && !Request::has('filter') ? 'premium' : ($ad->is_parsed ? 'desrap' : '') }}" id="ad_{{ $ad->id }}"
     style="position: relative;">
    <a href='{{ $ad->url }}' class="ad-image-link">
        <img data-src="{{ URL::asset($ad->getThumbnailUrl()) }}" src="{{ URL::asset('img/placeholder-small.jpg') }}" alt="" class="lazy" style="position: relative; background-color: transparent">
        @if($ad->discount && $ad->discount->active && $ad->is_premium)<span>-{{ $ad->discount->discount }} %</span>@endif
    </a>
    <div class="list-item-box">
        <div class="list-text">
            <div class="list-item-title">
                <a href='{{ $ad->url }}'>{{ $ad->title }}</a>
                <div class="list-item-price {{ !$ad->price ? 'not-set' : '' }}">
                    @if($ad->discount && $ad->discount->active && $ad->is_premium)<span class="discount">{{ $ad->price . ' руб. ' }}</span>@endif<span>{{ $ad->getPrice() }}</span>
                </div>
                @if (count($ad->prepaidDiscount) > 0)
                    <p style="clear: both; font-size: 14px;">Приобрели: {{ count($ad->prepaidDiscount) }}+</p>
                @endif
                {{--@if ($ad->is_premium)--}}
                    {{--<span class="premium-ad-label" title="Премиум объявления выводятся в топе">Премиум</span>--}}
                {{--@endif--}}
            </div>
            <p class="list-desc">{{ $ad->getText(true) }}</p>
        </div>
        <div class="meta clearfix">
            @if (!Request::has('filter'))
                <a href=""
                   title="Добавить в избранное"
                   class="meta-fav {{ $ad->isTheUsersFavorite() ? 'active' : '' }}"
                   data-set-url="{{ Request::root() }}/aadd/setfavorite/{{ $ad->id }}"
                   data-remove-url="{{ Request::root() }}/aadd/removefavorite/{{ $ad->id }}"
                   data-favorite-url="{{ Request::root() }}/profile/favorite"
                ></a>
            @endif
            @if ($ad->category)
                <a href="{{ $ad->category->url }}" class="meta-cat">{{ $ad->category->name }}</a>
            @endif
            @if ($ad->city)
                <a href="{{ Request::url() }}?city={{ $ad->city->id }}" class="meta-city">{{ $ad->city->name }}</a>
            @endif
            @if (!$ad->is_premium)
                <span class="meta-time">
                        {{ $ad->getFormattedTime() }}
                    </span>
            @endif
        </div>
    </div>
    @if (Auth::check() && Auth::user()->is_admin)
        <form action="{{ route('web_ad.destroy', [$ad->id]) }}" class="delete_ad_form">
            {{ csrf_field() }}
            <button onclick="return confirm('Вы уверены, что хотите удалить это объявление?');"
                    title="Удалить">
                <i class="fas fa-trash"></i>
            </button>
        </form>
        @if (!in_array(formatPhoneNumber($ad->phone), App\BlockedNumber::all()->pluck('number')->toArray()))
            <form action="{{ route('web_ad.destroy', [$ad->id, 'block_number' => true]) }}" class="delete_ad_form">
                {{ csrf_field() }}
                <button onclick="return confirm('Вы уверены, что хотите удалить это объявление и заблокировать указанный в нем номер?');"
                        title="Удалить и заблокировать">
                    <i class="fas fa-trash"></i> & <i class="fas fa-ban"></i>
                </button>
            </form>
        @endif
        <button style="margin-left: 8px;" onclick="window.location.href = '{{ Request::root() }}/admin/ads/{{$ad->id}}'"><i class="fas fa-edit"></i></button>
        @if (!$ad->is_parsed)
            <span style="margin-left: 16px;">
                    <i class="fab fa-adversal"></i>
                </span>
        @endif
    @endif
</div>