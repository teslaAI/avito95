<div id="comments-block">
	@forelse ($ad->comments as $comment)
		@if (!$comment->is_private || Gate::allows('show', $comment))
			<li class="comment comment-question" data-id="{{ $comment->id }}">
				<div class="clearfix">
					<div class="comment-author">{{ $comment->author->name }}</div>
					@if ($comment->is_private)
						<div class="private"></div>
					@endif
					<div class="comment-time">{{ $comment->getFormattedTime() }}</div>
					@can ('delete', $comment)
						<a href="{{ URL::to('comment/remove/'.$comment->id.'?ad='.$ad->id) }}" class="comment-remove"></a>
					@endcan
				</div>
				<p class="comment-text">{{ $comment->text }}</p>
				@can ('reply', $comment)
					<a href="" class="replay">Ответить</a>
				@endcan
			</li>
			@foreach ($comment->answers as $answer)
				@if (!$comment->is_private || Gate::allows('show', $answer))
					<li class="comment comment-answer">
						<div class="clearfix">
							<div class="comment-author">{{ $answer->author->name }}</div>
							@if ( $answer->is_private )
								<div class="private"></div>
							@endif
							<div class="comment-time">{{ $answer->getFormattedTime() }}</div>
							@can ('delete', $answer)
								<a href='{{ URL::to("comment/remove/$answer->id") }}' class="comment-remove"></a>
							@endcan
						</div>
						<p class="comment-text">{{ $answer->text }}</p>
					</li>
				@endif
			@endforeach
		@endif
	@empty
		<div>Пока не задано ни одного вопроса.</div>
	@endforelse
</div>
