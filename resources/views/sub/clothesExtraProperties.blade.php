<div class="create-ad-adv clearfix">
    <div class="create-ad-clothes">
        <div class="select">
            <select data-placeholder="Тип объявления" class="w50" name="clothes_type_of_ad" id="clothes_ad_type" required="">
                <option value="" disabled selected>Тип объявления</option>
                <option value="women_clothes" data-disable-lease="0" {{ $ad && count($ad->clothes_properties) && $ad->clothes_properties->type == 'Женская одежда' ? 'selected' : '' }}>Женская одежда</option>
                <option value="men_clothes" data-disable-lease="0" {{ $ad && count($ad->clothes_properties) && $ad->clothes_properties->type == 'Мужская одежда' ? 'selected' : '' }}>Мужская одежда</option>
                <option value="child_clothes" data-disable-lease="1" {{ $ad && count($ad->clothes_properties) && $ad->clothes_properties->type == 'Детская одежда' ? 'selected' : '' }}>Детская одежда</option>
                <option value="men_shoes" data-disable-lease="1" {{ $ad && count($ad->clothes_properties) && $ad->clothes_properties->type == 'Мужская обувь' ? 'selected' : '' }}>Мужская обувь</option>
                <option value="women_shoes" data-disable-lease="1" {{ $ad && count($ad->clothes_properties) && $ad->clothes_properties->type == 'Женская обувь' ? 'selected' : '' }}>Женская обувь</option>
                <option value="child_shoes" data-disable-lease="1" {{ $ad && count($ad->clothes_properties) && $ad->clothes_properties->type == 'Детская обувь' ? 'selected' : '' }}>Детская обувь</option>
                <option value="jewelry" data-disable-lease="1" {{ $ad && count($ad->clothes_properties) && $ad->clothes_properties->type == 'Ювелирные изделия' ? 'selected' : '' }}>Ювелирные изделия</option>
                <option value="bags" data-disable-lease="1" {{ $ad && count($ad->clothes_properties) && $ad->clothes_properties->type == 'Сумки' ? 'selected' : '' }}>Сумки</option>
                <option value="Другие аксессуары" data-disable-lease="1" {{ $ad && count($ad->clothes_properties) && $ad->clothes_properties->type == 'Другие аксессуары' ? 'selected' : '' }}>Другие аксессуары</option>
            </select>
        </div>
    </div>
</div>
