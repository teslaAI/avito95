<div class="search-attr search-adv-attr clearfix">
	<div class="select">
		<select
			data-placeholder="Марка"
			class="width-210" id="filter_make_select"
			data-url="{{ Request::root() }}/extra/filters/models"
			name="make">
			<option value="any">Любая марка</option>
			@foreach ($makes->sortBy('title') as $make)
				<option value="{{ $make->slug }}"
					@if ( $make->slug == Request::input('make') )
						selected
					@endif
				>{{ $make->title }}</option>
			@endforeach
		</select>
	</div>
	<div class="select">
		<select data-placeholder="Модель" class="width-210" id="filter_model_select" disabled="disabled" name="model">
			<option value="any">Любая модель</option>
		</select>
	</div>
	<div class="select">
		<select data-search="false" data-placeholder="Год от" class="width-100" id="auto_year_from" name="auto_year_from">
			<option value="0">Год от</option>
			<?php $step = 5 ?>
			@for ($i = 1960; $i <= date('Y'); $i += $step)
				@if ($i == 1990)
					<?php $step = 1 ?>
				@endif
				<option value="{{ $i }}" {{ Request::has('auto_year_from') && Request::input('auto_year_from') == $i ? 'selected' : '' }}>{{ $i }}</option>
			@endfor
		</select>
		<select data-search="false" data-placeholder="до" class="width-100" id="auto_year_to" name="auto_year_to">
			<option value="9999">Год до</option>
			<?php $step = 5 ?>
			@for ($i = 1960; $i <= date('Y'); $i += $step)
				@if ($i == 1990)
					<?php $step = 1 ?>
				@endif
				<option value="{{ $i }}" {{ Request::has('auto_year_to') && Request::input('auto_year_to') == $i ? 'selected' : '' }}>{{ $i }}</option>
			@endfor
		</select>
	</div>
	<div class="select">
		<select data-placeholder="Объем от" class="width-100" id="auto_capacity_from" name="auto_capacity_from">
			<option value="0.0">Объем от</option>
			<?php $step = 0.1 ?>
			@for ($i = 0.6; $i < 6.5; $i += $step)
				@if ($i >= 3.5)
					<?php $step = 0.5 ?>
				@endif
				<option value="{{ $i }}" {{ Request::has('auto_capacity_from') && Request::input('auto_capacity_from') == strval($i) ? 'selected' : '' }}>{{ $i }}</option>
			@endfor
		</select>
		<select data-placeholder="до" class="width-100" id="auto_capacity_to" name="auto_capacity_to">
			<option value="9.9">Объем до</option>
			<?php $step = 0.1 ?>
			@for ($i = 0.6; $i < 6.5; $i += $step)
				@if ($i >= 3.5)
					<?php $step = 0.5 ?>
				@endif
				{{ Request::input('auto_capacity_to') }} = {{ $i }}
				<option value="{{ $i }}" {{ Request::has('auto_capacity_to') && Request::input('auto_capacity_to') == strval($i) ? 'selected' : '' }}>{{ $i }}</option>
			@endfor
		</select>
	</div>
	<div class="select">
		<select data-placeholder="Тип двигателя" class="width-210" name="auto_engine_type">
			<option value="">Любой</option>
			<option value="бензин" {{ Request::input('auto_engine_type') == 'бензин' ? 'selected' : '' }}>Бензин</option>
			<option value="дизель" {{ Request::input('auto_engine_type') == 'дизель' ? 'selected' : '' }}>Дизель</option>
			<option value="гибрид" {{ Request::input('auto_engine_type') == 'гибрид' ? 'selected' : '' }}>Гибрид</option>
		</select>
	</div>
	<div class="select">
		<select data-placeholder="Коробка передач" class="width-210" name="auto_transmission">
			<option value="">Любая</option>
			<option value="автомат" {{ Request::input('auto_transmission') == 'автомат' ? 'selected' : '' }}>Автомат</option>
			<option value="механика" {{ Request::input('auto_transmission') == 'механика' ? 'selected' : '' }}>Механика</option>
			<option value="робот" {{ Request::input('auto_transmission') == 'робот' ? 'selected' : '' }}>Робот</option>
		</select>
	</div>
	<button class="button btn-sec">Найти</button>
</div>
@if (!Request::has('model'))
	<div class="brands clearfix">
		<ul class="clearfix">
			@if (Request::has('make'))
				@foreach ($makes->where('slug', Request::input('make'))->first()->models as $model)
					<li>
						<a href="{{ Request::root() }}/category/{{ $model->parent->category->slug }}/{{ $model->parent->slug }}/{{ $model->slug }}">{{ $model->title }}</a>
						<span>{{ $model->auto_extra_properties->count() }}</span>
					</li>
				@endforeach
			@else
				@foreach ($makes->take(25)->sortBy('title') as $make)
					<li>
						<a href="{{ Request::root() }}/category/{{ $make->category->slug }}/{{ $make->slug }}">{{ $make->title }}</a>
						<span>{{ $make->ads_count }}</span>
					</li>
				@endforeach
			@endif
		</ul>
		@if (! Request::has('make'))
			<div class="all-brands clearfix">
				<ul>
					<?php $makes_count = $makes->count(); ?>
					@foreach ($makes->take(- ($makes_count-25) )->sortBy('title') as $make)
						<li>
							<a href="{{ Request::root() }}/category/{{ $make->category->slug }}/{{ $make->slug }}">{{ $make->title }}</a>
							<span>{{ $make->ads_count }}</span>
						</li>
					@endforeach
				</ul>
			</div>
			<a class="show-all" href="">Показать все</a>
		@endif
	</div>
@endif
