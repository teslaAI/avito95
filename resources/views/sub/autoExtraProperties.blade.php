<div class="create-ad-adv clearfix">
	<div class="create-ad-transport">
		<label class="required" for="publish_make">Марка</label>
		<select data-placeholder="Марка" name="make" id="publish_make" data-models-url="{{ Request::root() }}/publish/extra/getmodels" required>
			<option value="" selected disabled>Выберите марку</option>
			@foreach ($models->where('make_id', null)->sortBy('title') as $make)
				<option value="{{ $make->id }}" {{ isset($ad) && count($ad->auto_models) && $ad->auto_models->first()->parent->id == $make->id ? 'selected':'' }}>{{ $make->title }}</option>
			@endforeach
		</select>
		<label class="required" for="publish_model">Модель</label>
		<select data-placeholder="Модель" name="model" id="publish_model" required disabled="">
			<option value="" selected disabled>Выберите модель</option>
		</select>
		<div class="select">
			<label class="w50 required" for="input_year">Год</label>
			<label class="w50" for="input_capacity">Объем</label>
			<select data-search="false" data-placeholder="Год выпуска" id="input_year" class="w50" name="year" required>
				<option value="">Год выпуска</option>
				<?php $step = 1 ?>
				@for ($i = date('Y'); $i >= 1950; $i -= $step)
					<option value="{{ $i }}"
						@if ((Request::has('auto_year_from') && Request::input('auto_year_from') == $i) || (isset($ad) && count($ad->auto_models) && $ad->auto_properties->year == $i))
							selected
						@endif
					>{{ $i }}</option>
				@endfor
			</select>
			<select data-search="false" data-placeholder="Объем двигателя" id="input_capacity" class="w50" name="capacity">
				<option value="" >Объем двигателя</option>
				<?php $step = 0.1 ?>
				@for ($i = 0.6; $i < 6.5; $i += $step)
					<option value="{{ $i }}"
						@if ( (Request::has('auto_capacity_from') && Request::input('auto_capacity_from')  == strval($i)) || (isset($ad) && count($ad->auto_models) && $ad->auto_properties->capacity == strval($i)))
							selected
						@endif
					>{{ $i }}</option>
				@endfor
			</select>
		</div>
		<label for="input_engine_type">Тип двигателя</label>
		<select name="engine_type" id="input_engine_type">
			<option value="" selected disabled>Тип двигателя</option>
			<option value="бензин" {{ isset($ad) && count($ad->auto_models) && $ad->auto_properties->engine_type == 'бензин' ? 'selected' : '' }}>Бензин</option>
			<option value="дизель" {{ isset($ad) && count($ad->auto_models) && $ad->auto_properties->engine_type == 'дизель' ? 'selected' : '' }}>Дизель</option>
			<option value="гибрид" {{ isset($ad) && count($ad->auto_models) && $ad->auto_properties->engine_type == 'гибрид' ? 'selected' : '' }}>Гибрид</option>
			<option value="электро" {{ isset($ad) && count($ad->auto_models) && $ad->auto_properties->engine_type == 'электро' ? 'selected' : '' }}>Электро</option>
			<option value="газ" {{ isset($ad) && count($ad->auto_models) && $ad->auto_properties->engine_type == 'газ' ? 'selected' : '' }}>Газ</option>
		</select>
		<div class="select">
			<label class="w50" for="input_power">Мощность (л.с.)</label>
			<label class="w50" for="input_mileage">Пробег (км.)</label>
			<input type="text" class="w50 styler number" placeholder="Укажите мощность" id="input_power" name="power" value="{{ isset($ad) && count($ad->auto_models) ? $ad->auto_properties->power : '' }}">
			<input type="text" class="w50 styler number" placeholder="Укажите пробег" id="input_mileage" name="mileage" value="{{ isset($ad) && count($ad->auto_models) ? $ad->auto_properties->mileage : '' }}">
		</div>
		<label for="input_transmission">Коробка передач</label>
		<select name="transmission" id="input_transmission">
			<option value="" selected disabled>Коробка передач</option>
			<option value="механика" {{ isset($ad) && count($ad->auto_models) && $ad->auto_properties->transmission == 'механика' ? 'selected' : '' }}>Механика</option>
			<option value="автомат" {{ isset($ad) && count($ad->auto_models) && $ad->auto_properties->transmission == 'автомат' ? 'selected' : '' }}>Автомат</option>
			<option value="робот" {{ isset($ad) && count($ad->auto_models) && $ad->auto_properties->transmission == 'робот' ? 'selected' : '' }}>Робот</option>
			<option value="вариатор" {{ isset($ad) && count($ad->auto_models) && $ad->auto_properties->transmission == 'вариатор' ? 'selected' : '' }}>Вариатор</option>
		</select>
	</div>
</div>
