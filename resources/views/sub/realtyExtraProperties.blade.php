<div class="create-ad-adv clearfix">
	<div class="create-ad-realty">
		<div class="select">
			<select data-placeholder="Тип объявления" class="w50" name="type_of_ad" id="realty_ad_type" required="">
				<option value="" disabled selected>Тип объявления</option>
				<option value="Сдам" data-disable-lease="0" {{ $ad && count($ad->realty_properties) && $ad->realty_properties->type == 'Сдам' ? 'selected' : '' }}>Сдам</option>
				<option value="Сниму" data-disable-lease="0" {{ $ad && count($ad->realty_properties) && $ad->realty_properties->type == 'Сниму' ? 'selected' : '' }}>Сниму</option>
				<option value="Продам" data-disable-lease="1" {{ $ad && count($ad->realty_properties) && $ad->realty_properties->type == 'Продам' ? 'selected' : '' }}>Продам</option>
				<option value="Куплю" data-disable-lease="1" {{ $ad && count($ad->realty_properties) && $ad->realty_properties->type == 'Куплю' ? 'selected' : '' }}>Куплю</option>
			</select>
			<select data-placeholder="Срок аренды" class="w50" name="lease" required="" id="realty_lease">
				<option value="" disabled selected>Срок аренды</option>
				<option value="long_term" {{ $ad && count($ad->realty_properties) && $ad->realty_properties->lease == 'long_term' ? 'selected' : '' }}>Длительный срок</option>
				<option value="dayly" {{ $ad && count($ad->realty_properties) && $ad->realty_properties->lease == 'dayly' ? 'selected' : '' }}>Посуточно</option>
			</select>
		</div>
		<div class="select">
			@if ($category != 'land')
				<select data-placeholder="Количество комнат" class="w50" name="num_of_rooms" required="">
					<option value="" disabled selected>Количество комнат</option>
					<option value="1" {{ $ad && count($ad->realty_properties) && $ad->realty_properties->num_of_rooms == 1 ? 'selected' : '' }}>1</option>
					<option value="2" {{ $ad && count($ad->realty_properties) && $ad->realty_properties->num_of_rooms == 2 ? 'selected' : '' }}>2</option>
					<option value="3" {{ $ad && count($ad->realty_properties) && $ad->realty_properties->num_of_rooms == 3 ? 'selected' : '' }}>3</option>
					<option value="4" {{ $ad && count($ad->realty_properties) && $ad->realty_properties->num_of_rooms == 4 ? 'selected' : '' }}>4</option>
					<option value="5" {{ $ad && count($ad->realty_properties) && $ad->realty_properties->num_of_rooms == 5 ? 'selected' : '' }}>5</option>
					<option value="6" {{ $ad && count($ad->realty_properties) && $ad->realty_properties->num_of_rooms == 6 ? 'selected' : '' }}>6</option>
					<option value="7" {{ $ad && count($ad->realty_properties) && $ad->realty_properties->num_of_rooms == 7 ? 'selected' : '' }}>7</option>
					<option value="8" {{ $ad && count($ad->realty_properties) && $ad->realty_properties->num_of_rooms == 8 ? 'selected' : '' }}>8</option>
					<option value="9" {{ $ad && count($ad->realty_properties) && $ad->realty_properties->num_of_rooms == 9 ? 'selected' : '' }}>9</option>
					<option value="&gt;9" {{ $ad && count($ad->realty_properties) && $ad->realty_properties->num_of_rooms == '>9' ? 'selected' : '' }}>Больше 9</option>
				</select>
			@endif
			<input type="text" class="w50 number styler" placeholder="Площадь м²" name="square" required="" value="{{ $ad && count($ad->realty_properties) ? $ad->realty_properties->square : '' }}">
		</div>
		@if ($category != 'land')
			<div class="select">
				@if ($category != 'houses')
					<input type="text" class="w50 number styler" placeholder="Этаж" name="floor" required="" value="{{ $ad && count($ad->realty_properties) ? $ad->realty_properties->floor : '' }}">
				@endif
				<input type="text" class="w50 number styler" placeholder="Этажей в доме" name="num_of_floors" required="" value="{{ $ad && count($ad->realty_properties) ? $ad->realty_properties->num_of_rooms : '' }}">
			</div>
		@endif
	</div>
</div>
