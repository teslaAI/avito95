<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<meta name="description" content="@yield ('description', 'Сайт бесплатных объявлений в Чеченской Республике. Автомобили, недвижимость, электроника и многое другое на kupi95.ru')">
	<meta name="google" content="notranslate" />
	<meta name="viewport" content="width=device-width, shrink-to-fit=YES">
	@yield ('og')
	<link rel="stylesheet" href="{{ asset('css/app.css?' . filemtime('css/app.css')) }}">
	<link rel="icon" href="{{ asset('img/favicon.png') }}">
	<link rel="manifest" href="{{ asset('manifest.json') }}">
	<meta name="theme-color" content="#4f5773"/>
	<title>@yield ('title', 'Купи95 &mdash; объявления в Чеченской Республике'){{ app('view')->hasSection('title') ? ' &mdash; Купи95' : '' }}</title>
    {{--<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>--}}
</head>
<body>
	<div class="header">
		<div class="row clearfix">
			<div class="mob-menu-btn">
				@if (!Request::is('/') || Request::has('filter'))
					<a class="back-btn" href="javascript:history.go(-1)"></a>
				@else
					<div class="mob-menu"></div>
				@endif
			</div>
			<a href="{{ url('/') }}" class="logo"></a>
			<div class="h-wrapper clearfix">
				<menu>
					<li><a href="{{ url('rules') }}">Правила сайта</a></li>
					<li><a href="{{ url('about') }}">О нас</a></li>
					<li><a href="{{ url('contacts') }}">Контакты</a></li>
				</menu>
				@if (Auth::check())
					<a href="{{ url('publish') }}" class="add-button button" style="color: #fff">Добавить объявление</a>
				@else
					<div class="sign-in">
						<a href="{{ url('publish') }}" class="add-button button" style="color: #fff">Добавить объявление</a>
					</div>
				@endif

				@if (Auth::check())
					<div class="h-avatar {{ $new_questions > 0 ? 'notify' : '' }}">
						<img src="{{ Auth::user()->getAvatarURL() }}" alt="">
						<div class="touch"></div>
						<div class="drop-menu">
							<ul>
								<li><a href="{{ url('profile/ads') }}">Мои объявления</a></li>
								<li><a href="{{ url('profile/questions') }}">
									Вопросы
									@if ($new_questions > 0)
										<span>{{ $new_questions }}</span>
									@endif
								</a></li>
								<li><a href="{{ url('profile/favorite') }}">Избранное</a></li>
								<li><a href="{{ url('profile/referral') }}">Конкурс</a></li>
								<li><a href="{{ url('profile/settings') }}">Настройки профиля</a></li>
								<li><a href="{{ url('logout') }}">Выход</a></li>
							</ul>
						</div>
					</div> 
				@else
					<div class="sign-in">
						<a href="">Войти</a>
					</div>
				@endif
			</div>
		</div>
	</div>
	<div class="row middle clearfix">
		<div class="menu col-1">
			<div class="menu-title">Категории</div>
			<ul class="nav">
				@foreach ($categories as $category)
					<li class="nav-item {{ $category->isActiveOrChildren() ? 'active' : ''}}">
						<p>{{ $category->name }}
{{--							<span>{{ $category->adsCount() }}</span>--}}
						</p>
						<ul class="submenu">
							<li class="{{ $category->isActive() ? 'active2' : ''}}">
								<a href='{{ url("category/$category->slug") }}'>Все объявления</a>
							</li>
							@foreach($category->children as $child)
								@if($child->adsCount())
									<li class="{{ $child->isActive() ? 'active2' : ''}}">
										<a href='{{ url("category/$child->slug") }}'>
{{--											<span class="subcats-count">{{ $child->adsCount() }}</span>--}}
											{{ $child->name }}
										</a>
									</li>
								@endif
							@endforeach
						</ul>
					</li>
				@endforeach
				<a href='{{ url("?filter=discount") }}'><li><p>Товары со скидкой<span>{{ $discount_count }}</span></p></li></a>
			</ul>
			<!-- Боковой -->
			<div style="padding-right: 20px;">
				<ins class="adsbygoogle"
			     style="display:block"
			     data-ad-client="ca-pub-9145432942650848"
			     data-ad-slot="1013596442"
			     data-ad-format="auto"></ins>
			</div>
			<script>
				(adsbygoogle = window.adsbygoogle || []).push({});
			</script>
			<!-- Премиум объявления -->
			<div class="premium-bar">
				<div id="premium">
					@foreach($premium as $ad)
						@include('sub.adsTemp')
					@endforeach
				</div>
			</div>
			{{--<a href="http://namazvdom.com/" class="banner" rel="nofollow" target="_blank">--}}
				{{--<img src="{{ asset('img/banners/namazvdom.jpg') }}" alt="">--}}
			{{--</a>--}}
		</div>

		@yield('content')

        <div style="clear:both;"></div>
        {{--<div class="pagination">--}}
            {{--<!-- Нижний -->--}}
            {{--<ins class="adsbygoogle"--}}
                 {{--style="display:block"--}}
                 {{--data-ad-client="ca-pub-9145432942650848"--}}
                 {{--data-ad-slot="6625951553"--}}
                 {{--data-ad-format="auto"></ins>--}}
            {{--<script>--}}
                {{--(adsbygoogle = window.adsbygoogle || []).push({});--}}
            {{--</script>--}}
        {{--</div>--}}

	</div>
    
	<div class="footer">
		<div class="row">
			<menu>
				<li><a href="{{ url('rules') }}">Правила</a></li>
				<li><a href="{{ url('about') }}">О нас</a></li>
				<li><a href="{{ url('contacts') }}">Контакты</a></li>
			</menu>
            <div class="social">
                <a class="social-link social-link--insta" href="https://www.instagram.com/kupi95.ru/">
                    <i class="fab fa-instagram"></i>
                </a>
                {{--<a class="social-link social-link--vk" href="http://vk.com/hoamru">--}}
                    {{--<i class="fab fa-vk"></i>--}}
                {{--</a>--}}
                {{--<a class="social-link social-link--fb" href="https://www.facebook.com/hoamru">--}}
                    {{--<i class="fab fa-facebook-f"></i>--}}
                {{--</a>--}}
            </div>
		</div>
	</div>
	<div class="popup">
		<div class="popup-wrapper">
			<ul class="clearfix">
				<li id="login" class="active">Войти</li>
				<li id="registr">Регистрация</li>
			</ul>
			<div class="forms">
				<form class="forget-form" action="{{ url('forget') }}">
					<p>Для получения нового пароля, введите E-mail, указанный при регистрации:</p>
					<input placeholder="E-mail" name="email" type="email" required>
					<button class="button">Отправить</button>
				</form>
				<form class="login" action="{{ url('login') }}">
					<input placeholder="E-mail" name="email" type="email">
					<input placeholder="Если регистрировались по номеру" id="phone" name="phone" type="phone">
					<input placeholder="Пароль" name="password" type="password" id="login_password">
					<button class="button">Войти</button>
					<a href="" class="pass-forget" >Забыли пароль?</a>
				</form>
				<form class="registration" action="{{ url('register') }}">
					<input placeholder="Имя" name="name" type="text" required>
					<input placeholder="E-mail" name="email" type="email" required>
					<input placeholder="Пароль" name="password" type="password" required>
					<div class="captcha-wrapper">
						<div class="g-recaptcha" style="transform:scale(0.92);transform-origin:0;-webkit-transform:scale(0.92);
							transform:scale(0.92);-webkit-transform-origin:0 0;transform-origin:0 0;"
							 data-sitekey="6LeNFrUUAAAAAPe9rrRO7uONBeHsyrvjX7xbQrRi">
						</div>
					</div>
					<button class="button">Регистрация</button>
				</form>
			</div>
		</div>
	</div>
	@if ($agent->isMobile())
		<div class="application hidden">
			{{--<div class="image"></div>--}}
			<img src="{{ url('/img/background.jpg') }}" alt="">
			{{--<h1>Скачайте приложение</h1>--}}
			@if ($agent->isAndroidOS())
				<a href="https://play.google.com/store/apps/details?id=kupi95.ru.kupi95&hl=ru" class="button-download">Скачать приложение</a>
			@elseif ($agent->is('OS X'))
				<a href="https://apps.apple.com/us/app/kupi95/id1480924277" class="button-download">Скачать приложение</a>
			@endif
			<a href="" class="button-close">Спасибо не надо</a>
		</div>
	@endif
	<div class="scroll-top">наверх</div>
	<script src="https://cdn.jsdelivr.net/npm/js-cookie@2/src/js.cookie.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/vanilla-lazyload@12.0.0/dist/lazyload.min.js"></script>
	<script src="{{ asset('js/app.js?' . filemtime('js/app.js')) }}"></script>
	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-72420904-1', 'auto');
	  ga('send', 'pageview');
	</script>
	<script src='https://www.google.com/recaptcha/api.js'></script>
    <script defer src="https://use.fontawesome.com/releases/v5.0.8/js/all.js" crossorigin="anonymous"
        integrity="sha384-SlE991lGASHoBfWbelyBPLsUlwY1GwNDJo3jSJO04KZ33K2bwfV9YBauFfnzvynJ"></script>
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-155380095-1"></script>
	<script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-155380095-1');
	</script>
	<!-- Yandex.Metrika counter -->
	<script type="text/javascript" >
        (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
            m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
        (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

        ym(56913829, "init", {
            clickmap:true,
            trackLinks:true,
            accurateTrackBounce:true,
            webvisor:true
        });
	</script>
	<noscript><div><img src="https://mc.yandex.ru/watch/56913829" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
	<!-- /Yandex.Metrika counter -->
	@yield('additionalScripts')
</body>
</html>
