@extends ('layout')

@section ('title', $ad->title)
@section ('description', $ad->text)

@section ('og')
    <meta property="fb:app_id" content="207354386280299">
    <meta property="og:title" content="{{ $ad->shareTitle }}">
    <meta property="og:type" content="website">
    <meta property="og:url" content="{{ Request::url() }}">
    <meta property="image" content="{{ $ad->getShareImageUrl('vk') }}">
    <meta property="og:image" content="{{ $ad->getShareImageUrl('facebook') }}">
    <meta property="og:description" content="{{ $ad->text }}">
@stop

@section('content')
    <div class="content col-2">
        <p>Услуга ТОВАРЫ СО СКИДКОЙ</p>
        <p>К вашему объявлению будут применены дополнительные услуги Kupi95.
            Объявление будет выделено со знаком указанной скидки,
            а также будет выставлена кнопка для предоплаты клиентом за вашу продукцию.
        </p>
        <p><b>Услуга применяется только к премиум объявлениям.</b></p>

        @if (session('message'))
            <p style="color: #37d89a">{{ session('message') }}</p>
        @endif

        <form class="form-ad-wrapper" action="{{ url('aadd/discount', $ad->id) }}" method="post" style="display: inline;">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">

            <label for="prepayment">Укажите сумму предоплаты за вашу продукцию *</label>
            <input required type="text" class="styler" id="prepayment" name="prepayment">

            <label for="discount">Укажите скидку на вашу продукцию в процентах *</label>
            <input required type="text" class="styler" id="discount" name="discount">

            <button class="button-2">Отправить заявку</button>
        </form>
    </div>
@stop
