@extends ('layout')

@section ('title', $ad->title)
@section ('description', $ad->text)

@section ('og')
    <meta property="fb:app_id" content="207354386280299">
    <meta property="og:title" content="{{ $ad->shareTitle }}">
    <meta property="og:type" content="website">
    <meta property="og:url" content="{{ Request::url() }}">
    <meta property="image" content="{{ $ad->getShareImageUrl('vk') }}">
    <meta property="og:image" content="{{ $ad->getShareImageUrl('facebook') }}">
    <meta property="og:description" content="{{ $ad->text }}">
@stop

@section('content')
    <div class="content col-2">
        <p>Услуга "поднятие в списке"</p>
        <p>Поднятие объявления в списке, без удержания в верху ленты возможно:</p>
        <ol>
            <li>Бесплатно 1 раз в трое суток.</li>
            <li>Платно (10 руб) любое количество раз</li>
        </ol>
        <form action="{{ url('payment') }}" method="post" style="display: inline;">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" name="OutSum" value="{{ App\Ad::REFRESH_SUM }}">
            <input type="hidden" name="InvId" value="{{ $ad->id . rand(10, 99) }}">
            <input type="hidden" name="Description" value="Подъем объявления">
            <button title="Вы можете бесплатно активировать объявление раз в 3 суток или платно неограниченное количество раз" class="button-2 refresh">Поднять объявление</button>
        </form>
    </div>
@stop
