@extends ('profile.layout')

@section ('profile-content')

<div class="my-addss">
	@forelse ($ads as $ad)
		<div class="my-ad clearfix">
			<div class="my-ad-img">
				<a href='{{ $ad->url }}'>
					<img src="{{ URL::asset($ad->getThumbnailUrl()) }}" alt="">
				</a>
			</div>
			<div class="my-ad-stat">
				<div class="progress-block clearfix">
					<span class="clearfix">Размещено до {{ $ad->getExpireTime() }}</span>
					<div class="progress">
						<div class="loaded" style="width: {{ $ad->getPassedTimeProgress() }}%;"></div>
					</div>
				</div>
				@if ($ad->is_premium)
					<div class="progress-block clearfix">
						<span class="clearfix">Премиум истекает через {{ $ad->getExpirePremium() }} дней</span>
					</div>
					<button class="premium-aadd" style="background-color:#909ccc; cursor: auto;" title="Премиум объявления выводятся в сайдбаре слева и повторяются в своей категории">Премиум</button>
					<button onclick="window.location.href = '{{ Request::root() }}/aadd/discount/{{ $ad->id }}';" class="button-2" style="border: 1px solid red; color: red">Товар со скидкой</button>
				@else
					<button class="go-premium go-super-premium" style="background-color: #76a7e1" onclick="window.location.href = '{{ Request::root() }}/aadd/super_premium/{{ $ad->id }}';">Сделать премиум +</button>
					<button class="go-premium" onclick="window.location.href = '{{ Request::root() }}/aadd/premium/{{ $ad->id }}';">Сделать премиум</button>
					@if (auth()->user()->is_admin || !Gate::denies('refresh', $ad))
						<button title="Вы можете бесплатно активировать объявление раз в 3 суток или платно неограниченное количество раз" data-title="Вы сможете обновить объявление через {{ $ad->getDaysToRefresh(true) }}" data-url="{{ Request::root() }}/aadd/refresh/{{ $ad->id }}" class="button-2 refresh {{ Gate::denies('refresh', $ad) ? 'unactive' : '' }}">Поднять объявление</button>
					@else
						<button title="Вы можете бесплатно активировать объявление раз в 3 суток или платно неограниченное количество раз" onclick="window.location.href = '{{ Request::root() }}/fresh/aadd/{{ $ad->id }}';" class="button-2 refresh">Поднять объявление</button>
					@endif
				@endif
			</div>
			<div class="my-ad-desc">
				<a href='{{ $ad->url }}' class="my-ad-title">{{ $ad->title }}</a>
				<span>{{ $ad->getPrice() }}</span>
				<a href='{{ URL::to("ad/edit/$ad->id") }}' class="button-2 edit">Изменить</a>
				<button title="Удалить" id="delete-1" class="button-2 delete" data-url='{{ URL::to("aadd/remove/$ad->id") }}'></button>
				@if ($ad->is_premium)
					<button onclick="window.location.href = '{{ Request::root() }}/aadd/discount/{{ $ad->id }}';" class="button-2 alt-stat" style="border: 1px solid red; color: red; padding: 0 7px; margin-left: 6px;" title="Товар со скидкой">%</button>
					<button class="premium-aadd alt-stat" style="background-color:#909ccc; cursor: auto;" title="Премиум объявления выводятся в сайдбаре слева и повторяются в своей категории">Премиум</button>
				@else
					<button class="go-premium alt-stat" onclick="window.location.href = '{{ Request::root() }}/aadd/premium/{{ $ad->id }}';">Сделать премиум</button>
					<button class="go-premium go-super-premium alt-stat" style="background-color: #76a7e1" onclick="window.location.href = '{{ Request::root() }}/aadd/super_premium/{{ $ad->id }}';">Сделать премиум +</button>
				@if (auth()->user()->is_admin || !Gate::denies('refresh', $ad))
						<button title="Вы можете бесплатно активировать объявление раз в 3 суток или платно неограниченное количество раз" data-title="Вы сможете обновить объявление через {{ $ad->getDaysToRefresh(true) }}" data-url="{{ Request::root() }}/aadd/refresh/{{ $ad->id }}" class="button-2 refresh alt-stat {{ Gate::denies('refresh', $ad) ? 'unactive' : '' }}">Поднять объявление</button>
					@else
						<button title="Вы можете бесплатно активировать объявление раз в 3 суток или платно неограниченное количество раз" onclick="window.location.href = '{{ Request::root() }}/fresh/aadd/{{ $ad->id }}';" class="button-2 refresh alt-stat">Поднять объявление</button>
					@endif
				@endif
			</div>
		</div>
	@empty
		Вы еще не добавили ни одно объявление.
	@endforelse
</div>

@stop
