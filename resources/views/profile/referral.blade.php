@extends ('profile.layout')

@section ('profile-content')

@if(auth()->user()->referral_count != 0)
    <p>Вы находитесь на {{ $user_index }}-м месте из {{ $count_users_with_referrals }}</p>
@endif

<p>Чтобы участвовать в конкурсе поделитесь ссылкой внизу.</p>

@if(count($ads))
    @php
        $ad = $ads->random(1);
        if (\App\Ad::whereId(1)->where('is_contest', 1)->first()) {
            $rand = rand(0, count($ads));
        }
    @endphp

    @if(isset($rand) && $rand == 0)
        <div class="item-share-group2">
            <a class="fb-share button-2" href="https://kupi95.ru?referral_id={{ auth()->id() }}" onclick="window.open('//www.facebook.com/sharer/sharer.php?u=https://kupi95.ru?referral_id={{ auth()->id() }}&amp;title={{ $ad->title }}','','Toolbar=1,Location=0,Directories=0,Status=0,Menubar=0,Scrollbars=0,Resizable=0,Width=550,Height=400'); return false;">Поделиться</a>
            <a class="vk-share button-2" href="http://vk.com/share.php?url=https://kupi95.ru?referral_id={{ auth()->id() }}" onclick="window.open('http://vk.com/share.php?url=https://kupi95.ru?referral_id={{ auth()->id() }}','','Toolbar=1,Location=0,Directories=0,Status=0,Menubar=0,Scrollbars=0,Resizable=0,Width=550,Height=400'); return false;">Поделиться</a>
            <a class="wa-share button-2" href="https://kupi95.ru?referral_id={{ auth()->id() }}" onclick="window.open('https://api.whatsapp.com/send?text=https://kupi95.ru?referral_id={{ auth()->id() }}&utm_source=share2','','Toolbar=1,Location=0,Directories=0,Status=0,Menubar=0,Scrollbars=0,Resizable=0,Width=550,Height=400'); return false;">Поделиться</a>
        </div>
    @else
        <div class="item-share-group2" style="float: none">
            <a class="fb-share button-2" href="https://kupi95.ru/ad/{{ $ad->id . "?referral_id=" . auth()->id() }}" onclick="window.open('//www.facebook.com/sharer/sharer.php?u=https://kupi95.ru/ad/{{ $ad->id . "?referral_id=" . auth()->id() }}&amp;title={{ $ad->title }}','','Toolbar=1,Location=0,Directories=0,Status=0,Menubar=0,Scrollbars=0,Resizable=0,Width=550,Height=400'); return false;">Поделиться</a>
            <a class="vk-share button-2" href="http://vk.com/share.php?url=https://kupi95.ru/ad/{{ $ad->id . "?referral_id=" . auth()->id() }}" onclick="window.open('http://vk.com/share.php?url=https://kupi95.ru/ad/{{ $ad->id . "?referral_id=" . auth()->id() }}','','Toolbar=1,Location=0,Directories=0,Status=0,Menubar=0,Scrollbars=0,Resizable=0,Width=550,Height=400'); return false;">Поделиться</a>
            <a class="wa-share button-2" href="https://kupi95.ru/ad/{{ $ad->id . "?referral_id=" . auth()->id() }}" onclick="window.open('https://api.whatsapp.com/send?text=https://kupi95.ru/ad/{{ $ad->id . "?referral_id=" . auth()->id() }}&utm_source=share2','','Toolbar=1,Location=0,Directories=0,Status=0,Menubar=0,Scrollbars=0,Resizable=0,Width=550,Height=400'); return false;">Поделиться</a>
        </div>
    @endif

@endif

@stop