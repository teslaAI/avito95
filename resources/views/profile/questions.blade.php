@extends ('profile.layout')

@section ('profile-content')

<div class="my-addss">
	@forelse ($comments as $comment)
		<div class="my-ad questions clearfix">
			<a class="my-ad-title {{ $comment->is_viewed ? '' : 'new' }}" href="{{ Request::root() }}/ad/{{ $comment->ad->id }}">{{ $comment->ad->title }}</a>
			<a class="reply" href="{{ Request::root() }}/ad/{{ $comment->ad->id }}">{{ $comment->answer_to === null ? 'Ответить' : 'Посмотреть' }}</a>
			<p class="query {{ $comment->answer_to === null ? '' : 'answer' }} {{ $comment->is_viewed ? 'viewed' : '' }}">{{ $comment->text }}</p>
		</div>
	@empty
		Здесь будут показаны вопросы, заданные под вашими объявлениями.
	@endforelse
</div>

@stop
