@extends ('layout')

@section('content')

<div class="content col-2">
	<div class="account clearfix">
		<div class="account-menu">
			<h2>Мой аккаунт</h2>
			<ul class="clearfix">
				<li class="{{ Request::is('profile/ads') ? 'active' : '' }}">
					<a href="{{ Request::root() }}/profile/ads">Мои объявления</a>
				</li>
				<li class="{{ Request::is('profile/questions') ? 'active' : '' }}">
					<a href="{{ Request::root() }}/profile/questions">Вопросы</a>
				</li>
				<li class="{{ Request::is('profile/favorite') ? 'active' : '' }}">
					<a href="{{ Request::root() }}/profile/favorite">Избранные</a>
				</li>
				<li class="{{ Request::is('profile/referral') ? 'active' : '' }}">
					<a href="{{ Request::root() }}/profile/referral">Конкурс</a>
				</li>
				<li class="{{ Request::is('profile/settings') ? 'active' : '' }}">
					<a href="{{ Request::root() }}/profile/settings">Настройки</a>
				</li>
			</ul>
		</div>

		@yield ('profile-content')

	</div>
</div>
@if (Request::is('profile/ads'))
	<div style="clear:both"></div>
	<div class="pagination-wrapper">
		{!! $ads->render() !!}
	</div>
@endif

@stop
