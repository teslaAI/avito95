@extends ('layout')

@section ('title', $ad->title)
@section ('description', $ad->text)

@section ('og')
    <meta property="fb:app_id" content="207354386280299">
    <meta property="og:title" content="{{ $ad->shareTitle }}">
    <meta property="og:type" content="website">
    <meta property="og:url" content="{{ Request::url() }}">
    <meta property="image" content="{{ $ad->getShareImageUrl('vk') }}">
    <meta property="og:image" content="{{ $ad->getShareImageUrl('facebook') }}">
    <meta property="og:description" content="{{ $ad->text }}">
@stop

@section('content')
    <div class="content col-2">
        <p>Услуга "премиум объявление"</p>
        <p>К вашему объявлению будут применены дополнительные услуги Kupi95. Объявление будет показываться 5 дней на самом заметном месте - над обычными объявлениями в поиске. 200 руб.</p>
        <form action="{{ url('payment') }}" method="post" style="display: inline;">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" name="InvId" value="{{ $ad->id . rand(10, 99) }}">
            <input type="hidden" name="Shp_item" value="premium">
            <input type="hidden" name="OutSum" value="{{ App\Ad::PREMIUM_SUM }}">
            <button class="button-2 refresh">Сделать премиум</button>
        </form>
    </div>
@stop
