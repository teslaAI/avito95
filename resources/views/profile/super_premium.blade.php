@extends ('layout')

@section ('title', $ad->title)
@section ('description', $ad->text)

@section ('og')
    <meta property="fb:app_id" content="207354386280299">
    <meta property="og:title" content="{{ $ad->shareTitle }}">
    <meta property="og:type" content="website">
    <meta property="og:url" content="{{ Request::url() }}">
    <meta property="image" content="{{ $ad->getShareImageUrl('vk') }}">
    <meta property="og:image" content="{{ $ad->getShareImageUrl('facebook') }}">
    <meta property="og:description" content="{{ $ad->text }}">
@stop

@section('content')
    <div class="content col-2">
        <p>Услуга "премиум объявление"</p>
        <p>К вашему объявлению будут применены дополнительные услуги Kupi95. Объявление будет показываться 5 дней на самом заметном месте - над обычными объявлениями в поиске. И будет произведена рассылка смс сообщений по указанному вами району.</p>

        <form class="form-ad-wrapper" action="{{ url('payment') }}" method="post" style="display: inline;">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" name="InvId" value="{{ $ad->id . rand(10, 99) }}">
            <input type="hidden" name="Shp_item" value="superPremium">

            <label for="message">Сообщение * <span id="text-chars-left">Текст должен состоять из 50 букв включая символы</span></label>
            <textarea required type="text" class="styler" id="message" name="message" maxlength="1000"></textarea>

            <label for="area">Укажите Город, район и улицу * <span id="text-chars-left">Через любой разделитель или построчно</span></label>
            <input required type="text" class="styler" id="area" name="area">

            <label for="sms">Количество смс</label>
            <select id="sms" data-placeholder="Количество смс" name="OutSum">
                <option value="3000">1000 шт - 3000 руб</option>
                <option value="6000">2000 шт - 6000 руб</option>
                <option value="9000">3000 шт - 9000 руб</option>
                <option value="12000">4000 шт - 12000 руб</option>
                <option value="15000">5000 шт - 15000 руб</option>
                <option value="18000">6000 шт - 18000 руб</option>
                <option value="21000">7000 шт - 21000 руб</option>
                <option value="24000">8000 шт - 24000 руб</option>
                <option value="27000">9000 шт - 27000 руб</option>
                <option value="30000">10000 шт - 30000 руб</option>
            </select>

            <button class="button-2 refresh">Сделать премиум + смс</button>
        </form>
    </div>
@stop
