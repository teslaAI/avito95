@extends ('profile.layout')

@section ('profile-content')

<form class="form-ad-wrapper" action="" method="post" enctype="multipart/form-data">
	<input type="hidden" name="_token" value="{{ csrf_token() }}">
	<div class="upload-avatar clearfix">
		<img src="{{ Auth::user()->getAvatarURL() }}" alt="">
		<div class="upload-avatar-info">
			<p>Макс. размер: 1 Мб</p>
			<p>Формат: JPG, PNG или GIF</p>
			<input type="file" name="image">
		</div>
	</div>

	<label for="name">Имя *</label>
	<input required type="text" class="styler" id="name" name="name" value="{{ Auth::check() ? Auth::user()->name :'' }}">

	<label for="phone">Телефон *</label>
	<input required type="tel" class="styler" id="phone" name="phone" value="{{ Auth::check() ? Auth::user()->phone :'' }}">

	<label for="whatsapp">Whatsapp</label>
	<input type="tel" class="styler" id="whatsapp" name="whatsapp" value="{{ Auth::check() ? Auth::user()->whatsapp :'' }}">

	<label for="email">E-mail для уведомлений</label>
	<input type="text" class="styler" id="email" name="email" value="{{ Auth::check() ? Auth::user()->email :'' }}">

	<label for="city">Город</label>
	<select id="city" data-placeholder="Выберите город" name="city">
		@foreach ($regions as $region)
			<optgroup label="{{ $region->name }}">
			@foreach ($region->cities as $city)
				<option value="{{ $city->id }}"
					@if (Auth::user()->city_id == $city->id)
						selected
					@endif
				>
					{{ $city->name }}
				</option>
			@endforeach
			</optgroup>
		@endforeach
	</select>

	<label for="phone">Пароль</label>
	<input type="password" class="styler" name="password" value="">

	<button class="send-button">Сохранить</button>
</form>

@stop