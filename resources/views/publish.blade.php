@extends ('layout')

@section ('title', 'Добавить объявление')

@section('content')

<div class="content col-2">
	<div class="create-ad clearfix">
		@if (Request::is('ad/edit/*'))
			<h2>Редактирование объявления</h2>
		@else
			<h2>Добавить объявление</h2>
		@endif
		<form class="form-ad-wrapper" action="{{ Request::url() }}" method="post">
			@if (count($errors) > 0)
			    @foreach ($errors->all() as $error)
			        <div class="warning">{{ $error }}</div>
			    @endforeach
			@endif
			@if (Auth::guest())
				<p class="mb3">
                    Чтобы иметь возможность редактировать это объявление в дальнейшем, сначала
                    <a id="pp-open-popup" href="">авторизуйтесь</a>.
                </p>
			@endif
			@if (isset($updated) && $updated)
				<div class="success">
					Объявление успешно отредактировано.
				</div>
			@endif
			@if (session()->has('isFlood') && session()->pull('isFlood'))
				<div class="warning">
					Внимание! Это объявление уже добавлено ранее.
				</div>
			@endif
            @if (session()->has('robot') && session()->pull('robot'))
                <div class="warning">
                    Вы не прошли проверку captcha.
                </div>
            @endif
            <div class="input-half-wrapper left">
                <label class="required" for="phone">Телефон</label>
                <input type="tel" class="styler" id="phone" name="phone" required
                       @if (Request::is('ad/edit/*'))
                       value="{{ $ad->phone }}"
                       @elseif (Auth::check())
                       value="{{ Auth::user()->phone }}"
                        @endif
                >
            </div>
			<div class="input-half-wrapper right">
				<label for="phone">Whatsapp</label>
				<input type="tel" class="styler" id="whatsapp" name="whatsapp"
					   @if (Request::is('ad/edit/*'))
					   value="{{ $ad->whatsapp }}"
					   @elseif (Auth::check())
					   value="{{ Auth::user()->whatsapp }}"
						@endif
				>
			</div>
            <div class="input-half-wrapper">
                <label class="required" for="name">Контактное имя</label>
                <input type="text" class="styler" id="name" name="name" required
                       @if (Request::is('ad/edit/*'))
                       value="{{ $ad->author_name }}"
                       @elseif (Auth::check())
                       value="{{ Auth::user()->name }}"
                        @endif
                >
            </div>

			<label class="required" for="city">Город</label>
			<select name="city_id" id="city" required>
				<option value="" disabled selected>Выберите город</option>
				@foreach ($regions as $region)
					<optgroup label="{{ $region->name }}">
						@foreach ($region->cities as $city)
							<option	value="{{ $city->id }}"
								@if ( (Request::is('ad/edit/*') && $ad->city_id == $city->id) || Auth::check() && $city->id == Auth::user()->city_id )
									selected
								@endif
							>{{ $city->name }}</option>
						@endforeach
					</optgroup>
				@endforeach
			</select>

			<label class="required" for="category">Категория</label>
			<select
				name="category_id" id="category"
				data-search-limit="20"
				data-extraurl="{{ URL::to('publish/extra') }}"
				data-id="{{ isset($ad) ? $ad->id : '' }}"
				required>
				<option value="" disabled selected>Выберите категорию</option>
				@foreach ($categories as $category)
					<optgroup label="{{ $category->name }}">
						@foreach ($category->children as $child)
							<option
								value="{{ $child->id }}"
								data-slug="{{ $child->slug }}"
								@if (Request::is('ad/edit/*') && $child->id == $ad->category_id)
									selected
								@endif
							>{{ $child->name }}</option>
						@endforeach
					</optgroup>
				@endforeach
			</select>

			<label class="required" for="title">Заголовок<span id="title-chars-left">70</span></label>
			<input
				type="text"
				class="styler"
				id="title"
				name="title"
				value="{{ Request::is('ad/edit/*') ? $ad->title : '' }}"
				maxlength="70"
				required>

			<label for="desc">Описание <span id="text-chars-left">1000</span></label>
			<textarea
				name="text"
				id="desc"
				class="styler"
				maxlength="1000"
			>{{ Request::is('ad/edit/*') ? $ad->text : '' }}</textarea>

			<label for="price">Цена (руб.)</label>
			<input
				type="text"
				class="styler number"
				id="price"
				name="price"
				maxlength="10"
				value="{{ Request::is('ad/edit/*') ? $ad->price : '' }}">

			<div class="nothing">
				<label id="free">
					<input type="checkbox" name="is_free" {{ Request::is('ad/edit/*') && $ad->is_free ? 'checked' : '' }}>
					<span>бесплатно</span>
				</label>
			</div>

			<div class="publish-loader">
				<img src="{{ Request::root() }}/img/loader.gif" alt="">
			</div>

			@if (Request::is('ad/edit/*'))
				<ul class="upload clearfix">
					<label>Фото</label>
					@foreach ($ad->images as $image)
						<li> 
							<div class="del-btn" data-url="{{ Request::root() }}/aadd/remove_image/{{ $image->id }}"></div>
							<img src="{{ Request::root() }}/{{ $image->small }}" alt="">
						</li>
					@endforeach
					<!-- <li class="upload-btn">
						<p>Добавить фото</p>
					</li>  -->
				</ul>
			@endif

			<p class="uploader-label">Фото</p>
			<div action="{{ Request::root() }}/publish/images" class="dropzone" id="my-awesome-dropzone"
				data-img-url="{{ URL::asset('img/image.svg') }}"
				data-rm-url="{{ Request::root() }}/publish/remove_images">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
			</div>
			<p class="field-description">Кликните или перетащите сюда до 9 фото</p>

			@if(Request::is('publish'))
				<div class="captcha-wrapper">
					<div class="g-recaptcha" data-sitekey="6LeNFrUUAAAAAPe9rrRO7uONBeHsyrvjX7xbQrRi"></div>
				</div>
			@endif

			<button class="send-button">{{ Request::is('ad/edit/*') ? 'Сохранить' : 'Отправить' }}</button>
		</form>
	</div>
</div>

@stop
