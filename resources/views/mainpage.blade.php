@extends ('layout')

@section ('og')
	<meta property="og:title" content="kupi95">
	<meta property="og:type" content="website">
	<meta property="og:url" content="{{ Request::url() }}">
	<meta property="og:image" content="{{ URL::asset('img/kupi95-og.jpg') }}">
	<meta property="og:description" content="Сайт бесплатных объявлений">
@stop

@if (Request::is('category/*'))
	@section ('title', $category->name)
@endif

@section ('content')
<div class="content col-2">

	@include('sub.filters')

	<div class="list {{ $agent->isMobile() || $grid ? 'grid' : '' }}" id="ads-data">
		<div class="list-top clearfix">
			@if (Request::has('filter'))
				<div class="discount-categories">
					<ul class="owl-carousel">
						@foreach ($discount_categories as $category)
							<li><a class="owl-categories" href='{{ url("category/$category->slug?filter=discount") }}'>{{ $category->name }}</a></li>
						@endforeach
					</ul>
				</div>
			@endif
			@if (Request::is('category/*'))
				<h2>{{ $title }}</h2>
			@elseif (Request::has('filter'))
				<h2>Товары со скидкой</h2>
			@else
				<h2>Новые объявления</h2>
			@endif
			<!-- <button class="button-2 save-search">Сохранить поиск</button> -->
				<div class="list-buttons">
					<button class="list-btn"><i class="fas fa-bars  button" style="font-size:22px; color: #d7d8e0;"></i></button>
					<button class="grid-btn active" ><i class="fas fa-th-large button" style="font-size:22px; color: #d7d8e0;"></i></button>
				</div>
		</div>
		@if ($ads->count())
            @include ('sub.ads')
		@elseif (Request::has('search'))
			<h4>По вашему запросу "{{ Request::input('search') }}" ничего не найдено. Попробуйте изменить параметры поиска.</h4>
		@else
			<h4>В этой категории пока нет объявлений.</h4>
		@endif
	</div>
</div>
<div style="clear:both"></div>

<div class="add-mobile">
	@if (Auth::check())
		<a href="{{ url('publish') }}" id="add-mobile" class="add-button button" style="color: #fff">Добавить объявление</a>
	@else
		<div class="sign-in">
			<a href="{{ url('publish') }}" id="add-mobile" class="add-button button" style="color: #fff">Добавить объявление</a>
		</div>
	@endif
</div>

{{--<div class="ajax-load">--}}
	{{--<p><img src="https://media.giphy.com/media/10kTz4r3ishQwU/giphy.gif" style="width: 30px;"></p>--}}
{{--</div>--}}

{!! $ads->links('pagination::default') !!}
@stop

{{--@section('additionalScripts')--}}
{{--<script>--}}
    {{--var visited_ad_id = parseInt(Cookies.get('visited_ad_id'));--}}
    {{--var start_page = parseInt(Cookies.get('start_page'));--}}
    {{--var start = true;--}}
    {{--var page = start_page ? start_page : 1;--}}
    {{--var scrolledOnce = false;--}}

    {{--if (start_page) {--}}
        {{--Cookies.remove('start_page');--}}
        {{--start_page = undefined;--}}
    {{--}--}}

    {{--if (visited_ad_id) {--}}
        {{--window.addEventListener('scroll', event => {--}}
            {{--if (!scrolledOnce) {--}}
                {{--if (page !== 1) {--}}
                    {{--document.getElementById('ad_' + visited_ad_id).scrollIntoView({block: "center"});--}}
                {{--}--}}
                {{--Cookies.remove('visited_ad_id');--}}
                {{--visited_ad_id = null;--}}
                {{--scrolledOnce = true;--}}
            {{--}--}}
        {{--});--}}
    {{--}--}}

    {{--function loadDataActive() {--}}
        {{--if (--}}
            {{--($(window).scrollTop() + $(window).height() + 1000) >= $(document).height()--}}
            {{--&& start--}}
            {{--&& !visited_ad_id--}}
        {{--) {--}}
            {{--loadMoreData(++page);--}}
        {{--}--}}
    {{--};--}}

    {{--function loadMoreData(page){--}}
        {{--$.ajax({--}}
            {{--url: '?page=' + page,--}}
            {{--type: "get",--}}
            {{--beforeSend: function() {--}}
                {{--$('.ajax-load').show();--}}
                {{--start = false;--}}
            {{--}--}}
        {{--}).done(function(data) {--}}
            {{--if(data.html == ""){--}}
                {{--$('.ajax-load').html("Больше ничего нет");--}}
                {{--return;--}}
            {{--}--}}
            {{--$('.ajax-load').hide();--}}
            {{--$("#ads-data").append(data.html);--}}

            {{--start = true;--}}

            {{--$( ".premium-bar .premium" ).remove();--}}
            {{--$("#premium").append(data.premium);--}}

        {{--}).fail(function(jqXHR, ajaxOptions, thrownError) {--}}
            {{--console.error('Server not responding...');--}}
        {{--});--}}
    {{--}--}}

    {{--window.addEventListener('scroll', loadDataActive);--}}
{{--</script>--}}
{{--@stop--}}
