<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="UTF-8">
	<title>Ответ на ваш вопрос</title>
</head>
<body>
	<p>Здравствуйте, {{ $username }}!</p>
	<p>На ваш вопрос в объявлении «{{ $ad_title }}» добавлен ответ:</p>
	<p>&mdash; {{ $comment_text }}</p>
	<p>Чтобы посмотреть, перейдите по <a href="{{ Request::root() }}/ad/{{ $ad_id }}">ссылке</a></p>
</body>
</html>