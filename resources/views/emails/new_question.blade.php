<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="UTF-8">
	<title>Новый вопрос</title>
</head>
<body>
	<p>Здравствуйте, {{ $username }}!</p>
	<p>К вашему объявлению «{{ $ad_title }}» добавлен новый вопрос:</p>
	<p>&mdash; {{ $comment_text }}</p>
	<p>Чтобы ответить, перейдите по <a href="{{ Request::root() }}/ad/{{ $ad_id }}">ссылке</a></p>
</body>
</html>