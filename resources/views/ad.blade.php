@extends ('layout')

@section ('title', $ad->title)
@section ('description', $ad->text)

@section ('og')
	<meta property="fb:app_id" content="207354386280299">
	<meta property="og:title" content="{{ $ad->shareTitle }}">
	<meta property="og:type" content="website">
	<meta property="og:url" content="{{ Request::url() }}">
	<meta property="image" content="{{ $ad->getShareImageUrl('vk') }}">
	<meta property="og:image" content="{{ $ad->getShareImageUrl('facebook') }}">
	<meta property="og:description" content="{{ $ad->text }}">
@stop

@section('content')
	<div class="content col-2">
		@include('sub.filters')
		<div class="item clearfix">
			<div class="item-top clearfix">
				<div class="breadcrumbs">
					<ul>
						<li>
							<a href="{{ URL::to('/') }}">Главная</a>
						</li>
						@if ($ad->category)
							<li>
								<a href="{{ URL::to('category/'.$ad->category->parent->slug) }}">{{ $ad->category->parent->name }}</a>
							</li>
							<li>
								<a href="{{ URL::to('category/'.$ad->category->slug) }}">{{ $ad->category->name }}</a>
							</li>
							@if ($autoModel and ($make = $autoModel->parent))
								<li>
									<a href="{{ URL::to('category/'.$ad->category->slug.'/'.$make->slug) }}">{{ $make->title }}</a>
								</li>
								<li>
									<a href="{{ URL::to('category/'.$ad->category->slug.'/'.$make->slug.'/'.$autoModel->slug) }}">{{ $autoModel->title }}</a>
								</li>
							@endif
						@endif
					</ul>
				</div>
				@can('manage', $ad)
					<div class="item-menu">
						<button title="Удалить" id="delete-2" class="button-2 delete" data-url="{{ Request::root() }}/aadd/remove/{{ $ad->id }}" data-root="{{ Request::root() }}"  ></button>
						<a title="Изменить" href="{{ Request::root() }}/ad/edit/{{ $ad->id }}" class="button-2 edit">Изменить</a>
						<?php $leftToRefresh = floor((time() - strtotime($ad->created_at)) / 86400); ?>
						@if ($ad->is_premium)
							@if (!$ad->discount)<button onclick="window.location.href = '{{ Request::root() }}/aadd/discount/{{ $ad->id }}';" class="button-2" style="border: 1px solid red; color: red">Товар со скидкой</button>@endif
							<button class="premium-aadd" style="background-color:#909ccc; cursor: auto;" title="Премиум объявления выводятся в сайдбаре слева и повторяются в своей категории">Премиум</button>
							<div class="progress-block clearfix">
								<span class="clearfix">Премиум истекает через {{ $ad->getExpirePremium() }} дней</span>
							</div>
						@else
							@if (auth()->user()->is_admin || !Gate::denies('refresh', $ad))
								<button title="Вы можете бесплатно активировать объявление раз в 3 суток или платно неограниченное количество раз" data-title="Вы сможете обновить объявление через {{ $ad->getDaysToRefresh(true) }}" data-url="{{ Request::root() }}/aadd/refresh/{{ $ad->id }}" class="button-2 refresh {{ Gate::denies('refresh', $ad) ? 'unactive' : '' }}">Поднять объявление</button>
							@else
								<button title="Вы можете бесплатно активировать объявление раз в 3 суток или платно неограниченное количество раз" onclick="window.location.href = '{{ Request::root() }}/fresh/aadd/{{ $ad->id }}';" class="button-2 refresh">Поднять объявление</button>
							@endif
							<button class="go-premium" onclick="window.location.href = '{{ Request::root() }}/aadd/premium/{{ $ad->id }}';">Сделать премиум</button>
							<button class="go-premium go-super-premium" style="background-color: #76a7e1" onclick="window.location.href = '{{ Request::root() }}/aadd/super_premium/{{ $ad->id }}';">Сделать премиум +</button>
						@endif
					</div>
				@endcan
			</div>
			<h1>{{ $ad->title }}</h1>
			<div class="slider">
				@if ($ad->images->count())
					<div class="fotorama"
						 data-loop="true"
						 data-transition="crossfade"
						 data-thumbmargin="5"
						 data-thumbborderwidth="0"
						 data-thumbwidth="100"
						 data-thumbheight="75"
						 data-width="100%"
						 data-nav="thumbs"
						 data-allowfullscreen="true"
						 data-keyboard="true">
						@foreach ($ad->images as $image)
							<a href="{{ Request::root() }}/{{ $image->big }}">
								<img src="{{ Request::root() }}/{{ $image->small }}" alt="">
							</a>
						@endforeach
					</div>
				@else
					<div class="no-image">
						<img src="{{ URL::asset('img/placeholder.jpg') }}" alt="placeholder">
					</div>
				@endif
			</div>
			<div class="item-desc">
				<div class="item-pt clearfix">
					@if($ad->discount && $ad->discount->active && $ad->is_premium)
						<div class="item-price-block">
							<div class="item-price discount">{{ $ad->price . ' руб. '}}</div>
							<div class="item-price price">{{ $ad->getPrice() }}</div>
							<a href="" style="color: #fff" id="{{ Auth::check() ? 'discount-open-popup' : 'open-popup' }}">Купить со скидкой</a>
						</div>
					@else
						<div class="item-price">{{ $ad->getPrice() }}</div>
					@endif
					<div class="time-view">
						<p>
							{{ $ad->getFormattedTime() }}
						</p>
{{--						<span>{{ $ad->views }}</span>--}}
					</div>
				</div>
				@if ($ad->auto_properties !== null)
					<div class="item-attr">
						{!! $ad->auto_properties->getHtml() !!}
					</div>
				@endif
				@if ($ad->realty_properties !== null)
					<div class="item-attr">
						{!! $ad->realty_properties->getHtml() !!}
					</div>
				@endif
				<div class="item-specific">
					<h4>Описание</h4>
					<p>{!! $ad->getText() !!}</p>
					<h4 class="item-contacts">Контакты</h4>
					<dl class="clearfix">
						@if ($ad->city)
							<dt>Город</dt>
							<dd>{{ $ad->city->name }}</dd>
						@endif
						<dt>Продавец</dt>
						@if (null != $ad->author_name)
							<dd>{{ $ad->author_name }}</dd>
						@elseif (null !== $ad->author)
							<dd>{{ $ad->author->name }}</dd>
						@endif
					</dl>
					<div class="contact-buttons">
						<div class="item-phone" data-value="{{ Crypt::encrypt($ad->phone) }}" data-root="{{ Request::root() }}">
							<a href=""><i class="fas fa-phone"></i> Позвонить</a>
						</div>
						<div class="item-contact">
							@if ($ad->whatsapp != null)
								<a href="https://wa.me/{{ str_replace(['+','(',')','-',' '], '', $ad->whatsapp) }}?text=Привет, я увидел ваше Объявление на сайте Kupi95.ru. Ссылка: {{ Request::url() }}" class="write-message" for="comment-textarea"><i class="fab fa-whatsapp"></i> Whatsapp</a>
							@elseif (Auth::check() && $ad->author !== null)
								<label class="write-message" for="comment-textarea"><i class="fas fa-comment"></i> Написать</label>
							@endif
						</div>
					</div>
					<p class="say-about-us-text">Скажите, что нашли это объявление на kupi95.ру</p>
				</div>
			</div>
			<div class="item-comments">
				<div class="item-share clearfix">
					<div class="item-share-group1">
						<button class="fav button-2 {{ $ad->isTheUsersFavorite() ? 'active' : '' }}"
								data-set-url="{{ Request::root() }}/aadd/setfavorite/{{ $ad->id }}"
								data-remove-url="{{ Request::root() }}/aadd/removefavorite/{{ $ad->id }}"
								data-auth="{{ Auth::check() }}">В избранное</button>
						{{--<button class="complain button-2">Пожаловаться</button>--}}
					</div>

					<!-- share -->
					<div class="item-share-group2">
						<a class="fb-share button-2" href="#" onclick="window.open('//www.facebook.com/sharer/sharer.php?u={{ Request::url() }}&amp;title={{ $ad->title }}','','Toolbar=1,Location=0,Directories=0,Status=0,Menubar=0,Scrollbars=0,Resizable=0,Width=550,Height=400'); return false;">Поделиться</a>
						<a class="vk-share button-2" href="http://vk.com/share.php?url={{ Request::url() }}" onclick="window.open('http://vk.com/share.php?url={{ Request::url() }}','','Toolbar=1,Location=0,Directories=0,Status=0,Menubar=0,Scrollbars=0,Resizable=0,Width=550,Height=400'); return false;">Поделиться</a>
						<a class="wa-share button-2" href="#" onclick="window.open('https://api.whatsapp.com/send?text={{ Request::url() }}&utm_source=share2','','Toolbar=1,Location=0,Directories=0,Status=0,Menubar=0,Scrollbars=0,Resizable=0,Width=550,Height=400'); return false;">Поделиться</a>
					</div>
					<!-- /share -->

					<div class="social"></div>
					<p class="ad-add2fav-warning">Чтобы добавить в избранное, необходимо <a id="open-popup" href="">авторизоваться</a>.</p>
				</div>
				<h4 class="comments-title">Вопросы владельцу</h4>
				@if ($ad->author === null)
					Это объявление от неавторизованного пользователя. Автор не может получать вопросы.
				@else
					<div class="clearfix">
						@include ('sub.comments')
					</div>
					@if (Auth::check() && Auth::user()->id != $ad->author_id)
						<h4>Написать сообщение</h4>
					@else
						<h4 class="comment-form-h-hided">Ответить</h4>
					@endif
					@if (Auth::check())
						<form action="{{ Request::root() }}/comment/create" class="comment-form {{ Auth::user()->id == $ad->author_id?'comment-form-hided':'comment-form' }}">
							<textarea name="text" placeholder="Введите сообщение" id="comment-textarea" required></textarea>
							<div class="check">
								<label>
									<input type="checkbox" name="is_private">
									<span>личное сообщение</span>
								</label>
							</div>
							<input type="hidden" name="ad_id" value="{{ $ad->id }}">
							<button class="send-button">Отправить</button>
						</form>
					@else
						<a href="" id="open-popup">Войдите в свой аккаунт</a>, чтобы задать вопрос владельцу.
					@endif
				@endif
			</div>
			<div class="item-related">
				@if ($similarAds->count())
					<h4>Похожие объявления</h4>
					<ul>
						@foreach ($similarAds as $s_ad)
							<li class="clearfix">
								<a href='{{ $s_ad->url }}'>
									<img src="{{ URL::asset($s_ad->getThumbnailUrl()) }}" alt="">
								</a>
								<div class="related-wraper">
									<div class="related-title">
										<a href='{{ $s_ad->url }}'>{{ $s_ad->title }}</a>
									</div>
									<div class="related-desc">{{ $s_ad->getText(true, 70) }}</div>
									<div class="related-price">{{ $s_ad->getPrice() }}</div>
								</div>
							</li>
						@endforeach
					</ul>
				@endif
			</div>
		</div>
	</div>
	@if(isset($ad->discount))
		<div class="discount-popup">
			<div class="popup-wrapper">
				<p>Уважаемый покупатель!</p>
				<p>Для заказа товара со скидкой, вам необходимо оплатить указанную продавцом предоплату за товар.
					Данная предоплата учитывается в сумму товара. Напоминаем вам, что если товар соответствует объявлению,
					данная предоплата не возвращается в случае отказа на сделанный заказ.</p>
				<p><b>Сумма к оплате: {{ $ad->discount->prepayment }} руб.</b></p><br>

				<form action="{{ url('payment') }}" method="post">
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
					<input type="hidden" name="InvId" value="{{ $ad->id . rand(10, 99) }}">
					<input type="hidden" name="Shp_item" value="prepaidDiscount">
					<input type="hidden" name="OutSum" value="{{ $ad->discount->prepayment }}">
					<input type="hidden" name="prepayment" value="prepayment">

					<button href="" class="button-2 discount-button">Оплатить</button>
				</form>
			</div>
		</div>
	@endif
@stop
