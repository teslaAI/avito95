<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

	<meta name="keywords" content="объявления, ингушетия, хоам">
	<meta name="description" content="Сайт бесплатных объявлений в Ингушетии.">

	<link rel="stylesheet" href="{{ asset('css/app.css?' . filemtime('css/app.css')) }}">
	<link rel="icon" href="{{ Request::root() }}/img/favicon.png">
	<title>Страница не найдена</title>
</head>
<body class="p404">
	<div class="header">
		<div class="row clearfix"> 
			<a href="{{ Request::root() }}" class="logo logo-404"></a>
			<div class="h-wrapper clearfix">
				<menu>
					<li><a href="{{ Request::root() }}/rules">Правила сайта</a></li>
					<li><a href="{{ Request::root() }}/about">О нас</a></li>
					<li><a href="{{ Request::root() }}/contacts">Контакты</a></li>
				</menu>
				<a href="{{ Request::root() }}/publish" class="add-button button">Добавить объявление</a>
				@if (Auth::check())
					<div class="h-avatar {{ $new_questions > 0 ? 'notify' : '' }}">
						@if (null !== Auth::user()->avatar)
							<img src="{{ Request::root() }}/upload/{{ Auth::user()->avatar->path }}" alt="">
						@else
							<img src="{{ Request::root() }}/img/noavatar.jpg" alt="">
						@endif
						<div class="touch"></div>
						<div class="drop-menu">
							<ul>
								<li><a href="{{ Request::root() }}/profile/ads">Мои объявления</a></li>
								<li><a href="{{ Request::root() }}/profile/questions">
									Вопросы
									@if ($new_questions > 0)
										<span>{{ $new_questions }}</span>
									@endif
								</a></li>
								<li><a href="{{ Request::root() }}/profile/favorite">Избранное</a></li>
								<li><a href="{{ Request::root() }}/profile/settings">Настройки профиля</a></li>
								<li><a href="{{ Request::root() }}/logout">Выход</a></li>
							</ul>
						</div>
					</div> 
				@else
					<div class="sign-in">
						<a href="">Войти</a>
					</div>
				@endif
			</div>
		</div>
	</div>
	<div class="row middle clearfix">
		@yield('content')
	</div>
	<div class="footer">
		<div class="row">
			<div class="social">
				<a class="social-vk" href="http://vk.com/hoamru"></a>
				<a class="social-fb" href="https://www.facebook.com/hoamru"></a>
			</div>
			<menu>
				<li><a href="{{ Request::root() }}/rules">Правила</a></li>
				<li><a href="{{ Request::root() }}/about">О нас</a></li>
				<li><a href="{{ Request::root() }}/contacts">Контакты</a></li>
			</menu>
		</div>
	</div>
	<div class="popup">
		<div class="popup-wrapper">
			<ul class="clearfix">
				<li id="login" class="active">Войти</li>
				<li id="registr">Регистрация</li>
			</ul>
			<div class="forms">
				<form class="forget-form" action="{{ Request::root() }}/forget">
					<p>Для получения нового пароля, введите номер телефона, указанный при регистрации:</p>
					<input placeholder="Телефон" name="phone" type="tel" required>
					<button class="button">Отправить</button>
				</form> 
				<form class="login" action="{{ Request::root() }}/login">
					<input placeholder="Телефон" name="phone" type="tel" class="phone-input">
					<input placeholder="Пароль" name="password" type="password" id="login_password">
					<button class="button">Войти</button>
					<a href="" class="pass-forget" >Забыли пароль?</a>
				</form> 
				<form class="registration" action="{{ Request::root() }}/register">
					<input placeholder="Имя" name="name" type="text" required>
					<input placeholder="Телефон" name="phone" type="tel" class="phone-input" required>
					<input placeholder="Пароль" name="password" type="password" required>
					<button class="button">Регистрация</button>
				</form>
			</div> 
		</div>
	</div>
	<div class="scroll-top">наверх</div>
	<script src="{{ Request::root() }}/js/bundle.min.js?{{ filemtime('js/bundle.min.js') }}"></script>
	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-72420904-1', 'auto');
	  ga('send', 'pageview');

	</script>
</body>
</html>