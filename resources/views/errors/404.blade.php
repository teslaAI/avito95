@extends ('errors.layout')

@section('content')

<div class="not-found">
    <h2>Страница, которую вы ищете, не существует.</h2>
    <p>Возможно, вы воспользовались несуществующей ссылкой или страница была удалена.</p>
    <img src="{{ Request::root() }}/img/404.gif">
</div>

@stop
