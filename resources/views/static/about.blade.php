@extends ('layout')

@section ('title', 'О сайте')

@section ('content')

<div class="content col-2">
	<div class="static clearfix">
		<h2>О проекте</h2> 
		<div class="static-text">
			<p>Kupi95 — это площадка для объявлений в новом формате. Внимание отводится каждому объявлению, благодаря удобному интерфейсу и отсутствию навязчивых элементов, среди которых теряются публикации. Здесь вы можете максимально быстро добавить или найти нужное объявление.</p>
			<p>Проект стартует в Республике и ставит перед собой амбициозную задачу - составить конкуренцию ведущим сайтам объявлений в России.</p>
			<p>Мы будем охотно прислушиваться к вашим замечаниям и советам, чтобы сайт развивался и становился лучше.</p>
			{{--<div class="video">--}}
				{{--<iframe width="640" height="360" src="https://www.youtube.com/embed/SmbDjIbE6T4" frameborder="0" allowfullscreen></iframe>--}}
			{{--</div>--}}
		</div>
	</div>
</div>

@stop
