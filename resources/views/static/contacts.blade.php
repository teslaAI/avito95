@extends ('layout')

@section ('title', 'Контакты')

@section ('content')
<div class="content col-2">
	<div class="static clearfix">
		<h2>Контакты</h2> 
		<div class="static-text">
			<script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3A7e9adf54b60575d53161d23bbeb37d9467048f7ebafa22590bd6d96dbb8f4b5c&amp;width=100%25&amp;height=400&amp;lang=ru_RU&amp;scroll=true"></script>
			<div class="contacts-info">
				<h4>Адрес:</h4>
				<p>Москва, ул. Ярославское шоссе, 146/2. 9-й этаж. Офис 962.</p>
				<h4>Офис в Чеченской Республике:</h4>
				<p>Г. Грозный просп. Мухаммеда Али, 2А</p>
			</div>
			<div class="contacts-info">
				<h4>Email:</h4>
				<p><a href="mailto:79260000004@mail.ru">79260000004@mail.ru</a></p>
			</div>
			<div class="contacts-info">
				<h4>Телефон:</h4>
				<p><a href="tel:+79260000004">+7 (926) 000-00-04</a></p>
			</div>
		</div>
	</div>
</div>
@stop
