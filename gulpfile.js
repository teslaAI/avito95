var gulp = require('gulp');
var concatCss = require('gulp-concat-css');
var minifyCSS = require('gulp-minify-css');
var rename = require("gulp-rename");
// var minify = require('gulp-minify');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
// var autoprefixer = require('gulp-autoprefixer');

gulp.task('default', function() {
	return gulp.src([
			// 'public/css/*.css',
			'public/css/fonts.css',
			'public/css/normalize.css',
			'public/css/style.css',
			'public/css/jquery.formstyler.css',
			'public/css/fotorama.css',
			'public/css/dropzone.css',
			'public/css/main.css',
			// '!public/css/bundle.min.css'
		])
	    .pipe(concatCss("bundle.css"))
	    .pipe(minifyCSS({
	    	keepSpecialComments: 0,
	    }))
	 //    .pipe(autoprefixer({
		// 	browsers: ['last 2 versions'],
		// 	cascade: false
		// }))
	    .pipe(rename('bundle.min.css'))
	    .pipe(gulp.dest('public/css/'));
});

gulp.task('scripts', function() {
	return gulp.src([
			'public/js/jquery-2.1.4.min.js',
			'public/js/*.js',
			// 'public/js/jquery.formstyler.min.js',
			// 'public/js/fotorama.js',
			// 'public/js/jquery.maskedinput.min.js',
			// 'public/js/script.js',
			// 'public/js/dropzone.js',
			// 'public/js/main.js',
			'!public/js/bundle.min.js'
		])
		.pipe(concat("bundle.js"))
		.pipe(uglify({
			mangle: false
		}))
		.pipe(rename('bundle.min.js'))
		.pipe(gulp.dest('public/js/'));
});



// gulp.task('watch', function() {
// 	gulp.watch('css/*.css', ['default']);
// });


// var elixir = require('laravel-elixir');


//  |--------------------------------------------------------------------------
//  | Elixir Asset Management
//  |--------------------------------------------------------------------------
//  |
//  | Elixir provides a clean, fluent API for defining some basic Gulp tasks
//  | for your Laravel application. By default, we are compiling the Sass
//  | file for our application, as well as publishing vendor resources.
//  |
 

// elixir(function(mix) {
//     mix.sass('app.scss');
// });
